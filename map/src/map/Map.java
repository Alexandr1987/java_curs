package map;


public class Map {
    static int hSize;
    static int vSize;
    static int x;
    static int y;
    static int cx;
    static int cy;
    public static void main(String[] args) {
        printMap(10,10);
        printMapArea();
        printViewDirection();
    }

    static void printMap(int hiSize, int viSize){
        hSize = hiSize;
        vSize = viSize;
        x = (hSize * 40)/100;
        //System.out.println(x);
        y = (vSize * 30)/100;
        //System.out.println(y);
        cx = (hSize * 40)/100;
        //System.out.println(cx);
        cy = (vSize * 50)/100;
        //System.out.println(cy);
        for (int i = -4; i <= hSize+4; i++){
            for (int j = -4; j <= vSize+4;j++){
                if (i == -3 && j == vSize/2) System.out.print("N");
                else if (i == -2 && j == vSize/2) System.out.print("^");
                else if (i == hSize/2 && j == -4) System.out.print("W  ");
                else if (i == hSize/2 && j == -3) System.out.print("<  ");
                else if (i == hSize/2 && j == vSize +3) System.out.print(" >");
                else if (i == hSize/2 && j == vSize +4) System.out.print(" E");
                else if (i == hSize + 3 && j == vSize/2) System.out.print("v");
                else if (i == hSize + 4 && j == vSize/2) System.out.print("S");
                else if (i == 0 && (j > 0 && j <= vSize)) {
                    System.out.print(j >= 10 ? j +" ": j + "  ");
                }else if ((j == -1)&&(i > 0 && i <= hSize)) {
                    System.out.printf("%2d ",i);
                }else if ((i > 0 && i <= hSize ) && (j > 0 && j <= vSize )){
                    if (i == x && j == y) {System.out.print("O  ");}
                    else if (i == cx && j == cy){
                        System.out.print("C  ");}
                        else {
                        System.out.print(".  ");
                    }
                }else {
                    System.out.print("   ");
                }
            }
            System.out.println();
        }
    }

    static void printMapArea(){
        int aria = hSize * vSize;
        System.out.println("Area is "+ aria +"m");
    }

    static void printViewDirection(){
        System.out.print("Direction tu wiew object is ");
        if (cx < x ) {
           // System.out.print("N");
            if (cy == y ||cy == y - 1 ||cy == y + 1){
                System.out.println("N");
            }else if (cy < y ){
                System.out.println("W");
            }
        } else if (cx == x) {
            if (cy < y ){
                System.out.print("W");
            }else {
                System.out.print("E");
            }
        }else if (cx > x ){
            if (cy == y || cy == y - 1 || cy == y + 1){
                System.out.print("S");
            }else if (cy < y ){
                System.out.println("W");
            }else {
                System.out.println("E");
            }
        }
    }

    static void printDistanceToObject(){

    }
}

