package main;
@SuppressWarnings("all")
public class ImmutableApp {

	public static void main(String[] args) {
		Currency todaysCurrency = new Currency("EUR","MDL",17.50f,"2020-01-01");
		Currency tomorrowsCurrency = todaysCurrency.setDate("2020-01-02");

		System.out.println(todaysCurrency);
		System.out.println(tomorrowsCurrency);

	}

	final class Currency {
	    private String baseCode;
	    private String code;
	    private Float value;
	    private String date;
		
	    
	    
	    public void setBaseCode(String baseCode) {
			this.baseCode = baseCode;
		}

		public void setCode(String code) {
			this.code = code;
		}

		public void setValue(Float value) {
			this.value = value;
		}

		public setDate(String date) {
			this.date = date;
		}

		public String getBaseCode() {
			return baseCode;
		}

		public String getCode() {
			return code;
		}

		public Float getValue() {
			return value;
		}

		public String getDate() {
			return date;
		}

		public Currency(String baseCode, String code, Float value, String date) {
			super();
			this.baseCode = baseCode;
			this.code = code;
			this.value = value;
			this.date = date;
		}

		@Override
		public String toString() {
			return "Currency [baseCode=" + baseCode + ", code=" + code + ", value=" + value + ", date=" + date + "]";
		}
	    
	    
	}
	
}
