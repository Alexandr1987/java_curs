package zerox;
import java.util.ArrayList;
import java.util.List;

import paper.PaperDocumentInterface;
public class Xerox {

	public PaperDocumentInterface copy(PaperDocumentInterface original){
	      return original;
	   }
	public List<PaperDocumentInterface> copy(PaperDocumentInterface original, int quantity){
        List<PaperDocumentInterface> list = new ArrayList<>();
        for (int i = 0; i < quantity; i++) {
            list.add(copy(original));}
            return list;
	   }
}
