package main;

import java.util.List;

import paper.PaperDocument;
import paper.PaperDocumentInterface;
import zerox.Xerox;

public class XeroxApp {

	public static void main(String[] args) {
		PaperDocumentInterface documentTest = new PaperDocument("Contract", "Java Developer Hiring Contract", "1. Working hours per day: 6\\n2. Earn per hour: 15EUR\\n3. Holidays per year: 30days.");
        Xerox xerox = new Xerox();
        System.out.println(documentTest);
        PaperDocument copy = (PaperDocument) xerox.copy(documentTest);
        copy.setType("Contract Copy");
        System.out.println(copy);
        List<PaperDocumentInterface> listOfThreeCopies = xerox.copy(copy, 3);

        for (PaperDocumentInterface paperDocument : listOfThreeCopies) {
            paperDocument.setType("Archived Contract Copy");
            
        }
        System.out.println(listOfThreeCopies);
	}

}
