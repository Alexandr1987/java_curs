package paper;

public class PaperDocument implements PaperDocumentInterface{

	private String type;
	private String title;
	private String content;

	@Override
	public void setType(String type) {this.type=type;}
	@Override
	public String getType() {return type;}
	@Override
	public void setTitle(String title) {this.title=title;}
	@Override
	public String getTitle() {return title;}
	@Override
	public void setContent(String content) {this.content=content;}
	@Override
	public String getContent() {return content;	}
	
	public PaperDocument(String type, String title, String content) {
		super();
		this.type = type;
		this.title = title;
		this.content = content;
	}
	
	public PaperDocument() {}
	
	@Override
	public String toString() {
		return "PaperDocument [type=" + type + ", title=" + title + ", content=" + content + "]";
	}	
}
