package provider;
import config.XMLConfigurationProvider;
import nasaStorage.NasaStorage;
import java.net.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.*;
@SuppressWarnings("all")

public class NasaDataProvider {
	NasaStorage NS;
	ArrayList<NasaStorage>  nasaStorage = new ArrayList<NasaStorage>();
	public ArrayList<NasaStorage> getNeoAsteroids(String start, String end) throws Exception{
	 String finalUrl = new XMLConfigurationProvider().getKey("url")+"?start_date="+start+"&end_date="+end+"&api_key="+new XMLConfigurationProvider().getKey("key");
	
	 //1 connect to NASA API 
	 URL nasa = new URL(finalUrl);
	 BufferedReader in = new BufferedReader(
	 new InputStreamReader(nasa.openStream()));
	 
	 //2 read data
	 String stringData ="";
	 String inputLine;
	 while ((inputLine = in.readLine()) != null)
//	     System.out.println(inputLine.replaceAll("}", "}\n"));
		 stringData +=inputLine;
	 in.close(); 
	 
	 //3 Parse JSON
	 JSONObject data = new JSONObject(stringData);
	 int count = data.getInt("element_count");
	 //4 Test some data
	 System.out.println("Found "+count+" results");	 
	 
		 LocalDate localDate1 = LocalDate.parse(start);
		 LocalDate localDate2 = LocalDate.parse(end);
	     List<LocalDate> datesSet = new ArrayList<>(150);
	     while (localDate1.isBefore(localDate2) || localDate1.equals(localDate2)) {
	                datesSet.add(localDate1);
	                localDate1 = localDate1.plusDays(1);	                
	        } 
	   for(LocalDate datasample:datesSet) {
		   System.out.println(datasample);
	   
		String dateNow = datasample.toString();
		
	    int arr = data.getJSONObject("near_earth_objects").getJSONArray(dateNow).length();
	    for(int counter=0;counter<arr;counter++) {			
			Float d_min = data.getJSONObject("near_earth_objects")
	 				.getJSONArray(dateNow)
	 				.getJSONObject(counter)
	 				.getJSONObject("estimated_diameter")
	 				.getJSONObject("kilometers")
	 				.getFloat("estimated_diameter_min");
			
			Float d_max = data.getJSONObject("near_earth_objects")
					.getJSONArray(dateNow)
					.getJSONObject(counter)
					.getJSONObject("estimated_diameter")
					.getJSONObject("kilometers")
					.getFloat("estimated_diameter_max");
			
			Long distance_km = data.getJSONObject("near_earth_objects")
					.getJSONArray(dateNow)
					.getJSONObject(counter)
					.getJSONArray("close_approach_data")
					.getJSONObject(0)
					.getJSONObject("miss_distance")
					.getLong("kilometers");
			
			boolean isHazardous = data.getJSONObject("near_earth_objects")
					  .getJSONArray(dateNow)
					  .getJSONObject(counter)
					  .getBoolean("is_potentially_hazardous_asteroid");
			
			nasaStorage.add(new NasaStorage(dateNow,d_min,d_max,distance_km,isHazardous));

//			System.out.printf("%s %n",new NasaStorage(dateNow,d_min,d_max,distance_km,isHazardous));	
		}
	 }
	return nasaStorage;
  } 

}
