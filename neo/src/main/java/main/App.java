package main;

import java.util.Scanner;

import config.XMLConfigurationProvider;
import provider.NasaDataProvider;
@SuppressWarnings("all")
public class App {

	public static void main(String[] args)throws Exception{
		Scanner in = new Scanner(System.in);
		System.out.println("Enter start/end dates");
		String start = in.next(), end = in.next();	
		new NasaDataProvider().getNeoAsteroids(start, end);
		
//		new NasaDataProvider().getNeoAsteroids("2020-04-01","2020-04-02");
	}

}
