package main;

import java.io.InputStream;
import java.net.*;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
@SuppressWarnings("all")
public class GUI extends Application {	

    @Override
    public void start(Stage primaryStage)throws Exception{
    	FXMLLoader loader = new FXMLLoader();
    	URL xmlUrl = getClass().getResource("/window/wind.fxml");
        loader.setLocation(xmlUrl);
        Parent root = loader.load();
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }   
    
	public static void main(String[] args) {
		launch();
	}

}
