package mvc;
import java.util.List;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import nasaStorage.NasaStorage;
import provider.NasaDataProvider;
@SuppressWarnings("unused")
public class MainGUIController {

    @FXML
    private Label response;
	@FXML
	private TextField startDateInput;

	@FXML
	private TextField endDateInput;

	@FXML
	private TableView<NasaStorage> tableView;

	@FXML
	private TableColumn<NasaStorage, String> date;

	@FXML
	private TableColumn<NasaStorage, Float> diameterMin;

	@FXML
	private TableColumn<NasaStorage, Float> diameterMax;
	
	@FXML
	private TableColumn<NasaStorage, Long> missDistance;

	@FXML
	private TableColumn<NasaStorage, Boolean> isHazardous;

	@FXML
	void getData() throws Exception {
		if(startDateInput.getText().isEmpty() && endDateInput.getText().isEmpty()) {
			response.setText("Error! provide correct start/end Date");System.out.println("no dates");
		}else {
			response.setText("");
			List<NasaStorage> asteroids =  new NasaDataProvider().getNeoAsteroids(startDateInput.getText(),endDateInput.getText());
			ObservableList<NasaStorage> data = FXCollections.observableArrayList(asteroids);
			date.setCellValueFactory(new PropertyValueFactory<NasaStorage, String>("date"));
			diameterMin.setCellValueFactory(new PropertyValueFactory<NasaStorage, Float>("d_min"));
			diameterMax.setCellValueFactory(new PropertyValueFactory<NasaStorage, Float>("d_max"));
			missDistance.setCellValueFactory(new PropertyValueFactory<NasaStorage, Long>("diameter"));
			isHazardous.setCellValueFactory(new PropertyValueFactory<NasaStorage, Boolean>("hazardous"));

			tableView.setItems(data);
		}
		


	}
	
	 
}
