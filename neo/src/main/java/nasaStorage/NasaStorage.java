package nasaStorage;

public class NasaStorage {
	private String date;
	private Float d_min;
	private Float d_max;
	private Long diameter;
	private boolean hazardous;
	 
	
	public String getDate() {
		return date;
	} 
	public void setDate(String date) {
		this.date = date;
	}
	public Float getD_min() {
		return d_min;
	}
	public void setD_min(Float d_min) {
		this.d_min = d_min;
	}
	public Float getD_max() {
		return d_max;
	}
	public void setD_max(Float d_max) {
		this.d_max = d_max;
	}
	public Long getDiameter() {
		return diameter;
	}
	public void setDiameter(Long diameter) {
		this.diameter = diameter;
	}
	public boolean isHazardous() {
		return hazardous;
	}
	public void setHazardous(boolean hazardous) {
		this.hazardous = hazardous;
	}
	public NasaStorage(String date, Float d_min, Float d_max, Long diameter, boolean hazardous) {
		this.date=date;
		this.d_min = d_min;
		this.d_max = d_max;
		this.diameter = diameter;
		this.hazardous = hazardous;
	}
	
	
	public NasaStorage() {}
	@Override
	public String toString() {
		return "NasaStorage [date=" + date + ", d_min=" + d_min + ", d_max=" + d_max + ", diameter=" + diameter
				+ ", hazardous?--" + hazardous + "]";
	}

	
	
}
