package config;

import java.io.File;
import java.io.InputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
  
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
@SuppressWarnings("all")
public class XMLConfigurationProvider {

	public  String getKey(String name) {
		 try {
		 ClassLoader classLoader = XMLConfigurationProvider.class.getClassLoader();
//		 File file = new File(classLoader.getResource("xml_data/nasa.xml").getFile());
		 InputStream inputStream = classLoader.getSystemResourceAsStream("xml_data/nasa.xml");
		 DocumentBuilderFactory factory =  DocumentBuilderFactory.newInstance();
		 DocumentBuilder builder = factory.newDocumentBuilder();
//		 Document doc = builder.parse(file); 
		 Document doc = builder.parse(inputStream); 
		 NodeList nList = doc.getElementsByTagName("connection");
		 		 Node nNode=nList.item(0);
				 Element eElement = (Element)nNode;				 
				 if(name == "key") {
					 name = eElement.getElementsByTagName("key").item(0).getTextContent();
					 return name;
				 }else if(name == "url") {
					 name = eElement.getElementsByTagName("url").item(0).getTextContent();
					 return name;
				 }
		 }
		 catch(Exception e) {e.printStackTrace();}
		return name;
		
	 }
	
}
