package tests;
import config.XMLConfigurationProvider;
import static org.junit.jupiter.api.Assertions.*;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import org.junit.jupiter.api.Test;

@SuppressWarnings("all")

class ConnTest  {

	@Test
	public void testURL() throws Exception {
	    String strUrl = new XMLConfigurationProvider().getKey("url")+"?start_date=2015-09-07&end_date=2015-09-08&api_key="+new XMLConfigurationProvider().getKey("key");

	    try {
	        URL url = new URL(strUrl);
	        HttpURLConnection urlConn = (HttpURLConnection) url.openConnection();
	        urlConn.connect();

	        assertEquals(HttpURLConnection.HTTP_OK, urlConn.getResponseCode());
	    } catch (IOException e) {
	        System.err.println("Error creating HTTP connection");
	        e.printStackTrace();
	        throw e;
	    }
	}
}
