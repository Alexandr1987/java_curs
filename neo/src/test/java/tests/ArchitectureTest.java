package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import mvc.MainGUIController;
import main.*;
import config.XMLConfigurationProvider;
import provider.NasaDataProvider;
@SuppressWarnings("all")

class ArchitectureTest {
	// this test checks if application has the MVCLogInGUIController
	@Test
	void CheckForMVCLogInGUIController() {
		try {
			Class.forName("mvc.LogInGUIController");
		} catch (ClassNotFoundException e) {
			
			fail("The app doesnt have MainGUIController(mvc)");
		}
	}
	// this test checks if application has the MainGUIController
	@Test
	void CheckForMVCMainGUIController() {
		try {
			Class.forName("mvc.MainGUIController");
		} catch (ClassNotFoundException e) {
			
			fail("The app doesnt have MainGUIController(mvc)");
		}
	}
	// this test checks if application has the Main GUI
		@Test
		void CheckForGUIClass() {
			try {
				Class.forName("main.GUI");
			} catch (ClassNotFoundException e) {
				
				fail("The app doesnt have GUIClass(main)");
			}
		}
// this test checks if application has the Main Application
	@Test
	void CheckForAppClass() {
		try {
			Class.forName("main.App");
		} catch (ClassNotFoundException e) {
			
			fail("The app doesnt have AppClass(main)");
		}
	}
	// this test checks if application has the XMLConfigurationProvider
	@Test
	void CheckForConfigClass() {
		try {
			Class.forName("config.XMLConfigurationProvider");
		} catch (ClassNotFoundException e) {
			
			fail("The app doesnt have XMLConfigurationProviderClass(config)");
		}
	}
	
	// this test checks if application has the NasaDataProvider
	@Test
	void CheckForNasaDataProviderClass() {
		try {
			Class.forName("provider.NasaDataProvider");
		} catch (ClassNotFoundException e) {
			
			fail("The app doesnt have NasaDataProviderClass(provider)");
		}
	}
	
	// this test checks if application has the NasaStorage
	@Test
	void CheckForNasaStorageClass() {
		try {
			Class.forName("nasaStorage.NasaStorage");
		} catch (ClassNotFoundException e) {
			
			fail("The app doesnt have NasaStorageClass(nasaStorage)");
		}
	}
// check method main
	@Test
	void CheckMethodMainApp()  throws NoSuchMethodException,SecurityException{
		try {
			Class.forName("main.App").getMethod("main",String[].class);
		} catch (ClassNotFoundException e) {
			
			fail("The app doesnt have both GUI and CLI");
		} catch (NoSuchMethodException e) {
			fail("The app doesnt have  method");
		} catch (SecurityException e) {
			e.printStackTrace();
		}
	}
	@Test
	void CheckMethodMainGUI()  throws NoSuchMethodException,SecurityException{
		try {
			Class.forName("main.GUI").getMethod("main",String[].class);
		} catch (ClassNotFoundException e) {
			
			fail("The app doesnt have both GUI and CLI");
		} catch (NoSuchMethodException e) {
			fail("The app doesnt have  method");
		} catch (SecurityException e) {
			e.printStackTrace();
		}
	}
}
