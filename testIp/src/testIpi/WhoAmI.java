package testIpi;

import java.net.*;

public class WhoAmI {
   public static void main(String[] args) throws Exception {
      if (args.length != 1) {
         System.err.println("Usage: WhoAmI MachineName");
         System.exit(1);
      }
      InetAddress a = InetAddress.getByName("localhost");
      System.out.println(a);
   }
} 
