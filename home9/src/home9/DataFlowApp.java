package home9;

public class DataFlowApp {

	public static void main(String[] args) {
		
		DataTransformer.integerToByte(10000000);
//		DataTransformer.byteToInteger((byte)3);
		DataTransformer.checkByteToInteger(DataTransformer.byteToInteger((byte)3));
//		DataTransformer.doubleToInteger(500f);
		DataTransformer.checkDoubleToInteger(DataTransformer.doubleToInteger((double)300f));
//		DataTransformer.integerToDouble(300);
		DataTransformer.checkIntegerToDouble(DataTransformer.integerToDouble(32999));
//		DataTransformer.shortToInteger((short)5);
		DataTransformer.checkShortToInteger(DataTransformer.shortToInteger((short)322767));
//		DataTransformer.integerToShort(3);
		DataTransformer.checkIntegerToShort(DataTransformer.integerToShort(32));
//		DataTransformer.doubleToShort(1.5f);
		DataTransformer.checkDoubleToShort(DataTransformer.doubleToShort(33767.6565f));
	}

}
class DataTransformer {
	
	static void integerToByte( int value){
		byte res = (byte)value;		
		if(res>128) {
			System.err.println("WARNING! the value "+value+" overflows \"byte\" range. DATA will was lost!");
		}else {
			System.out.println(res);
		}
	}
	static int byteToInteger(byte valueTwo){
		int res1 = (int)valueTwo;
		return res1;
	}
	static void checkByteToInteger(int res1) {
		if(res1>2147483647) {
			System.err.println("WARNING! the value "+res1+" overflows \"integer\" range. DATA will was lost!");
		}else {
			System.out.println(res1);
		}
	}
	static int doubleToInteger(double valueThree){
		int res2 = (int)valueThree;
		return res2;
	}
	static void checkDoubleToInteger(double res2) {
		if(res2>2147483647) {
			System.err.println("WARNING! the value "+res2+" overflows \"integer\" range. DATA will was lost!");
		}else {
			System.out.println(res2);
		}
	}
	static double integerToDouble(int value){
		double res3 = (double)value;
		return res3;
	}
	static void checkIntegerToDouble(double res3) {
		if(res3>(double)2147483647.12346) {
			System.err.println("WARNING! the value "+res3+" overflows \"double\" range. DATA will was lost!");
		}else {
			System.out.println(res3);
		}
	}
	static int shortToInteger(short valueFour){
		int res4 = (short)valueFour;
		return res4;
	}
	static void checkShortToInteger(int res4) {
		if(res4>32767) {
			System.err.println("WARNING! the value "+res4+" overflows \"int\" range. DATA will was lost!");
		}else {
			System.out.println(res4);
		}
	}
	
	static short integerToShort(int value){
		short res5 = (short)value;
		return res5;
	}
	static void checkIntegerToShort(short res5) {
		if(res5>32767) {
			System.err.println("WARNING! the value "+res5+" overflows \"short\" range. DATA will was lost!");
		}else {
			System.out.println(res5);
		}
	}
	static short doubleToShort(double value){
		short res6 = (short)value;
		return res6;
	}
	static void checkDoubleToShort(short res6) {
		if(res6>32767) {
			System.err.println("WARNING! the value "+res6+" overflows \"short\" range. DATA will was lost!");
		}else {
			System.out.println(res6);
		}
	}
}



// pentru ce transformam short -> integer?? s.a. in principiu eroarea va aparea daca depasim valoarea maxima admisa....
