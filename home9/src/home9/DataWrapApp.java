package home9;

public class DataWrapApp {

	public static void main(String[] args) {
		DataWrapper.wrapRound("text");
		DataWrapper.wrapSquare("array");
		DataWrapper.wrapAngle("tag");
		DataWrapper.wrapDouble(0.5);
		DataWrapper.printer(DataWrapper.wrapRound("text"));
		DataWrapper.printer(DataWrapper.wrapSquare("array"));
		DataWrapper.printer(DataWrapper.wrapAngle("tag"));
		DataWrapper.printer(DataWrapper.wrapDouble(0.5));
	}

}

class DataWrapper {
	static String wrapRound( String text ) {
		  return "(" + text + ")";
		}
	static String wrapSquare( String text ) {
		  return "[" + text + "]";
		}
	static String wrapAngle(  String text ) {
		  return "<" + text + ">";
		}
	static String wrapDouble(  Double num ) {
		  return "(" + num + ")";
		}
	
	static void printer(String text) {
		System.out.println(text);
	}
}