const EventEmitter = require('events')
const calendar = new EventEmitter()


calendar.on('Sunday', ()=>{
    console.log("Yey!! Sunday arrived!!!")
  })

  calendar.on('Friday', ()=>{
    console.log("Yey!! weekend is here!!!")
  })
  
 function myDelay(day){
    if(day == "Friday"){
        calendar.emit("Friday")
        calendar.emit("Sunday")
    }else if(day == "Sunday"){
        calendar.emit("Sunday");   
    }else{
        console.log("No weekend for you!!!")
    }
 }


 calendar.emit("Friday")
  setTimeout(myDelay, 1000, "Sunday");
 