function createEntity(){alert("This will create an entity")}
function updateEntity(){alert("This will update an entity")}
function deleteEntity(){alert("This will delete an entity")}

function showEntities(response){
    console.log(response)
    let tableHead = ''
    response.data.forEach((entity) => {
        tableHead=`<tr>`
        Object.keys(entity).forEach((key)=>{                
            
            tableHead+=`<th>${key}</th>`
                
        });
        tableHead+=`<th>Actions</th>`
        tableHead+=`</tr>`
    });
    // create visual table
    let tableBody = ''
    response.data.forEach((entity) => {
        tableBody+=`<tr>`
        Object.keys(entity).forEach((key)=>{                
            
            tableBody+=`<td>${entity[key]}</td>`                
        });
        tableBody+=`<td>
        <button type="button" class="btn btn-info btn-sm" data-bs-toggle="modal" data-bs-target="#updateModal"><i class="bi bi-pencil"></i></button>
        <button type="button" class="btn btn-danger btn-sm" data-bs-toggle="modal" data-bs-target="#deleteModal"><i class="bi bi-trash"></i></button>
        </td>`
        tableBody+=`</tr>`
    });
    let template = `        
    <table class="table">
        ${tableHead}
        ${tableBody}
    </table>`

    let contentdiv = document.getElementById("content")
    contentdiv.innerHTML = template;
}

function processResponse(response){
    return response.json()
}

function showError(error){
    console.log(error);
    let footerDiv = document.getElementById("footer")
    footerDiv.innerHTML = 'Error, cannot load data<br>Reason: '+error;

}

function getEntities(type){
    const URL =`/${type}/index.json`
    fetch(URL).then(processResponse)
              .then(showEntities)
              .catch(showError)
}

function postEntity(){

}

getEntities("posts")