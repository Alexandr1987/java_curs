   if(url== "/"){
       const html = fs.readFileSync("./server/public/index.html")
    res.end(html)
   } else if(url.startsWith("/api/")){
        // Split path
        let urlPath =url.split("?")[0]
        let urlParams =url.split("?")[1]
        let key = urlParams.split("=")[1]
        // Split path

        if(key!=access_key){
            return res.end("ACCESS DENIED")
        }

        // api routing
        if(urlPath.endsWith("/products/all")){
            const products = fs.readFileSync("./server/data/products.json")
            res.end(products)
        }else if(urlPath.endsWith("/products/category")){
            // DECODE JSON
            const products = JSON.parse(fs.readFileSync("./server/data/products.json"))
            const productsFiltered =[]
            for(let i = 0; i< products.length; i++){
                if(products[i].category == "Category 1"){
                    productsFiltered.push(products[i])
                }
            }
            res.end(JSON.stringify(productsFiltered))
        }else{
            res.end("NOT FOUND")
        }
   }else{
       res.end("NOT FOUND")
   }

//    api routing