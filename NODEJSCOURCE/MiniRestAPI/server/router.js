const fs = require("fs");
exports.route = function route(url, aliases, res){
    // if(url=="/favicon.ico"){ return res.end()}  
    
    const path = url.split("?")[0];
    const queryString = url.split("?")[1];
    console.log(queryString)
    let params
    if(queryString){
            params = queryString.split("&").map(value=>{
            let obj={}
            obj[value.split("=")[0]] = value.split("=")[1]
            return obj;
        })
        
    }
    

    // auto routing
    // -> /[module]/[method]
    let alias = aliases.find(obj=>obj[path])
    alias = alias ? alias[path]:path
    let segments = alias.split("/")
    let moduleName = segments[1]
    let functionName = segments[2]
    
    if(
        fs.existsSync(`./modules/${moduleName}.js`)
        ){
        const moduleObject = require(`./modules/${moduleName}`)
        res.end(moduleObject[functionName](params))
    }else{
        res.end("404-NOT FOUND")
    }
}