// switch to hsttps

const http = require('http');
const{port,host,access_key}=require("./config.json")
const {route} = require("./router")


// ROUTE CONFIG
const routesAlliases = [
    {"/products":"/products/all"},
    {"/clients":"/clients/all"}
    ]
// ROUTE LOGIC


// 1. main page
// 2. /api/* - api requests
// 3. not api and not main ->404

const server = http.createServer(({url},res)=>{
    
   route(url,routesAlliases,res)

})

server.listen(port,host,()=>{console.log("server started on: "+"http://"+host+":"+port)})