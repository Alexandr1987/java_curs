    const http = require("http");
    const {host,port,access_key}=require("./config")
    const fs = require("fs")
    const readHTMLPage = require("./pageSurfer")
    const rdJSONFile = require("./dataSurfer")
    const server = http.createServer((req,res)=>{
        const {url} = req;
        //routing
        if(url=="/"){            
            //   readHTMLPage.rdHtml("index", (content)=>{console.log(content); return res.end(content)})    
            
            readHTMLPage.rdHtml("index", (content)=>{console.log(">>>>>>>>>>>>>>> \n",content); res.end(content)})
         
        }else if(url.startsWith("/api/")){
                // let path = url
                let path   = url.split("?")[0]
                // let params = url.split("?")[1]
                // let key    = params.split("=")[1]
                // // api routing
                // if(key!= access_key){
                //     return res.end("Access denied !!!" + key + `${access_key}`)
                // }

                if(path.endsWith("products/all")){
                  rdJSONFile.rdJson( "products", (data) => {console.log(data); res.end(JSON.stringify(data)) }) 
                    // const products = fs.readFileSync("./server/data/products.json")
                    // no json decoded !!!

                    // res.end(products)
                }else if(path.endsWith("products/category")){
                    const products = JSON.parse(fs.readFileSync("./server/data/products.json"))
                    // !! decode json
                    const productsFiltered  = []
                    for(let i = 0; i < products.length; i++){
                        if(products[i].category== "Category 1"){
                            productsFiltered.push(products[i])
                        }
                    }
                    res.end(JSON.stringify(productsFiltered))
                }else{
                    res.end("NOT FOUND")
                }


        }else{
            res.end("NOT FOUND")
        }

        console.log(`> Server:incoming request`+ url)
        res.end("OK")
    })

    server.listen(port,host, ()=>{
        console.log(`> Server is running http://${host}:${port}`)
    })