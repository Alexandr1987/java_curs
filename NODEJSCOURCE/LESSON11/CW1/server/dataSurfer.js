const fs = require('fs')
const myPath = "./server/data/"

module.exports.rdJson =async function readJSONFile(name,cb){
   return await fs.promises.readFile(myPath+name+".json").then(result=>{
   
       try{cb( JSON.parse(result))}catch(err){console.log(err)}
   })
}

// readJSONFile( "products", (data) => { console.log(data) }) 