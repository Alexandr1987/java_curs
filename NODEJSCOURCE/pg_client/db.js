const {Client } = require('pg')


module.exports.getData = async function getData(){
    const client = new Client({
        host: 'localhost',
        port: 5432,
        user: 'postgres',
        database: 'pg_node_ex',
        password: 'geoserver',
    })
    await client.connect()
    const res = await client.query('SELECT * FROM people')
    // console.log(res.rows[2])
    let data = []
    res.rows
    // .filter((row)=>{
    //     return row.age>=20
    // })
    .forEach((row)=>{
        data.push(row)
    })
    await client.end()
    return data;
}

module.exports.saveData = async function saveData(person){
    const {id,name,age} = person
    const client = new Client({
        host: 'localhost',
        port: 5432,
        user: 'postgres',
        database: 'pg_node_ex',
        password: 'geoserver',
    })
    await client.connect()
    await client.query('INSERT INTO people(id, name, age)VALUES ($1, $2, $3);',[id,name,age])
    await client.end()
}


module.exports.deleteData = async function deleteData(person){
    const {id,name,age} = person
    const client = new Client({
        host: 'localhost',
        port: 5432,
        user: 'postgres',
        database: 'pg_node_ex',
        password: 'geoserver',
    })
    await client.connect()
    await client.query('DELETE FROM people WHERE id=$1;',[person.id])
    await client.end()
}

module.exports.updateData = async function updateData(person){
    const {id,name,age} = person
    const client = new Client({
        host: 'localhost',
        port: 5432,
        user: 'postgres',
        database: 'pg_node_ex',
        password: 'geoserver',
    })
    await client.connect()
    await client.query('UPDATE people SET name=$2, age=$3 WHERE id=$1;',[id,name,age])
    await client.end()
}
 