const http = require('http')
const dataFetcher = require('./db.js')
const fs = require ('fs')

const hostname = '192.168.0.138';
const port = 3000;

const send =(res,status,message,data=null)=>{
    res.writeHead(status, {'Content-Type': 'application/json'})
    let responseBody = {message:message}
    if (data!=null){
        responseBody.data=data 
    }
    res.write(JSON.stringify(responseBody))
    res.end("")
}



const server = http.createServer((req,res)=>{
    if(req.url == "/"){
       fs.readFile("./frontend/index.html",(err,data)=>{
        // if(err) throw err       
        res.end(data)
        })
      return
    }
    if(req.url == "/client.js"){
       fs.readFile("./frontend/client.js",(err,data)=>{
        if(err) throw err       
        res.end(data)
         })
            return
    }    

    if(req.url != "/api")
    send(res,404,"Ivalid ulr address")
        
    if(req.headers['content-type']!='application/json')
        {
            return send(res,400,"This API supports only JSON content");
        }

    switch(req.method){
        case "POST": 
        // get all data from req body  
        let data = ""   
                req.on('data',(chunk)=>{
                    data += chunk;
                    console.log(data)
                });
                req.on('data',()=>{
                    data = JSON.parse(data)
                    console.log(data)
                    console.log("No more data")
                    dataFetcher.saveData(data).then(()=>{
                        send(res,200,"record created");
                    }).catch((err)=>{
                        console.log(err)
                    }) 
                })        
        break;
        case "GET" :
            dataFetcher.getData().then(dataR=>{
                send(res,200,"record/s found",dataR);
            });
            
         break;
         case "PATCH" :
            let datat = ""   
            req.on('data',(chunk)=>{
                datat += chunk;
            });
            req.on('data',()=>{
                datat = JSON.parse(datat)                
                console.log("No more data")
                dataFetcher.updateData(datat)
                send(res,200,"record updated");
                }); 
            

            
         break;
         case "DELETE" :
            let datal = ""   
            req.on('data',(chunk)=>{
                datal += chunk;
            });
            req.on('data',()=>{
                datal = JSON.parse(datal)                
                console.log("No more data")
                dataFetcher.deleteData(datal).then(()=>{
                    send(res,200,"record deleted");
                }); 
            }) 

         break;
        default    :send(res,405,"use only GET, POST, PUT, DELETE");break;
        }    


    
})
server.listen(port, hostname, () => {
    console.log(`Server running at http://${hostname}:${port}/`);
  });