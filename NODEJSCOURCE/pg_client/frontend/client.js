function showError(error){
    console.log(error);
    let footerDiv = document.getElementById("footer")
    footerDiv.innerHTML = 'Error, cannot load data<br>Reason: '+error;

}


const fetchPeople =()=>{

fetch("/api", { 
            headers: {
                "Content-Type":"application/json"
            },
        }).then( res=>res.json() )
          .then(data=> {renderPeopleList(data.data)} )
    }
    const renderPeopleList = (data)=>{
       
        let contentDiv = document.getElementById("content")
        let html = ''
        let tableHead = ''
        let tableBody = ''
        data.forEach(item => {
            tableHead=`<tr>`
            Object.keys(item).forEach((key)=>{                
            
                tableHead+=`<th>${key}</th>`
                    
            });
            tableHead+=`<th>Actions</th>`
            tableHead+=`</tr>`
        });
        data.forEach(item => {
            tableBody+=`<tr>`
            Object.keys(item).forEach((key)=>{                
            
                tableBody+=`<td>${item[key]}</td>` 
                    
            });
            tableBody+=`<td>
            <button type="button" id=${item.id} class="btn btn-info btn-sm idsData" data-bs-toggle="modal" data-bs-target="#updateModal" value = ${item.id}  onclick="myData(${item.id})"><i class="bi bi-pencil"></i></button>
            <button type="button" id="deleteById" class="btn btn-danger btn-sm" data-bs-toggle="modal" data-bs-target="#deleteModal" value = ${item.id} onclick="myData(${item.id})"><i class="bi bi-trash"></i></button>
            </td>`
            tableBody+=`</tr>`
        });

        let template = `        
        <table class="table table-dark table-striped">
            ${tableHead}
            ${tableBody}
        </table>`

        contentDiv.innerHTML = template
    }


    window.onload = fetchPeople


    function createEntity(){
        let Inputid = document.getElementById("InputId").value;
        let nameClient = document.getElementById("InputName").value;
        let ageClient = document.getElementById("InputAge").value;

        let obj = {"id":parseInt(Inputid),"name":nameClient,"age":parseInt(ageClient)}
          console.log(obj)      
        fetch("/api",{
            method:'POST',
            body:JSON.stringify(obj),
            headers:{'Content-Type':'application/json'}
        }).then((res)=>{
            return res.json()
        }).then((res)=>{

            
           let template =`
           <div class="alert alert-success d-flex align-items-center" role="alert">
            <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Success:"><use xlink:href="#check-circle-fill"/></svg>
            <div>
                "${res.message}"!
            </div>
            </div>`
    
            let modalBody = document.getElementById('createModalBody');
            let modalFooter = document.getElementById('createModalFooter');
            modalBody.innerHTML = template;
            modalFooter.innerHTML='';
            fetchPeople();
    
        }).catch(showError)
    }

    // Store value of id from table 
    let val 
    function myData(id){
        val = id
    }


    function updateEntity(){
        let Inputid = document.getElementById("InputIdUpdate").value;
        let nameClient = document.getElementById("InputNameUpdate").value;
        let ageClient = document.getElementById("InputAgeUpdate").value;
         let obj = {"id":parseInt(Inputid),"name":nameClient,"age":parseInt(ageClient)}
          console.log(obj)      
        fetch("/api",{
            method:'PATCH',
            body:JSON.stringify(obj),
            headers:{'Content-Type':'application/json'}
        }).then((res)=>{
            return res.json()
        }).then((res)=>{

            
           let template =`
           <div class="alert alert-success d-flex align-items-center" role="alert">
            <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Success:"><use xlink:href="#check-circle-fill"/></svg>
            <div>
                "${res.message}"!
            </div>
            </div>`
    
            let modalBody = document.getElementById('updateModalBody');
            let modalFooter = document.getElementById('updateModalFooter');
            modalBody.innerHTML = template;
            modalFooter.innerHTML='';
            fetchPeople();
    
        }).catch(showError)
    
    }

    function deleteEntity(){
        let obj = {"id":val,"name":null,"age":null}
        console.log(obj)      
      fetch("/api",{
          method:'DELETE',
          body:JSON.stringify(obj),
          headers:{'Content-Type':'application/json'}
      }).then((res)=>{
          return res.json()
      }).then((res)=>{

          
         let template =`
         <div class="alert alert-success d-flex align-items-center" role="alert">
          <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Success:"><use xlink:href="#check-circle-fill"/></svg>
          <div>
              "${res.message}"!
          </div>
          </div>`
  
          let modalBody = document.getElementById('deleteModalBody');
          let modalFooter = document.getElementById('deleteModalFooter');
          modalBody.innerHTML = template;
          modalFooter.innerHTML='';
          fetchPeople();
  
      }).catch(showError)
    }