module.exports.menu = require("./menu").menu
let helpers = require("./helpers")
const fs = require('fs');
let localName;
module.exports.printMenu = helpers.printMenu;
module.exports.printMessage = helpers.printMessage;

const dir = fs.readdirSync('./food');
dir.forEach((fileName) => {
    if(!fileName.endsWith('.json') && fileName !="index.js")
 localName = fileName.split('.').slice(0,-1).join('.');
console.log(">>>>>>>>>"+fileName);
module.exports.localName = require('./'+fileName);

})
