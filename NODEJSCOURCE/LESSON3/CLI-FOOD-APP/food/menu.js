//DATA MODULE
// this module depends on Item

const Item = require("./Item")
const Price = require("./price")
const Drink = require("./drink.js");
const Food = require("./food.js");
const Measurement = require("../physical/measurement.js")
module.exports.menu = 
    [
        // new Item(1,"Salad",new Price(25,"MDL")),
        // new Item(2,"Soup",new Price(15,"MDL")),
        // new Item(3,"Bread",new Price(5,"MDL")),
        new Drink(1, "Cola", new Price(10, "MDL"), new Measurement(0.5,"L")),
        new Food(2, "Salad", new Price(25, "MDL"), new Measurement(200,"g"))
    ]

