const Item = require("./Item")
const Price = require("./price")
const Drink = require("./drink.js");
const Food = require("./food.js");
const Measurement = require("../physical/measurement.js")

module.exports = class Food extends Item {
    constructor(id,name, price,weight){
        super(id,name, price)
        this.weight = weight;
    }

    print(){
        return `-----------------------\n`+
        `${this.id}.`.padEnd(2)+` ${this.name}: `.padStart(10)+`${this.price.value} ${this.price.unit}`.padStart(0)+`${this.weight.value} ${this.weight.unit}`.padStart(8)+`\n`+
               `-----------------------`
    }
}