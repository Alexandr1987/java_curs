const Item = require("./Item")

module.exports = class Price {
    constructor(value, unit){
        this._value = value;
        this.unit=unit;        
    }

    get value(){
        return this._value;
    }
    set value(amount){
        return this._value = amount;
    }

    get unit(){
        return this._unit;
    }
    set unit(units){
        return this._unit = units;
    }
}

