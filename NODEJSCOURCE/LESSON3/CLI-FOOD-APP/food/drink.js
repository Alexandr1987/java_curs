const Item = require("./Item")
const Price = require("./price")
const Drink = require("./drink.js");
const Food = require("./food.js");
const Measurement = require("../physical/measurement.js")

module.exports = class Drink extends Item {
    constructor(id,name, price,volume){
        super(id,name, price)
        this.volume = volume;
    }
    
    print(){
        return `-----------------------\n`+
        `${this.id}.`.padEnd(2)+` ${this.name}: `.padStart(10)+`${this.price.value} ${this.price.unit}`.padStart(0)+`${this.volume.value} ${this.volume.unit}`.padStart(8)+`\n`+
               `-----------------------`
    }
}