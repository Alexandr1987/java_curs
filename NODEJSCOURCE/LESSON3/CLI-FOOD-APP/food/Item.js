// ad field "Id"
// make price -> value:1.00, currency: "USD"
//refactor print -->allign

// 1. Salad
module.exports = class Item {
    constructor(id,name, price){
        this.id = id;
        this.name=name;
        this.price=price;
    }

    print(){
        return `-----------------------\n`+
        `${this.id}.`.padEnd(2)+` ${this.name}: `.padStart(10)+`${this.price.value} ${this.price.unit}`.padStart(0)+`\n`+
               `-----------------------`
    }
}

//              