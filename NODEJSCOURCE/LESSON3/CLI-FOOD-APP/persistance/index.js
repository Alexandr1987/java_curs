const FS = require("fs");
const Food = require('../food/food.js')
const Price = require("../food/price")
const Drink = require("../food/drink.js");
const Measurement = require("../physical/measurement.js")


module.exports.save=(data)=>{
   let path = "./persistance/data/food.json"
      FS.writeFileSync(path,JSON.stringify(data));
   }
// let myData = new Food(2, "Salad", new Price(25, "MDL"), new Measurement(200,"g"))
// FS.writeFileSync("./data/food.json",JSON.stringify(myData));

module.exports.load=()=>{
   let data = JSON.parse(FS.readFileSync("./persistance/data/food.json"));
  return data;
 
 }

