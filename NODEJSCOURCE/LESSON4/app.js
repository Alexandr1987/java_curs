const http = require('http');
const {KEY, PATH, HOST} = require('./config');
const readline = require("readline");
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});
// let city = "Chisinau";

rl.question("What is your city ? ", function(city) {
try{

const callback = (res)=>{
    // console.log(res)

    res.on('end',()=>{
        console.log("API responsed end")
    })

    res.on('data',(chunk)=>{
        // console.log("API responsed data")
        const data = JSON.parse(chunk.toString());
        console.log(" Temp: "+data.main.temp+" K \n Temp_min: "+data.main.temp_min+" K \n Temp_max: "+data.main.temp_max+" K \n W_speed: "+data.wind.speed+" m/s \n W_dir: "+data.wind.deg+" deg")
    })

    res.on('error',()=>{
        console.log("API responsed error")
    })
}

const req = http.request({
    host: HOST,
    path:  PATH + `?q=${city}&appid=${KEY}`,
    port:80,
    method: 'GET'
},callback)
// res=>{

// }
// req.on('connect',()=>{
//     console.log('>>>>>>> got connected')
// })
// req.on('connect',()=>{
//     console.log('>>>>>>> got connected some info')
// })

req.end();
}catch (e){
    console.log(e)
}
});