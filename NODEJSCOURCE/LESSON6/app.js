const http = require('http');
const {KEY, PATH, HOST} = require('./config');


// https://alexwohlbruck.github.io/cat-facts/docs/


const callback = (res)=>{
    // console.log(res)

    res.on('end',()=>{
        console.log("API responsed end")
    })

    res.on('data',(chunk)=>{
        // console.log("API responsed data")
        // console.log(JSON.parse(chunk.toString()))
        const data = JSON.parse(chunk.toString());
        console.log(data.length)
        for(let i = 0; i<data.length; i++){
            console.log("Cats facts here: " + data[i].text);
        }
        
    })

    res.on('error',()=>{
        console.log("API responsed error")
    })
}

// const req = http.request({
//     host: HOST,
//     path:  PATH + `?q=${city}&appid=${KEY}`,
//     port:80,
//     method: 'GET'
// },callback)
const req = http.request({
    host: HOST,
    path:  PATH,
    port:80,
    method: 'GET'
},callback)
// res=>{

// }
// req.on('connect',()=>{
//     console.log('>>>>>>> got connected')
// })
// req.on('connect',()=>{
//     console.log('>>>>>>> got connected some info')
// })

req.end();
