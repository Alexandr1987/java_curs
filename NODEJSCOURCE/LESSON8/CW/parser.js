

data = require("./data/test-single.json")
/**
 * 
 * @param {String} path, indicates the search path 
 * @param {*} data, the data structure 
 * @returns * - if found, undefined - if not found
 */
function getValue(path, data){

    let segments = path.split(".")
    let level = data
    let lastSegment = undefined
    segments.forEach(s => {
        if(level!==undefined && level!== null ){
        level=level[s]
        } else{
            level = undefined
            throw "ERROR: Key "+lastSegment+" not found !!!" 
        }
        lastSegment = s
        // console.log(level)
    });
    // console.log(segments)
    return level
}
module.exports.getValue = getValue
// // console.log(getValue("a",data))
// // console.log(getValue("a.5",data))
// // console.log(getValue("o.p",data))
// // console.log(getValue("o.p2",data))
// console.log(getValue("n",data))
// console.log(getValue("n.0",data))
// console.log(getValue("b",data))
// console.log(getValue("b.0",data))