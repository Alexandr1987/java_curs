let data = {
  
    users:[
      {
        name: "John",
        address: {
          city: "GoodTown",
          country: "Amazing States of Unity"
        }
      },
      {
        name: "Marry",
        address: {
          city: "JobTown",
          country: "Employment Emirates"
        }
      },
      
      // ...
  
    ]
    
  }

  function getValue( path ) {
        const pathTree = path.split('.')
        const currentPath = []
        let value = data;

        for(const segment of pathTree){
            try{
                value = value[segment]
                currentPath.push(segment)
            }catch(err){
                throw new Error(currentPath.join('.'),'not exist')
            }
        }
        return value;
  }

  console.log (getValue('users.0.name'))
  console.log (getValue('users.1.address.city'))
//   console.log (getValue('users.2.name'))