const http = require("http");
const {host,port,access_key}= require("./config")
const {route}=require("./router")


// route config
const routeAliases = [
    {"/clients":"/clients/all"},
    {"/products":"/products/all"},
]

//route logic

const server = http.createServer(({url},res)=>{


    route (url, routeAliases, res)



    // auto routing
    //ROUTING
    //protocol  /[module]/[function]
    // if (url == "/favicon.ico") return res.end() //guard go to line 21 - 24




    //ROUTING


    // if (url == "/products/all"){
    //     const products = require("./modules/products")
    //     res.end(products.all())
    // }
    // else if (url == "/products/details/1"){
    //     const products = require("./modules/products")
    //     res.end(products.details())
    // }
    // else if (url == "/products/delete/1?access=123"){
    //     const products = require("./modules/products")
    //     res.end(products.delete())
    // }
    // else if (url == "/products/save?access=123"){
    //     const products = require("./modules/products")
    //     res.end(products.save())
    // }
    //cons
    // - big code
    // - strong coupling 
    // - not reusable 
    // - static 



})

server.listen(port,host,()=>{
    console.log(`>Test Server running http://${host}:${port}`)
})