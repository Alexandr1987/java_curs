const fs = require("fs")
exports.route = function route(url, aliases, res){

    const path = url.split("?")[0]
    const queryString = url.split("?")[1]
    let params
    if(queryString){
        params = queryString.split("&").map(val =>{
            let obj={}
            obj[val.split("=")[0]]=val.split("=")[1]
            return obj
        })
         console.log(params)
    }

    let alias = aliases.find(obj => obj[path])
    alias = alias ? alias[path] : path
    let segments = alias.split("/")
    let moduleName = segments[1]
    let functionName = segments[2]
    // console.log(fs.Sync(`./modules/${moduleName}.js`))
    if (fs.existsSync(`./modules/${moduleName}.js`)){
        const modulesObject = require(`./modules/${moduleName}`)
        res.end(modulesObject[functionName](params))
    }else{
        res.end(" 404 NOT FOUND ")
    }
}