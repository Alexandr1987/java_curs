// const fs = require('fs')
// function readFromFile(fileName) {
//     const data = fs.readFileSync("./data.json")
//     return JSON.parse(data)
// }


// function getData(data, propertyName) {
//     if (data.hasOwnProperty(propertyName)) {
//         return data[propertyName]
//     }
// }


// function printValue(value) {
//     console.log(">>>>>>>>"+value)
// }

// ////////////////////////////////
// printValue(getData(readFromFile('./data.json'), 'dummy'))
// printValue(getData(readFromFile('./data.json'), 'inexistent'))

// const data1 = require("./data.json")
// const fs = require('fs').promises

// async function readFromFile( filename, cb ) {    
//     let dataX
//     // const data = await fs.readFile(filename).then(cb(dataX)).then(cb(dataX)).catch( cb(`ERROR IN DATA-PARSING: ${error}`))
//     await fs.readFile(filename).then(data=>JSON.parse(data)).then(data=>cb(data)).catch( error => cb(`ERROR IN DATA-PARSING: ${error}`))
//         // try{
//         //   const data =await fs.readFile(filename);
//         //   dataX = JSON.parse(data);
//         //   cb(dataX)
//         // }catch(error){
//         //     cb(null,`ERROR IN DATA-PARSING: ${error}`)
//         // }
//   }

//   function app(){
//     readFromFile("./data.json", (data,error)=>{
//         if(error){
//             console.log(error)
//         }else{
//             console.log(data)
//         }
//     } )
//   }
//   app()

const fs = require('fs').promises
async function readFromFile(filename, cb) {
    await fs.readFile(filename).then(data=>JSON.parse(data)).then(data=>cb(data)).catch( error => cb(`ERROR IN DATA-PARSING: ${error}`))
}


async function getData(propertyName) {
        readFromFile('./data.json', (data,error)=>{
        if(error){
            console.log(error)
        }else{
            if (data.hasOwnProperty(propertyName)) {
                return data[propertyName]
            }
        }
    } )



}


function printValue(value) {
    console.log(value)
}

////////////////////////////////
// printValue(getData(readFromFile('./data.json'), 'dummy'))
printValue(getData("dummy"))