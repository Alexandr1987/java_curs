const fs = require("fs")
const data1 = require("./data.json")

async function readGromFile(filename){
    return fs.promises.readFile(filename).then(result=>JSON.parse(result));
}

async function getData(path,propertyName){
    return await readGromFile(path).then(result=>result.hasOwnProperty(propertyName) ? result[propertyName]: 'no such data')
}

async function printData (path,name){
    return await getData(path,name).then(result=>console.log(result))
}

printData('./data.json', 'dummy');
printData('./data.json', 'inexistent');