// 2) Async
// function getData(json, cb) {
//     let data
  
//     setTimeout(() => {
//       data = JSON.parse(json)
//       cb(data)
//     }, 1000)
//   }
  
//   function app() {
//     // let data = getData('{"name": "javscript"}')
//     // console.log(data)
  
//     try {
//       getData('{"name": "javscript"}', (data) => {
//         console.log(data)
//       })
//     } catch (error) {
//       console.log('DATA ERROR')
//     }
//   }

// app()


function getData(json, cb) {
    let data
  
    setTimeout(() => {
        try {
      data = JSON.parse(json)
      cb(data)
    } catch (error) {
        cb(null,"DATA ERROR")
      }
    }, 1000)
  }
  
  function app() {
    // let data = getData('{"name": "javscript"}')
    // console.log(data)
  
    
      getData('{"name": "javscript"}', (data,error) => {
        if(error){
            console.log(error)
        }else{
            console.log(data)
        }
      })
    
  }

app()