package main;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class Application {

	public static void main(String[] args) {
		
		List<Integer> numbers = new ArrayList<>();
		
		numbers.add(1000);
		numbers.add(-2000);
		numbers.add(3000);
		numbers.add(-4000);
		numbers.add(5000);
		
		
		System.out.println("Setting up stream");
		Stream <Integer> ns = numbers
			   .stream()
			   .filter(n->{
				   System.out.println("The predicate operates on "+ n);
				   return n>=0;})
			   .filter(n->{
				   System.out.println("The predicate operates on "+ n);
				   return n<=4000;});
			   
		System.out.println("Starting up stream");
		ns.forEach(n->System.out.println(n));
	}

}
