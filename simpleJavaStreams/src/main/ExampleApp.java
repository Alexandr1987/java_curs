package main;

import java.util.ArrayList;
import java.util.List;

public class ExampleApp {

	public static void main(String[] args) {
		
		////////////////////// Producer (Source)//////////
		List<String> names = new ArrayList<>();
		names.add("Jonny");
		names.add("Oreo");
		names.add("Poppins");
		names.add("Oscar");
		names.add("Marry");
		names.add("Droppins");
		names.add("Zoia");
		names.add("Orlando");
		////////////////////////STREAM -TEMPORARY/////////
		names.stream()
			 .skip(2)
			 .filter(ExampleApp::select)
			 .forEach(ExampleApp::show); // convert to stream
		////////////////////////Consumer//////////////////
	}
	public static void show (String v) {
		System.out.println(v);
	}
	
	public static boolean select(String v) {
		
		return v.startsWith("O");
	} 
}
