package home;

import ingredients.CoffeePowder;
import ingredients.Suggar;
import ingredients.Water;

public class Cup {
	private Water water;
	private CoffeePowder coffeePowder;
	private Suggar suggar;
	public Water getWater() {
		return water;
	}
	public void setWater(Water water) {
		this.water = water;
	}
	public CoffeePowder getCoffeePowder() {
		return coffeePowder;
	}
	public void setCoffeePowder(CoffeePowder coffeePowder) {
		this.coffeePowder = coffeePowder;
	}
	public Suggar getSuggar() {
		return suggar;
	}
	public void setSuggar(Suggar suggar) {
		this.suggar = suggar;
	}
	
	
	
}
