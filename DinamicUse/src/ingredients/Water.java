package ingredients;

public class Water {
	private int milliliters;

	public int getMilliliters() {
		return milliliters;
	}

	public void setMilliliters(int milliliters) {
		this.milliliters = milliliters;
	}
}
