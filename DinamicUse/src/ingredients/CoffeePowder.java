package ingredients;

public class CoffeePowder {
	private int gramms;

	public int getGramms() {
		return gramms;
	}

	public void setGramms(int gramms) {
		this.gramms = gramms;
	}
}
