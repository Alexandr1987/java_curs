package application;
import home.*;
import ingredients.*;
public class ApplicationCoffee {

	public static void main(String[] args) {

        Cup cup = new Cup();


        CoffeePowder coffeePowder = new CoffeePowder();
        coffeePowder.setGramms(80);
        Suggar suggar = new Suggar();
        suggar.setGramms(200);
        Water water = new Water();
        water.setMilliliters(20);


        cup.setCoffeePowder(coffeePowder);
        cup.setSuggar(suggar);
        cup.setWater(water);
        Table table = new Table(cup);
        Kitchen kitchen = new Kitchen(table) ;

        kitchen.print();

	}

}
