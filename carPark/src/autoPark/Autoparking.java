package autoPark;
import world.Car;
public class Autoparking {
    private byte freePlaces;
    private Car place1A;
    private Car place1B;
    private Car place1C;
    private Car place2A;
    private Car place2B;
    private Car place2C;

    public Autoparking(){
        this.freePlaces = -6;
    }

    public void parkCar(Car c,String  place){
        switch (place){
            case "1A":
                if (place1A == null) place1A = c;freePlaces++;break;
            case "1B":
                if (place1B == null) place1B = c;freePlaces++;break;
            case "1C":
                if (place1C == null) place1C = c;freePlaces++;break;
            case "2A":
                if (place2A == null) place2A = c;freePlaces++;break;
            case "2B":
                if (place2B == null) place2B = c;freePlaces++;break;
            case "2C":
                if (place2C == null) place2C = c;freePlaces++;break;
                default:
                    System.out.println("This "+place+" is busy by other car");
        }
    }
    public void printFreePlaces(){
        System.out.println("there are "+freePlaces);
    }

    public void printMap(){
            for (int x = 0; x < 3;x++) {
                System.out.println("----------------------");
                Car carA = null;
                Car carB = null;
                if (x == 0) {carA = place1A; carB = place2A;}
                else if (x == 1){carA = place1B; carB = place2B;}
                else {carA = place1C; carB = place2C;}
                    System.out.printf("| %-8s | %-8s |\n| %-8s | %-8s |\n",
                            carA != null ? carA.getModel() : "0", carB != null ? carB.getModel() : "0",
                            carA != null ? carA.getId() : "0", carB != null ? carB.getId() : "0");
                    System.out.println("----------------------");
                }
    }
}
