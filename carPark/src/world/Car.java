package world;

public class Car {
    private String model;
    private String id;

    public Car(String model,String id){
        setModel(model);
        setId(id);
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        if (model=="BMW" || model=="Mercedes"|| model=="Fiat")
        this.model = model;
        else System.out.println("Take away, this is not a car");
    }
    public void print(){
        System.out.printf("%s",model);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
