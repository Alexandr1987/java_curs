package main;
import world.Car;
import autoPark.Autoparking;
public class main {

	public static void main(String[] args) {
        Car car1 = new Car("BMW", "AB 123");
        Car car2 = new Car("Mercedes","K  222");
        Car car3 = new Car("Fiat","XY 567");

        Autoparking parking = new Autoparking();
        parking.parkCar(car2,"1A");
        parking.parkCar(car3,"2B");
        parking.parkCar(car1,"1C");
        parking.printMap();
        parking.printFreePlaces();

	}

}
