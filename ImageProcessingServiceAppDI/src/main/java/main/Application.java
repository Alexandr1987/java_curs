package main;

import java.io.IOException;

import behavior.DirectoryObserver;


public class Application {    
	public static void main(String[] args) throws InterruptedException, IOException  {

		DirectoryObserver directoryObserver = new DirectoryObserver();


		directoryObserver.observe("jpeg");
	}

}
