package aspects;

import java.io.File;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
@Aspect
public class TestAspect {
	private long lastTime;
	
	
	@Before("execution(public void behavior.DirectoryObserver.observe(String))")
	  public void logBeforeProcessing(JoinPoint jp) {
		lastTime =System.nanoTime();
		
		File theDir = new File("Images/processed/");
		if (!theDir.exists()){
		    theDir.mkdirs();
		}
		
	     System.out.println("STARTING ON: " +lastTime/1000000000.0 +"upon type "+ jp.getArgs()[0]);
	  }
	
	
	
	@After("execution(public void behavior.DirectoryObserver.observe(String))")
	  public void logAfterProcessing(JoinPoint jp) {
		long duration= System.nanoTime() - lastTime;
		long result  = (long) ((duration/1000000000.0) - this.lastTime);
	     System.out.println("Process finished upon: "+jp.getArgs()[0]+" in "+result );
	  }
	
	@Around("execution(* transform(String))")
	public void measureProcessingPerformance(ProceedingJoinPoint pJP) throws Throwable {
		long duration1= System.nanoTime() - lastTime;
		System.out.println(" >> " + pJP.getArgs()[0]);
		pJP.proceed();
		System.out.println(pJP.getArgs()[0] + " was processed in:"+duration1/1000000000.0);
	}
}
