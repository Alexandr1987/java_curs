package ajTools;



public aspect LoggingAspect {
	pointcut whenRandomMethoCalled():call( public void observe());
	
	
	private long lastTime;
	
	before():whenRandomMethoCalled(){
		lastTime =System.nanoTime();
		System.out.println("Reading STARTED!!!"+lastTime+" nSecs ");
	}
	
	after():whenRandomMethoCalled(){
		long duration= System.nanoTime() - lastTime;
		System.out.println("Reading FINISHED!!!"+duration/1000000.0+" miliSecs ");
	}
}
