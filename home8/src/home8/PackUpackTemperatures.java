package home8;
import java.util.Scanner;
public class PackUpackTemperatures {
	
	static String packTemperatures(int t1, int t2,int t3, int t4, int t5, int t6, int t7) {
		String data = "" + t1 + " " + t2 + " " + t3 + " " + t4 + " " + t5 + " " + t6 + " " + t7;
		return data;
	}
	
	static void avgTemperature(int t1, int t2,int t3, int t4, int t5, int t6, int t7) {
		int temp = (t1+t2+t3+t4+t5+t6+t7)/7;
		System.out.println("temperature: " + temp);
	}
	
	static void unpackTemperatures(String data) {
		Scanner in = new Scanner(data);
	    int a = in.nextInt();
	    int b = in.nextInt();
	    int c = in.nextInt();
	    int d = in.nextInt();
	    int e = in.nextInt();
	    int f = in.nextInt();
	    int g = in.nextInt();
	    System.out.println( "a = " + a );
	    System.out.println( "b = " + b );
	    System.out.println( "c = " + c );
	    System.out.println( "d = " + d );
	    System.out.println( "e = " + e );
	    System.out.println( "f = " + f );
	    System.out.println( "g = " + g );
	    avgTemperature(a,b,c,d,e,f,g);
	}
	
	
	public static void main(String[] args) {
		 unpackTemperatures( packTemperatures(-5, 5, 0, -5, 5, 0, -5) ) ;

	}

}
