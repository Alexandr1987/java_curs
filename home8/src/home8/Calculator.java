package home8;

public class Calculator {
		
	
	static void printResult(int result) {
		 System.out.printf("result: %08d\n",result);
	}
	
	static void printDivider() {
	    System.out.println("################");
	}
	
   static int add(int a, int b) {
	    int r = a + b;
	    return r;
	}
   static int sub(int a, int b) {
	   int d = a - b;
	    return d;
   }
   static int mul(int a, int b) {
	   int r = a * b;
	    return r;
   }
   static int div(int a, int b) {
	   int r = a / b;
	    return r;
   }
   static int pow(int v, int e) {
	   int res = 1;
	   for (int i=0; i<e; i++) {
		   res*=v;
	   }
	    return res;
   }
   
   
	
	
	public static void main(String[] args) {
//		  printResult(0);
//	      printResult(10);
//	      printResult(99999999);
//	      printResult(999999999);
//	      printResult(-100);
//	      
//	      printDivider();
//	      printResult(10);
//	      printDivider();
//	      
//	      printDivider();
	      printResult(add(1,0) );
	      printResult(sub(10,5) );
	      printResult(div(3,4) );
	      printResult(pow(5,3) );
	      printResult(mul(3,3) );
	      printDivider();
          printResult(add(1,0)+pow(2,3)*div(3,4)-sub(10,5));
	      printDivider();
	      
	      

	}

	

}
