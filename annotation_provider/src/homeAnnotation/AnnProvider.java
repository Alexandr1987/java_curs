package homeAnnotation;

import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Messager;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.TypeElement;
import javax.tools.Diagnostic.Kind;
@SupportedAnnotationTypes(value = {"homeAnnotation.Incapsulated"}) //FQCN for the annotation types
public class AnnProvider extends AbstractProcessor{
	private Messager messanger;
	
	@Override
	public synchronized void init(ProcessingEnvironment processingEnv) {
		super.init(processingEnv);
		messanger = processingEnv.getMessager();
	}
//	@Override
//	public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) { 
//		for(TypeElement te: annotations) {
//			Set<? extends Element> annotatedElements = roundEnv.getElementsAnnotatedWith(te);
//			for(Element e: annotatedElements) {
//				messanger.printMessage(Kind.ERROR,"Incaps proc found and running",e);
//				
//			}
//			 
//		}
//		
//		
//		
//		return true;
//	}
	@Override
	public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) { 
		Set<? extends Element> annotatedElements = roundEnv.getElementsAnnotatedWith(Incapsulated.class);
		for(Element annotatedElement: annotatedElements) {
			if(annotatedElement.getKind() != ElementKind.CLASS) {
				messanger.printMessage(Kind.ERROR,"The"+Incapsulated.class+"annotation can be used only at class level",annotatedElement);
				continue;
			}
			List<? extends Element> children = annotatedElement.getEnclosedElements();
			List<? extends Element> childFields = new ArrayList<>(children);
									childFields.removeIf(e -> e.getKind() != ElementKind.FIELD);
			List<? extends Element> childMethods = new ArrayList<>(children);
									childMethods.removeIf(e -> e.getKind() != ElementKind.METHOD || !e.getModifiers().contains(Modifier.PUBLIC));
									
			for(Element field: childFields) {
				Set<javax.lang.model.element.Modifier> modifiers = field.getModifiers();
				 if(!modifiers.contains(Modifier.PRIVATE)) {
					 messanger.printMessage(Kind.ERROR,"The"+field+"does not have a private modifier",field); 
					 break;
				 }
				 String fieldName = field.getSimpleName().toString();
				 String getterName = "get"+fieldName.substring(0,1).toUpperCase()+fieldName.substring(1);
				 String setterName = "set"+fieldName.substring(0,1).toUpperCase()+fieldName.substring(1);
				 Boolean foundGetter = false;
				 Boolean foundSetter = false;
				 for (Element method : childMethods ) {
					 if( method.getSimpleName().toString().equals(getterName)) foundGetter = true;
					 if( method.getSimpleName().toString().equals(setterName)) foundSetter = true;
				 }
				 if (!foundGetter || !foundSetter) {
					 messanger.printMessage(Kind.ERROR,"The"+field+"does not have an accesor methood",field); 
					 break;
				 }
			}
													
			 
		}
		
		
		
		return true;
	}

}
