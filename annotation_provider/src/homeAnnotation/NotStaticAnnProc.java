package homeAnnotation;


import java.util.List;
import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Messager;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.tools.Diagnostic.Kind;

@SupportedAnnotationTypes(value = {"homeAnnotation.NotStatic","homeAnnotation.NotStaticFields"}) //FQCN for the annotation types
public class NotStaticAnnProc extends AbstractProcessor{
	
	private Messager messager;
	@Override
	public synchronized void init(ProcessingEnvironment processingEnv) {
		super.init(processingEnv);
//		get messaging service object
		messager=processingEnv.getMessager();
	}

	@Override
	public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {  // any type "?" that exends typeElement
		
		
		Set<? extends Element> elements = roundEnv.getElementsAnnotatedWith(homeAnnotation.NotStatic.class);
		for(Element element:elements) {
			//get elements descendents
			List<? extends Element> childrenElements = element.getEnclosedElements();
			for(Element childElement:childrenElements) {
				if(childElement.getModifiers().contains(Modifier.STATIC)) {
					messager.printMessage(Kind.ERROR, "Class "+element+"does not corespond",element);
					messager.printMessage(Kind.ERROR, "This element cannot be static  ",childElement);
					break;
				}
			}
			
		}
		
		
		
		
		Set<? extends Element> elements2 = roundEnv.getElementsAnnotatedWith(homeAnnotation.NotStaticFields.class);
		
		for(Element element2:elements2) {
			//get elements descendents
			List<? extends Element> childrenElements2 = element2.getEnclosedElements();
			childrenElements2.removeIf(e -> e.getKind() != ElementKind.FIELD);
			for(Element childElement2:childrenElements2) {
				if(childElement2.getModifiers().contains(Modifier.STATIC)) {
					messager.printMessage(Kind.ERROR, "Class "+element2+"does not corespond222",element2);
					messager.printMessage(Kind.ERROR, "This element cannot be static 222 ",childElement2 );
					break;
				}
			}
			
		}
		return true;
	}

}
