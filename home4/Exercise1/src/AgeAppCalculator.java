import java.util.Scanner;

//  i.  nextDouble()	Reads a double value from the user
//  ii. �������� ��������� "AgeCalculatorApp" � ������� � ���� ��������� ��� ������� ���, 
//       � ���������� ������� ������ ���� ��� �������� � ���� 4 ������, 
//         ����� ���� ��������� ������� ��� ������� � �����!
public class AgeAppCalculator {

	public static void main(String[] args) {
		final int CURRENT_YEAR = 2019;
		int resultAge;
		
		
		Scanner birthYear = new Scanner(System.in);
		int yearOfBirth = birthYear.nextInt();
		resultAge = CURRENT_YEAR - yearOfBirth;
		System.out.println("Age: " + resultAge);

	}

}
