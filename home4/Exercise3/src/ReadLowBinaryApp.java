import java.util.Scanner;


public class ReadLowBinaryApp {

	public static void main(String[] args) {
		 final int VALID_PIN  = 1234;  
	       int scannedPin = 0; 
	       
	       Scanner myPin = new Scanner(System.in);
	       System.out.print("Enter first number of your pin (from 0 - 9): ");
	       int pinPos1 = 1000* myPin.nextInt();
	       System.out.print("Enter second number of your pin (from 0 - 9): ");
	       pinPos1 = pinPos1 + 100 * myPin.nextInt();
	       System.out.print("Enter third number of your pin (from 0 - 9): ");
	       pinPos1 = pinPos1 + 10 * myPin.nextInt();
	       System.out.print("Enter fourth number of your pin (from 0 - 9): ");
	       pinPos1 = pinPos1 + 1 * myPin.nextInt();
	       boolean finalResponse = pinPos1 == VALID_PIN;
	       System.out.println("Account accessible? "+ finalResponse);
	}

}