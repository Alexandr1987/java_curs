package main;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import product.Product;

public class MainApp {

	public static void main(String[] args) {

		
		List<Product> products = Arrays.asList(new Product("health","banana",20f,5),
											   new Product("second-hand","skirt",200f,1),
											   new Product("second-hand","BMW",20000f,1),
											   new Product("health","spagetti",50f,2),
											   new Product("new","jeans",200f,1));

			
			System.out.println(products);
			VATFunction vatfunction = product->{
				float priceNew = product.getPrice()+product.getPrice()*18/100;
				return new Product(product.getCategory(),product.getName(),priceNew,product.getQuantity());
				};
			System.out.println("---------------------------------------------------------");
			IsHealthPredicate healthPred = product ->product.getCategory().equals("health");
			System.out.println("---------------------------------------------------------");
			IsSHPredicate	isSHPredicate = product ->product.getCategory().equals("second-hand");
			System.out.println("---------------------------------------------------------");
			ConsolePrintConsumer consolePrintConsumer = product -> System.out.println(product);	
			
		    List<Product> modProds1 = products.stream()
		            .filter(healthPred::test)
	                .map(vatfunction::calculate)
	                .collect(Collectors.toList());

	
		    List<Product> modProds2 = products.stream()
		    		.filter(isSHPredicate::test)
	                .map(vatfunction::calculate)
	                .collect(Collectors.toList());
		    modProds1.forEach(consolePrintConsumer::consume);
		    System.out.println("---------------------------------------------------------");
		    modProds2.forEach(consolePrintConsumer::consume);
	}
}
@FunctionalInterface
interface VATFunction 
{ 
    Product calculate(Product product); 
} 
@FunctionalInterface
interface IsHealthPredicate 
{ 
    Boolean test(Product product); 
} 
@FunctionalInterface
interface IsSHPredicate 
{ 
    Boolean test(Product product); 
} 
@FunctionalInterface
interface ConsolePrintConsumer 
{ 
    void consume(Product product); 
}