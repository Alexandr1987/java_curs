package arrayList1;

import java.util.ArrayList;
import java.util.Random;

public class Ex1 {

	public static int rand(int min, int max)
	{
		if (min > max) {
			throw new IllegalArgumentException("Invalid range");
		}

		Random rand = new Random();

		return rand.ints(min, (max + 1))	// IntStream
					.findFirst()			// OptionalInt
					.getAsInt();			// int
	}
	
	public static void main(String[] args) {
		
		int min = 1900, max = 2019;
		  ArrayList<Integer> dates = new ArrayList<Integer>(); // << ??? 1.
		  for (int i = 0; i<10; i++) {
			  dates.add(rand(min, max));
			  
		  }
		  System.out.println(dates);
		  for( int d: dates ) {          // << ??? 3.
		      int y = 50; 
		      if ((max-d)<y) {System.out.println("dates:" + d );}
		      
		  }
		  
		  ArrayList young_dates = new ArrayList<Integer>();
		  for( int i = 0; i < dates.size(); i++ ){
			  int f =30;
			  if ((max-dates.get(i))<f) {
				  young_dates.add(dates.get(i));
			  }
			  
		  }
		  System.out.println("young_dates:" + young_dates);
		  
		  while(young_dates.size()>0) {
			  System.out.println(young_dates);
			  young_dates.size();
			  young_dates.remove(1); 
			  System.out.println(young_dates + "removed 1");
		  }
	}

}
