package main;
import world.*;
public class main {

	public static void main(String[] args) {
		Box bigBox= new Box(new Hammer(2.5f));
		Box smallBox= new Box(new Axe(3.2f));
		bigBox.getTool().printInfo();
		smallBox.getTool().printInfo();
	}

}
