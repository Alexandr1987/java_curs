package annProvider;

import java.util.List;
import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Messager;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.lang.model.element.Element;
//import javax.lang.model.element.ElementKind;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.tools.Diagnostic.Kind;

@SupportedAnnotationTypes(value = {"annProvider.PrivateFields"})
public class AnnLogic extends AbstractProcessor{
	private Messager messager;
	@Override
	public synchronized void init(ProcessingEnvironment processingEnv) {
		super.init(processingEnv);
		messager=processingEnv.getMessager();
	}
	@Override
	public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {  
			
			
			Set<? extends Element> elements = roundEnv.getElementsAnnotatedWith(annProvider.PrivateFields.class);
			for(Element element:elements) {
				List<? extends Element> childrenElements = element.getEnclosedElements();
				for(Element childElement:childrenElements) {
					if(childElement.getModifiers().contains(Modifier.PRIVATE)) {
//						messager.printMessage(Kind.OTHER, "Class "+element+" does corespond ",element);
						messager.printMessage(Kind.OTHER, "This element is private  ",childElement);
//						break;
					}
				}
				
			}
		return true;
	}


}
