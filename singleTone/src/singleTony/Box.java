package singleTony;

public class Box {
	private static Box instance;
	private Object value;

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "Box [value=" + value + "]";
	}
	
	private Box (Object value) {
		try {
			Thread.sleep(2000);
		}catch(InterruptedException ex) {
			ex.printStackTrace();
		} this.value = value;
	}
	public static Box getInstance(Object value) {
        if (instance == null) {
            instance = new Box(value);
        }
        return instance;
	}
}
