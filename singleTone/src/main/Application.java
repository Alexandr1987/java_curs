package main;

import singleTony.Box;

public class Application {

	public static void main(String[] args) {
		System.out.println("If you see the same value, then singleton was reused (yay!)" + "\n" +
                "If you see different values, then 2 singletons were created (booo!!)" + "\n\n" +
                "RESULT:" + "\n");
        Box singleton = Box.getInstance("FOO");
        Box anotherSingleton = Box.getInstance("BAR");
        System.out.println(singleton.getValue());
        System.out.println(anotherSingleton.getValue());

	}

}
