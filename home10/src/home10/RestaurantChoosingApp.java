package home10;
public class RestaurantChoosingApp {
    public static void main(String[] args) {
    	VeganRestaurant.Info.printName();
    	VeganRestaurant.Info.printAddress();
    	VeganRestaurant.Menu.getPrice();
    	VeganRestaurant.Menu.getPrice(5);
    	VeganRestaurant.SpecialMenu.getPrice();
    	VeganRestaurant.SpecialMenu.getPrice(10);
    	SushiRestaurant.Info.printName();
    	SushiRestaurant.Info.printAddress();
    	SushiRestaurant.Menu.getPrice();
    	SushiRestaurant.Menu.getPrice(5);
    	SushiRestaurant.SpecialMenu.getPrice();
    	SushiRestaurant.SpecialMenu.getPrice(10);
    }
}

// ###################### RESTAURANT CLASSES ##########################
class Restaurant {
	static class Info {
	    static void printName() {
	    	System.out.println("No Name");
	    }
	    static void printMenu() {
	    	System.out.println("No Adress");
	    }
	    static void printAddress() {
	    	System.out.println("No Adress");
	    }
	  }
	  static class Menu {
	    static double getPrice() {
	    	return 100.0;
	    }
	   static double getPrice(int portions) {
		   return 1f;
	    }
	    }
	  static class SpecialMenu {}
	}



class VeganRestaurant extends Restaurant {
  static class Info {
	    static void printName() {
	    	System.out.println("########### Sushi Mini Restaurant ###########");
	    }
	    static void printMenu() {
	    	
	    }
	    static void printAddress() {
	    	System.out.println("-- DownTown Str 88/A, Minicity             --");
	    }
  }
  static class Menu {
      static double getPrice() {
    	    int x = 1; 
	    	double v = 100f; 
	    	System.out.printf("MENU           x%d  portion         %.2f MDL\n",x,v);
	    	return v;
      }
      static double getPrice(int portions) {
    	  int portionsThis = portions;
    	  int percentOfDiscount = 10;
    	  double price = 100.00;
    	  double endPrice = (portions * price)-((portions * price)/(float)percentOfDiscount);
    	  System.out.printf("MENU           x%d  portion%s   %.2f MDL\n",portionsThis,"(-10%)",endPrice);
    	  return endPrice;
      }
    }
  static class SpecialMenu {
	  static double getPrice() {
  	    int x = 1; 
	    	double v = 300f; 
	    	System.out.printf("MENU           x%d  portion         %.2f MDL\n",x,v);
	    	return v;
    }
    static double getPrice(int portions) {
      int portionsThis = portions;
  	  int percentOfDiscount = 5;
  	  double price = 300.00;
  	  double endPrice = (portions * price)-((portions * price)/percentOfDiscount);
  	  System.out.printf("MENU          x%d  portion%s  %.2f MDL\n",portionsThis,"(-20%)",endPrice);
  	  return endPrice;
    }
  }
}

class SushiRestaurant extends Restaurant {
	 static class Info {
		    static void printName() {
		    	System.out.println("########### Vegan Pure Restaurant ###########");
		    }
		    static void printMenu() {
		    	
		    }
		    static void printAddress() {
		    	System.out.println("-- UpTown Str 77/B, Maxicity               --");
		    }
	  }
	  static class Menu {
	      static double getPrice() {
	    	    int x = 1; 
		    	double v = 50f; 
		    	System.out.printf("MENU           x%d  portion          %.2f MDL\n",x,v);
		    	return v;
	      }
	      static double getPrice(int portions) {
	    	  int portionsThis = portions;
	    	  int percentOfDiscount = 20;
	    	  double price = 50.00;
	    	  double endPrice = (portions * price)-((portions * price)/(float)percentOfDiscount);
	    	  System.out.printf("MENU           x%d  portion%s    %.2f MDL\n",portionsThis,"(-5%)",endPrice);
	    	  return endPrice;
	      }
	    }
	  static class SpecialMenu {
		  static double getPrice() {
	  	    int x = 1; 
		    	double v = 200f; 
		    	System.out.printf("MENU           x%d  portion         %.2f MDL\n",x,v);
		    	return v;
	    }
	    static double getPrice(int portions) {
	      int portionsThis = portions;
	  	  int percentOfDiscount = 10;
	  	  double price = 200.00;
	  	  double endPrice = (portions * price)-((portions * price)/percentOfDiscount);
	  	  System.out.printf("MENU          x%d  portion%s  %.2f MDL",portionsThis,"(-10%)",endPrice);
	  	  return endPrice;
	    }
	  }
}