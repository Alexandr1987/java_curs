package home10;
import java.util.Random;
public class TemperatureMeteoApp {

	public static void main(String[] args) {
		HumidityProvider.getCurrentTemperature(498);
		HumidityProvider.getCurrentHumidity(498);
	}

}
class OpenMeteoProvider {
    static int getCurrentTemperature( int countryCode ){
    	int minTemp = -50;
    	int maxTemp =50;
    	int differenceTemp = maxTemp - minTemp;
        Random randomizeTemperature = new Random();
        int temperature = randomizeTemperature.nextInt(differenceTemp + 1);
        temperature += minTemp;
    	switch (countryCode) {
		  case 498:
		    System.out.print("Moldova " + temperature + "\u00B0" +"C ");
		    break;
		  case 642:
		    System.out.print("Romania " + temperature + "\u00B0" +"C ");
		    break;
		  case 804:
			System.out.print("Ukraine " + temperature + "\u00B0" +"C ");
			break;
		  case 100:
			System.out.print("Bulgaria " + temperature + "\u00B0" +"C ");
			break;
		  case 792:
			System.out.print("Turkey " + temperature + "\u00B0" +"C ");
			break;
		  default:
		    System.err.print( "Can't provide data for this country! ");
		}
    	return temperature;
    }

 }

class HumidityProvider extends OpenMeteoProvider {
	static byte getCurrentHumidity( int countryCode ) {
	        int humidity = 0;
	        if (countryCode == 498 || countryCode == 642 || countryCode == 804 || countryCode == 100 || countryCode == 792) {
	            int minHumidity = 0;
	            int maxHumidity = 100;
	            int differenceHumidity = maxHumidity - minHumidity;
	            Random random = new Random();
	            humidity = random.nextInt(differenceHumidity + 1);
	            humidity += minHumidity;
	            System.out.println("Air humidity " + humidity + " %");
	            return (byte)humidity;
	        } else {
	            System.err.println("Can't provide data for this country!");
	            return (byte)humidity;
	        }
	}
}




