package persistance;

import java.util.ArrayList;
import java.util.List;

import data.CloneObjectCat;
import data.Cat;
import data.FelineInterface;

@SuppressWarnings("all")

public class FelineRepository implements FelineRepositoryInterface{
	private List<FelineInterface> felines;
	ArrayList<FelineInterface> data = new ArrayList();
	public FelineRepository () {
		felines = new ArrayList<>();
	}
	@Override
	public boolean save(FelineInterface feline) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean update(FelineInterface feline) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean delete(FelineInterface feline) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<FelineInterface> all() {
		// TODO Auto-generated method stub
		return felines;
	}

	@Override
	public FelineInterface get(Integer Id) {
		for (FelineInterface f:felines) {
			if(f.getId()==Id) {
				return f;
			}
		}
		return null;
	}
	
	public void generate() {
		felines.add(new Cat(1,"Bars","Siberian",2000));
		felines.add(new Cat(2,"Mars","Egiptean",2005));
		felines.add(new Cat(3,"Fars","Arabic",2001));
		felines.add(new Cat(4,"Dars","Englez",2009));
		felines.add(new Cat(5,"Bars","Mongolez",2002));
		felines.add(new Cat(6,"Bars","Norvegian",2003));
	}
	@Override
	public FelineInterface getByName(String name) {
		
        for (FelineInterface feline : felines) {
            if (feline.getName().equals(name)) {
//            	Cat cat = (Cat) feline;
            	CloneObjectCat newCat = (CloneObjectCat) feline;
            	
            	data.add((FelineInterface) newCat);
                return (FelineInterface) data;
            }
        }
		return null;
	}
	@Override
	public FelineInterface getByRace(String race) {
		for (FelineInterface f:felines) {
			if(f.getRace()==race) {
				return f;
			}
		}
		return null;
	}
}
