package persistance;

import java.util.List;

import data.FelineInterface;




public interface FelineRepositoryInterface {
	public boolean save(FelineInterface feline);
	public boolean update(FelineInterface feline);
	public boolean delete(FelineInterface feline);
	public List <FelineInterface>all();
	public FelineInterface get(Integer Id);
	public FelineInterface getByName(String name);
	public FelineInterface getByRace(String race);
}
