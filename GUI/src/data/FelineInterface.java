package data;

public interface FelineInterface {
	public Integer getId() ;
	public void setId(Integer id);
	public void setName(String name);	
	public String getName(); 
	public void setRace(String race); 
	public String getRace();	
	public void setYear(int year);	
	public int getYear();
	
	
}
