package data;

public class Cat implements FelineInterface {
 public Integer Id;
 public String name;
 public String race;
 public int year;
 
 

 
public Integer getId() {
	return Id;
}
public void setId(Integer id) {
	Id = id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getRace() {
	return race;
}
public void setRace(String race) {
	this.race = race;
}
public int getYear() {
	return year;
}
public void setYear(int year) {
	this.year = year;
}

public Cat() {}

public Cat(Integer Id, String name, String race, int year) {
	super();
	this.Id = Id;
	this.name = name;
	this.race = race;
	this.year = year;
}
@Override
public String toString() {
	return "Cat [Id="+Id+", name=" + name + ", race=" + race + ", year=" + year + "]";
}

 
 
}
