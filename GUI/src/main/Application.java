package main;
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.BoxLayout;
import javax.swing.JTextField;
import data.Cat;
import data.FelineInterface;
import java.util.ArrayList;
import java.util.List;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
@SuppressWarnings("all")
public class Application {
private static List<String> catsNameList = new ArrayList<>(); 
public static ArrayList<FelineInterface> feline = new ArrayList<>();
	public static void main(String[] args) {
		initGUI();
	}
	public static void initGUI () {
		JButton saveBtn = new JButton("SAVE");
		JTextField nameInput = new JTextField();
		JTextField raceInput = new JTextField();
		JTextField yearInput = new JTextField();
		JFrame window =new JFrame ("Cats database App");		
		window.getContentPane().add(nameInput);
		window.getContentPane().add(raceInput);
		window.getContentPane().add(yearInput);
		window.getContentPane().add(saveBtn);		
		window.getContentPane().setLayout(new BoxLayout(window.getContentPane(),
																BoxLayout.PAGE_AXIS));
		window.setBounds(100,100,400,300);
		window.show();		
		saveBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {				
//				feline.add(new Cat(nameInput.getText(),raceInput.getText(),Integer.parseInt(yearInput.getText())));
				feline.add(new Cat(null,nameInput.getText(),
									raceInput.getText(),
									Integer.parseInt(yearInput.getText())));
//				catsNameList.add(nameInput.getText());
//				catsNameList.add(raceInput.getText());
//				catsNameList.add(yearInput.getText());
				printList2();
				nameInput.setText("");
				raceInput.setText("");
				yearInput.setText("");
			}
		});
	}
//	public static void printList() {
//		System.out.println("["+catsNameList.size()+"]");
//		for(int i =0; i<catsNameList.size();i++) {
//			System.out.println(catsNameList);
//		}
//		System.out.println(String.join(",",catsNameList));
//	}
	public static void printList2() {
		System.out.println("["+feline.size()+"]");
		for(int i =0; i<feline.size();i++) {
			System.out.println(feline);
		}
//		System.out.println(String.join(",",feline));
	}
}
