package student.example.security.jwt;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.interfaces.DecodedJWT;



public class JWTProvider {
	private final static String key = "admin";
	
	public static String createSimpleToken(String username, String userUUID) {
		String token = null;
		Algorithm algorithmHS = Algorithm.HMAC256(key);
		
		try {
			 token = JWT.create()
					 	.withIssuer("minisocial.example")
					 	.withClaim("name", username)
					 	.withClaim("uuid", userUUID)
					    .sign(algorithmHS)
					    ;
		} catch (JWTCreationException exception) {
			// TODO: handle exception
		}
		
		 return token;
	}
	
	
	public static DecodedJWT verifySimpleToken(String token) throws Exception{
		
		Algorithm algorithmHS = Algorithm.HMAC256(key);
		JWTVerifier verifier = JWT.require(algorithmHS)
								.withIssuer("minisocial.example")
//								.withClaim("uuid", userUUID)
								.build();
		DecodedJWT jwt = verifier.verify(token);
		return jwt;

	}
//	public static String createSecureToken() {
//		String token = null;
//		Algorithm algotithmHS = Algorithm.("secret");
//		 return token;
//	}
}
