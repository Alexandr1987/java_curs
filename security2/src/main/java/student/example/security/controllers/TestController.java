package student.example.security.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {
		@GetMapping("/")
		public String publicPage() {
			System.err.println("test controller worked");
			return "All the users can access the page";
		}
		
		@GetMapping("/profile")
		public String profilePage() {
			System.err.println("test controller worked");
			return "only authenticated users can access the page";
		}
		@GetMapping("/login")
		public String loginPage() {
			System.err.println("test controller worked");
			return "Enter your name and pass";
		}
}
