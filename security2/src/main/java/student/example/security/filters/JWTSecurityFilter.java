package student.example.security.filters;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import static org.springframework.util.StringUtils.isEmpty;
import student.example.security.jwt.JWTProvider;


@SuppressWarnings("all")
@Component
public class JWTSecurityFilter extends OncePerRequestFilter{
	@Override 
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException{
		System.err.println("Filter JWT ACTIVE");
		final String header = request.getHeader(HttpHeaders.AUTHORIZATION);
		if(isEmpty(header)||!header.startsWith("Bearer ")) {
			chain.doFilter(request, response);
			return;
		}
		final String token = header.split(" ")[1].trim();
		System.err.println(token);
		try {
			String TokenClaimedUserName = JWTProvider.verifySimpleToken(token).getClaim("name").toString();
			System.err.println("User claims its" + TokenClaimedUserName);
			if(TokenClaimedUserName.equals("admin")) {
				UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(null, null);
				authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
				SecurityContextHolder.getContext().setAuthentication(authentication);
			}

			chain.doFilter(request, response);
		}catch(Exception e) {
			e.printStackTrace();
			chain.doFilter(request, response); 
			return;
		}
	}
}
