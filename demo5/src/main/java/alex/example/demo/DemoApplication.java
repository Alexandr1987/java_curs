package alex.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
//	@RequestMapping(path = "hello/{idOne}/{idTwo}", method = RequestMethod.GET)
//	public String hello(@PathVariable int idOne, @PathVariable int idTwo) {
//		int result = idOne * idTwo;
//	return "<html>\n" + "<head><title>Welcome</title></head>\n" +
//	          "<body>\n" + "<H1>"+"Metoda: hello/{idOne}/{idTwo}"+"<H1>"+"Rezultatul Inmultirii: "+result+"</H1>\n" + "</body>\n" + "</html>";
//	}
	@GetMapping("/hello2")
	public String hello2(@RequestParam(value = "nameO", defaultValue = "0") int name1, @RequestParam(value = "nameT", defaultValue = "0") int name2) {
		int res2 =name1+name2;
	return "<html>\n" + "<head><title>Welcome</title></head>\n" +
			"<body>\n" +"Rezultatul sumei: "+res2+"</H1>\n" + "</body>\n" + "</html>";
	}

	
}
