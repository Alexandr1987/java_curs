package clazz;

import annot.DefaultValue;
@DefaultValue
public class Annot {
	@DefaultValue
	   private int xyz;

	public int getXyz() {
		return xyz;
	}
	public void setXyz(int xyz) {
		this.xyz = xyz;
	}

	public Annot(int xyz) {
		this.xyz = xyz;
	}

	public Annot() {
		
	}
	@Override
	public String toString() {
		return "Annot [xyz=" + xyz + "]";
	}
	   
	   
}
