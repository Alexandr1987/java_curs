package tool;

import java.io.IOException;

import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass; // wrapper for class
import javassist.CtConstructor;
import javassist.CtNewConstructor;
import javassist.NotFoundException;

public class Transformer {
	
	
	public static Class<?> transform() throws NotFoundException, CannotCompileException, IOException {
	//  get the class manipulation tool
		ClassPool pool = ClassPool.getDefault();
		pool.appendClassPath("D:\\mavenplay\\CodeTransform\\bin");
	// .appendClassPath - in case of errors
	// load desired class
		CtClass cc = pool.get("original.Box");
		CtConstructor ccon = cc.getDeclaredConstructor(null);
	// remove it
		cc.removeConstructor(ccon);
	// create a new default constructor
		CtConstructor cstructor =  CtNewConstructor.defaultConstructor( cc	) ;
	// modify construct behavior
		cstructor.setBody("this.id=100;");
	// add constructor to class
		cc.addConstructor(cstructor);
	// add the modified class
		return cc.toClass();
	}
}
