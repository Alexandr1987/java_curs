package box;
@SuppressWarnings("all")
public class Book implements ThingInteface{

	private String name;
	private String authorName;
	private String title;
	private Integer pages;
	private Integer publishingYear;
	@Override
	public String getName() {
		return name;
	}
	@Override
	public void setName(String name) {
		this.name = name;
	}
	public String getAuthorName() {
		return authorName;
	}
	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Integer getPages() {
		return pages;
	}
	public void setPages(Integer pages) {
		this.pages = pages;
	}
	public Integer getPublishingYear() {
		return publishingYear;
	}
	public void setPublishingYear(Integer publishingYear) {
		this.publishingYear = publishingYear;
	}
	public Book(String name, String authorName, String title, Integer pages, Integer publishingYear) {
		super();
		this.name = name;
		this.authorName = authorName;
		this.title = title;
		this.pages = pages;
		this.publishingYear = publishingYear;
	}
	
	public Book() {}
	@Override
	public String toString() {
		return "Book [name=" + name + ", authorName=" + authorName + ", title=" + title + ", pages=" + pages
				+ ", publishingYear=" + publishingYear + "]";
	}
	
	
	
}
