package box;
@SuppressWarnings("all")
public class Journal implements ThingInteface{
    private String name;
    private Integer pages;
    private String title;
    private String category;
    @Override
	public String getName() {
		return name;
	}
    @Override
	public void setName(String name) {
		this.name = name;
	}
	public Integer getPages() {
		return pages;
	}
	public void setPages(Integer pages) {
		this.pages = pages;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public Journal(String name, Integer pages, String title, String category) {
		super();
		this.name = name;
		this.pages = pages;
		this.title = title;
		this.category = category;
	}

	public Journal() {}
	@Override
	public String toString() {
		return "Journal [name=" + name + ", pages=" + pages + ", title=" + title + ", category=" + category + "]";
	}
	
	
}
