package box;
@SuppressWarnings("all")
public class Box{

	private ThingInteface content;  // ii Nu am un raspuns! ? crearea obiectului "content" de tip ThingInteface

	public Box(ThingInteface content) {
		super();
		this.content = content;
	}
	
	public Box() {
		
	}

	public ThingInteface getContent() {
		return content;
	}

	public void setContent(ThingInteface content) {
		this.content = content;
	}

	@Override
	public String toString() {
		return "Box [content=" + content + "]";
	}
	
	
}
