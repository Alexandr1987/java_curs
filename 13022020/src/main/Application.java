package main;


import box.*;
@SuppressWarnings("all")
public class Application {

	public static void main(String[] args) {
        Box firstBox = new Box();
        Box secondBox = new Box();
        
        ThingInteface book = new Book("Book", "Dorin Esinenco","Java Almanah",300,2020);
        ThingInteface journal = new Journal("Journal",30,"SF","Sciente fiction");
        
        firstBox.setContent(book);
        secondBox.setContent(journal);
        
        System.out.println(firstBox);
        System.out.println(secondBox);
	}
	
}
