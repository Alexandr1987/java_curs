package alex.examp.es;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


import java.util.*;


@SuppressWarnings("all")
@RestController
public class SubscriberController {
	
	
	@Autowired
	SubscriberRepository subsRepo;
	
	
	@GetMapping("/subscribers")
	public List<Subscriber> index() {
		List<Subscriber> subscriber = subsRepo.data();
		return subscriber;
		}

//	@GetMapping("/subscribers")
//	public String getSubscribersEmails() {		
//		return "fsegswgswgewe";
//		}

}
