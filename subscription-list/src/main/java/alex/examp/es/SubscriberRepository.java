package alex.examp.es;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
@Repository
public class SubscriberRepository {

@Autowired
JdbcTemplate jdbcTemplate;
public List<Subscriber> data(){
	List<Subscriber> subscr = this.jdbcTemplate.query(
	        "select id, name, email from public.subscribers",
	        new RowMapper<Subscriber>() {
	            public Subscriber mapRow(ResultSet rs, int rowNum) throws SQLException {
	            	Subscriber subsc = new Subscriber();
	            	subsc.setId(rs.getInt("id"));
	            	subsc.setName(rs.getString("name"));
	            	subsc.setEmail(rs.getString("email"));
	                return subsc;
	            }
	        });
	return subscr;
}
}
