package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import main.CLIstart;
import main.GUIstart;

class ArchitectureTest {
// this test checks if app has CLI and GUI
	@Test
	void CheckBothCLIandGUI() {
		try {
			Class.forName("main.CLIstart");
			Class.forName("main.GUIstart");
		} catch (ClassNotFoundException e) {
			
			fail("The app doesnt have both GUI and CLI");
		}
	}
// check method main
	@Test
	void CheckMethodMain()  throws NoSuchMethodException,
	SecurityException{
		try {
			Class.forName("main.CLIstart").getMethod("main",String[].class);
			Class.forName("main.GUIstart").getMethod("main",String[].class);
		} catch (ClassNotFoundException e) {
			
			fail("The app doesnt have both GUI and CLI");
		} catch (NoSuchMethodException e) {
			fail("The app doesnt have  method");
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
