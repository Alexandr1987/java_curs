package collections;

public class SimpleList {
    // properties
    private int length;
    private Box first;

    // private data type
		    private class Box {
		      int value;
		      Box next;
			public Box( int value) {
				
				this.value = value;
				}
			public int getValue() {
				return value;
				}
			
		    }
    
    
    public int getLength() {
		return length;
	}

	// constructor
    public SimpleList() {
      length = 0;
      first = null;
    }

    public void print() {
    	Box current = first;
    	if(getLength() > 0) {
    		for (int i=0; i <getLength(); i++) {
    			System.out.println(current.value);
    			current = current.next;
    		}
    	}else System.out.println("list empty");
    }
    
    public int get(int index) {
    	
    	if(index<=getLength()) {
    		Box current = first;
    		for (int i=0; i <index; i++) {    			
    			current = current.next;    			
    		}
    		System.out.println(current.value);
    	}else System.out.println("no data");
    	return index;
    }
    

    public void add( int value ) {
      Box current = first;
      if( length == 0 ) {
         first = new Box( value );
      } else {
        for(int i = 0; i < length - 1; i++) {
          current = current.next; 
        }
        current.next = new Box( value );
      }
      length++;
    }
}
