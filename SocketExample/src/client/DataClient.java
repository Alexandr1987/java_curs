package client;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.List;

public class DataClient {
	
private Socket clientSocket;
private List <Integer> data1;

public List<Integer> getData1() {
	return data1;
}

public void setData1(List<Integer> data1) {
	this.data1 = data1;
}

public DataClient(List<Integer> data1) {
	super();
	this.data1 = data1;
}

@Override
public String toString() {
	return "DataClient [data=" + data1 + "]";
}

public String stringTransform() {
//	System.out.println(">>>>"+getData1().size());
	StringBuilder str = new StringBuilder();
	String strFin = null;
	for(int i:getData1()) {
		//System.out.println(">#>>"+i);
		str.append(i+",");
		strFin = str.substring(0, str.length() - 1);
//		System.out.println("str >#>#>"+strFin);
	}
	return strFin;
}

public void initComm(String host, int port) throws IOException, InterruptedException {
	clientSocket =  new Socket(host,port);
	System.out.println("Client opened for comunication: ");
	transferData();
	Thread.sleep(10000);
	reciveData(); 
}


public void transferData() throws IOException{	
	System.out.println("Client >>> sending");	
	DataOutputStream dout = new DataOutputStream(clientSocket.getOutputStream());
	String x = (String)stringTransform();
	System.out.println("client converted data: "+x);
	dout.writeUTF(x);
	dout.close();  
 
	System.out.println(">>>Client: Data sended");
//	dout.close();
}

public void reciveData() throws IOException {
	DataInputStream dis = new DataInputStream(clientSocket.getInputStream());
	System.out.println(">>>Client: Data recived"+(String) dis.readUTF());
//	dis.close();
}

}
