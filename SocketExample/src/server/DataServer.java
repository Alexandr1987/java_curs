package server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class DataServer  {
	private ServerSocket serverSocket;
	private Socket clientSocketServer;

	public DataServer() {}

	public ServerSocket getServerSocket() {
		return serverSocket;
	}

	public void setServerSocket(ServerSocket serverSocket) {
		this.serverSocket = serverSocket;
	}
	
   public void startServer(int port) throws IOException, InterruptedException {
			   System.out.println("Server started : ");
			   serverSocket = new ServerSocket(port);
			   System.out.println("Server waiting:");
			   Socket clientSocketServer  = serverSocket.accept();	
			   System.out.println("clientSocketServer ss.accepted at: "+clientSocketServer);
			   Thread.sleep(6000);
			   dataArrival();			   
			   System.out.println("Data arrived");
			   dataSender(dataArrival());
	   
   }
	
	public String dataArrival()throws IOException{
		System.out.println("Data recived !!!");
		try {		
			DataInputStream dis2 = new DataInputStream(clientSocketServer.getInputStream());
			String data = (String)dis2.readUTF();
			return data;
		}catch (Exception e) {
				e.printStackTrace();
		}

//		String data="1,2,3";
		System.out.println("$$$$$$$$$$$$$$$$$");
		return "0,10,2";
		
	}
	public void dataSender(String data) throws IOException {
		System.out.println("Data in dataSender !!!" + data);
		DataOutputStream dout2 = new DataOutputStream(clientSocketServer.getOutputStream());
		dout2.writeUTF((String)dataProcessor(data));
		dout2.flush();
		System.out.println("Server : Data sended");
	
//		dout.close();
		
	}
	
 public String dataProcessor(String data) {
		int suma = 0;
		int numitor = 0;
		int rezultat=0;
		for (String numar : data.split(",") ) {
			suma += Integer.parseInt(numar);
			numitor ++;
			rezultat = suma/numitor;
		}
		
	 return  "media aritmetica: "+ rezultat;
 }
 
}
