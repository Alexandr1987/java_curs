package student.examples.minisocial.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import student.examples.minisocial.domain.entities.Comment;
import student.examples.minisocial.services.CommentService;

@RestController
@RequestMapping("/comments")
public class CommentController {
	
	@Autowired
	CommentService commentService;
	
	@PostMapping("/create")
	public String create(@RequestBody Comment comment){
		commentService.save(comment);
		return "Comment: " + comment.toString() + " created";
	}
	
	@GetMapping("/all")
	public List<Comment>  index() {
		 return commentService.getCommentsList();
	}

	@GetMapping("/show/{id}")
	public Comment show(@PathVariable int id) {
		return commentService.getCommentById(id);
	}

	@DeleteMapping("/delete/{id}")
	public String destroy(@PathVariable int id) {
		commentService.deleteCommentById(id);
		return "Comment with id=" + id + " deleted!";
	}


	@PutMapping(value = "/update/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public String update(@RequestBody Comment comment, @PathVariable int id) {
		commentService.updateComment(id, comment);
		return "Comment with id=" + id + " updated!";
	}
}
