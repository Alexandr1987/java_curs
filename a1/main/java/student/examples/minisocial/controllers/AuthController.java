package student.examples.minisocial.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import student.examples.minisocial.domain.entities.User;
import student.examples.minisocial.services.AuthService;

public class AuthController {
	
	@Autowired
	AuthService authService;
	
	@PostMapping(value = "/register", consumes = MediaType.APPLICATION_JSON_VALUE)
	public Boolean register(@RequestBody User user) {
			authService.register(user);
			return null;
		}
	
	@PostMapping(value = "/login", consumes = MediaType.APPLICATION_JSON_VALUE)
	public Boolean login(@RequestBody String nickName, @RequestBody String password) {
		authService.login(nickName, password);
			return null;
		}
	
	
	@GetMapping(value = "/logout")
	public Boolean logout() {
		authService.logOut();
			return null;
		}
}
