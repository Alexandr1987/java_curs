package student.examples.minisocial.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import student.examples.minisocial.domain.entities.Post;
import student.examples.minisocial.services.PostService;


@RestController
@RequestMapping("/posts")
public class PostController {
	@Autowired
	PostService postService;
	@PostMapping(value="/new", consumes=MediaType.APPLICATION_JSON_VALUE)
	public String create(@RequestBody Post post){
		postService.save(post);
		return "Post: " + post.toString() + " created";
	}
	
	@GetMapping("/all")
	public List<Post>  index() {
		 return postService.getPostsList();
	}

	@GetMapping("/show/{id}")
	public Post show(@PathVariable int id) {
		return postService.getPostById(id);
	}

	@DeleteMapping("/delete/{id}")
	public String destroy(@PathVariable int id) {
		postService.deletePostById(id);
		return "Post with id=" + id + " deleted!";
	}


	@PutMapping(value = "/update/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public String update(@RequestBody Post post, @PathVariable int id) {
		postService.updatePost(id, post);
		return "Comment with id=" + id + " updated!";
	}
}
