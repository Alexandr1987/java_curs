package student.examples.minisocial.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import student.examples.minisocial.dao.UserRepository;
import student.examples.minisocial.domain.entities.User;
@Service
public class UserService {
	@Autowired
	public UserRepository userRepository;

	public List<User> getUsersList() {
		return userRepository.findAll();
	}

	public User getUserById(int id) {
		return userRepository.getOne(id);
	}

	public void deleteUserById(int id) {
		userRepository.deleteById(id);
	}

	public void saveUser(User user) {
		userRepository.save(user);
	}

	public void updateUser(int id, User user) {
		userRepository.deleteById(id);
		user.setId(id);
		userRepository.save(user);
	}

//	public List<User> getUsersFriendsList(int id) {
//		Optional<User> userResult = userRepository.findById(id);
//		
//		if(userResult.isPresent()) {
//			return userResult.get().getFriendTo();
//		}
//		
//		return null;
//	}
//
//	public void addUserFriend(int id, int id2) {
//	 List<User> test = userRepository.getOne(id).getFriendTo();
//		 for (User looser : test) {
//			if(id2 != looser.getId()) {
//				test.add(userRepository.getOne(id2));
//			}
//		 }		
//	 }
//	
//	public void removeUserFriend(int id, int id2) {
//		 List<User> test = userRepository.getOne(id).getFriendTo();
//			 for (User looser : test) {
//				if(id2 == looser.getId()) {
//					test.remove(userRepository.getOne(id2)); // SOMETHING MISSING
//				}
//			 }		
//		 }
	

}
