package student.examples.minisocial.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import student.examples.minisocial.dao.PostRepository;
import student.examples.minisocial.domain.entities.Post;

@Service

public class PostService {
	@Autowired
    PostRepository postRepository;
	
	public void save(Post post) {
		postRepository.save(post);
	}
	
	public List<Post> getPostsList() {
		return postRepository.findAll();
	}

	public Post getPostById(int id) {
		return postRepository.getOne(id);
	}

	public void deletePostById(int id) {
		postRepository.deleteById(id);
	}

	public void updatePost(int id, Post post) {
		postRepository.deleteById(id);
		post.setId(id);
		postRepository.save(post);
	}
}
