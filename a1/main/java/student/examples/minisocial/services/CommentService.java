package student.examples.minisocial.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import student.examples.minisocial.dao.CommentRepository;
import student.examples.minisocial.domain.entities.Comment;
@Service
public class CommentService {
	@Autowired
    CommentRepository commentRepository;
	
	public void save(Comment comment) {
		commentRepository.save(comment);
	}
	
	public List<Comment> getCommentsList() {
		return commentRepository.findAll();
	}

	public Comment getCommentById(int id) {
		return commentRepository.getOne(id);
	}

	public void deleteCommentById(int id) {
		commentRepository.deleteById(id);
	}

	public void updateComment(int id, Comment comment) {
		commentRepository.deleteById(id);
		comment.setId(id);
		commentRepository.save(comment);
	}
}
