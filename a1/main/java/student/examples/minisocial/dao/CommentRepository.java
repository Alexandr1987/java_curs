package student.examples.minisocial.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import student.examples.minisocial.domain.entities.Comment;

public interface CommentRepository extends JpaRepository<Comment,Integer>{

}
