package student.examples.minisocial.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import student.examples.minisocial.domain.entities.Post;
@Repository
public interface PostRepository extends JpaRepository<Post,Integer> {
	
}
