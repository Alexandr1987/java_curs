package student.examples.minisocial.domain.entities;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import student.examples.minisocial.domain.entities.interfaces.HasAuthor;
import student.examples.minisocial.domain.entities.interfaces.HasSentiment;
import student.examples.minisocial.domain.entities.interfaces.IsChronological;
import student.examples.minisocial.domain.entities.interfaces.IsCommentable;

@Entity(name="post")
public class Post implements IsCommentable, IsChronological, HasSentiment, HasAuthor{
	@Id
	@GeneratedValue(strategy =  GenerationType.AUTO)
	private Integer id;
	@ManyToOne
	@JoinColumn(name = "author")
	private User author;
	@OneToMany(mappedBy="post")
	private List<Comment> comment = new ArrayList<>();
	@OneToMany(mappedBy="post")
	private List<Sentiment> sentiments = new ArrayList<>();
	
	@Column(unique = true, length = 50)
	private String title;
	@Column
	private String body;
	@CreationTimestamp
	private LocalDateTime created;
	@UpdateTimestamp
	private LocalDateTime updated;
	private LocalDateTime deleted;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public User getAuthor() {
		return author;
	}
	public void setAuthor(User author) {
		this.author = author;
	}
	public List<Comment> getComment() {
		return comment;
	}
	public void setComment(List<Comment> comment) {
		this.comment = comment;
	}
	public List<Sentiment> getSentiments() {
		return sentiments;
	}
	public void setSentiments(List<Sentiment> sentiments) {
		this.sentiments = sentiments;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public LocalDateTime getCreated() {
		return created;
	}
	public void setCreated(LocalDateTime created) {
		this.created = created;
	}
	public LocalDateTime getUpdated() {
		return updated;
	}
	public void setUpdated(LocalDateTime updated) {
		this.updated = updated;
	}
	public LocalDateTime getDeleted() {
		return deleted;
	}
	public void setDeleted(LocalDateTime deleted) {
		this.deleted = deleted;
	}
	public Post(Integer id, User author, List<Comment> comment, List<Sentiment> sentiments, String title, String body,
			LocalDateTime created, LocalDateTime updated, LocalDateTime deleted) {
		super();
		this.id = id;
		this.author = author;
		this.comment = comment;
		this.sentiments = sentiments;
		this.title = title;
		this.body = body;
		this.created = created;
		this.updated = updated;
		this.deleted = deleted;
	}
	public Post() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "Post [id=" + id + ", author=" + author + ", comment=" + comment + ", sentiments=" + sentiments
				+ ", title=" + title + ", body=" + body + ", created=" + created + ", updated=" + updated + ", deleted="
				+ deleted + "]";
	}
	
	





}
