package student.examples.minisocial.domain.entities;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import student.examples.minisocial.domain.entities.interfaces.HasAuthor;
import student.examples.minisocial.domain.entities.interfaces.HasSentiment;
import student.examples.minisocial.domain.entities.interfaces.IsChronological;
import student.examples.minisocial.domain.entities.interfaces.IsCommentable;

@Entity(name="comment")
public class Comment implements IsCommentable, IsChronological, HasSentiment, HasAuthor{
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
	@Column(length = 150)
    private String body;
    @ManyToOne
    private Post post;
	@CreationTimestamp
	private LocalDateTime created;
	@UpdateTimestamp
	private LocalDateTime updated;
	private LocalDateTime deleted;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public Post getPost() {
		return post;
	}
	public void setPost(Post post) {
		this.post = post;
	}
	public LocalDateTime getCreated() {
		return created;
	}
	public void setCreated(LocalDateTime created) {
		this.created = created;
	}
	public LocalDateTime getUpdated() {
		return updated;
	}
	public void setUpdated(LocalDateTime updated) {
		this.updated = updated;
	}
	public LocalDateTime getDeleted() {
		return deleted;
	}
	public void setDeleted(LocalDateTime deleted) {
		this.deleted = deleted;
	}
	public Comment(Integer id, String body, Post post, LocalDateTime created, LocalDateTime updated,
			LocalDateTime deleted) {
		super();
		this.id = id;
		this.body = body;
		this.post = post;
		this.created = created;
		this.updated = updated;
		this.deleted = deleted;
	}
	public Comment() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "Comment [id=" + id + ", body=" + body + ", post=" + post + ", created=" + created + ", updated="
				+ updated + ", deleted=" + deleted + "]";
	}
	
	





}
