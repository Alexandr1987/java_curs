package student.examples.minisocial.domain.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import student.examples.minisocial.domain.entities.interfaces.HasSentiment;
@Entity
public class Sentiment implements HasSentiment{
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
	@ManyToOne
	private Post post;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Post getPost() {
		return post;
	}
	public void setPost(Post post) {
		this.post = post;
	}
	public Sentiment(Integer id, Post post) {
		super();
		this.id = id;
		this.post = post;
	}
	public Sentiment() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "Sentiment [id=" + id + ", post=" + post + "]";
	}
	
	
}
