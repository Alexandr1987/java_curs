package student.examples.minisocial.domain.entities;

public enum RelationState {
NEW,IGNORED,ACCEPTED
}
