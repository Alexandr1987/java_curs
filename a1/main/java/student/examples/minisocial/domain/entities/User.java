package student.examples.minisocial.domain.entities;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import student.examples.minisocial.domain.entities.interfaces.IsCommentable;
@Entity(name="Profile")
public class User implements IsCommentable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(unique = true, length = 25)
    private String nickName;
    private Date createdDate;
    private Date modifiedDate;
    @Column(length = 25)
    private String password;
    @Column(unique = true, length = 50)
    private String email;
    private String avatar;
    
    @OneToMany(mappedBy = "author", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Post> posts = new ArrayList<>();
    
    @OneToMany(mappedBy = "author", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Message> sentMessages = new ArrayList<>();
    
    @OneToMany(mappedBy = "reciver", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Message> receivedMessages = new ArrayList<>();
    
	@CreationTimestamp
	private LocalDateTime created;
	@UpdateTimestamp
	private LocalDateTime updated;
	private LocalDateTime deleted;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNickName() {
		return nickName;
	}
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAvatar() {
		return avatar;
	}
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	public List<Post> getPosts() {
		return posts;
	}
	public void setPosts(List<Post> posts) {
		this.posts = posts;
	}
	public List<Message> getSentMessages() {
		return sentMessages;
	}
	public void setSentMessages(List<Message> sentMessages) {
		this.sentMessages = sentMessages;
	}
	public List<Message> getReceivedMessages() {
		return receivedMessages;
	}
	public void setReceivedMessages(List<Message> receivedMessages) {
		this.receivedMessages = receivedMessages;
	}
	public LocalDateTime getCreated() {
		return created;
	}
	public void setCreated(LocalDateTime created) {
		this.created = created;
	}
	public LocalDateTime getUpdated() {
		return updated;
	}
	public void setUpdated(LocalDateTime updated) {
		this.updated = updated;
	}
	public LocalDateTime getDeleted() {
		return deleted;
	}
	public void setDeleted(LocalDateTime deleted) {
		this.deleted = deleted;
	}
	public User(Integer id, String nickName, Date createdDate, Date modifiedDate, String password, String email,
			String avatar, List<Post> posts, List<Message> sentMessages, List<Message> receivedMessages,
			LocalDateTime created, LocalDateTime updated, LocalDateTime deleted) {
		super();
		this.id = id;
		this.nickName = nickName;
		this.createdDate = createdDate;
		this.modifiedDate = modifiedDate;
		this.password = password;
		this.email = email;
		this.avatar = avatar;
		this.posts = posts;
		this.sentMessages = sentMessages;
		this.receivedMessages = receivedMessages;
		this.created = created;
		this.updated = updated;
		this.deleted = deleted;
	}
	public User() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "User [id=" + id + ", nickName=" + nickName + ", createdDate=" + createdDate + ", modifiedDate="
				+ modifiedDate + ", password=" + password + ", email=" + email + ", avatar=" + avatar + ", posts="
				+ posts + ", sentMessages=" + sentMessages + ", receivedMessages=" + receivedMessages + ", created="
				+ created + ", updated=" + updated + ", deleted=" + deleted + "]";
	}

	

	


}




/*
profile
   ( id .... nick_name .... ) 
     1 ....  User1
     2 ....  User2
     3 ....  User3
     4 ....  User4
     5 ....  User5
 
 
profile_friend_to
   ( profile_of_id  profile_to_id )
  	  1      ->      2
  	  1      ->      3
  	  
  	  
User2  ->  reverse  ->  User1 -> direct -> User2 .... 	  
  	  
  	  
  	  
  */
