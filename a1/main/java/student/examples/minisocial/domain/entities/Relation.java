package student.examples.minisocial.domain.entities;

import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
@Entity
public class Relation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Enumerated()
    private RelationType type;
    @Enumerated()
    private RelationState state;
    @ManyToOne
    private User fromProfile;
    @ManyToOne
    private User toProfile;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public RelationType getType() {
		return type;
	}
	public void setType(RelationType type) {
		this.type = type;
	}
	public RelationState getState() {
		return state;
	}
	public void setState(RelationState state) {
		this.state = state;
	}
	public User getFromProfile() {
		return fromProfile;
	}
	public void setFromProfile(User fromProfile) {
		this.fromProfile = fromProfile;
	}
	public User getToProfile() {
		return toProfile;
	}
	public void setToProfile(User toProfile) {
		this.toProfile = toProfile;
	}
	public Relation(Integer id, RelationType type, RelationState state, User fromProfile, User toProfile) {
		super();
		this.id = id;
		this.type = type;
		this.state = state;
		this.fromProfile = fromProfile;
		this.toProfile = toProfile;
	}
	public Relation() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "Relation [id=" + id + ", type=" + type + ", state=" + state + ", fromProfile=" + fromProfile
				+ ", toProfile=" + toProfile + "]";
	}

    
    
    
    
    
}
