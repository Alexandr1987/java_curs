package stado;

public class Students {

	String fullname;
    String specialty;
    Float  grade;
    
    public Students(String fullname, String specialty, Float grade) {
		super();
		this.fullname = fullname;
		this.specialty = specialty;
		this.grade = grade;
	}
    
    public Students() {}

	@Override
	public String toString() {
		return "Students [fullname=" + fullname + ", specialty=" + specialty + ", grade=" + grade + "]";
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getSpecialty() {
		return specialty;
	}

	public void setSpecialty(String specialty) {
		this.specialty = specialty;
	}

	public Float getGrade() {
		return grade;
	}

	public void setGrade(Float grade) {
		this.grade = grade;
	}
    
    
}
