package main;
import stado.Students;
import java.util.ArrayList;
public class Main {
	public static void main(String[] args) {
		ArrayList<Students> students = new ArrayList<Students>();  
			students.add(new Students("Ionel","Cadastru",8f));
			students.add(new Students("Vasilica","Programare",7f));
			students.add(new Students("Grigoras","Fizica",9f));
			students.add(new Students("Vasilisa","Psihologie",6f));
			students.add(new Students("Kassandra","Fotogrammetrie",9.9f));			
			int indexOfBest = 0;
			for(int i=0;i<students.size();i++) {
			if(students.get(indexOfBest).getGrade()<students.get(i).getGrade()){indexOfBest=i;}			
			}
			students.get(indexOfBest).setGrade(students.get(indexOfBest).getGrade()+0.1f);			
		ArrayList <Students> bestStudents = new ArrayList<Students>();			
			for(Students student: students) {
				if(student.getGrade()>=8f) {
					bestStudents.add(student);
				}				
			}		
			System.out.println("list of all students: "+students);
			System.out.println("-------------------------------------");
			System.out.println("index of the best student: "+indexOfBest);
			System.out.println("-------------------------------------");
			System.out.println("full record of the best Student: "+students.get(indexOfBest));
			System.out.println("-------------------------------------");
			System.out.println("students with mark over 8.0:"+bestStudents);
	}
}
