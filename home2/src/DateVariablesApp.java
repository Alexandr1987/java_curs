
public class DateVariablesApp {

	public static void main(String[] args) {
		int actualYyear  = 2019;
        int actualMonth = 11;
        int actualDay   = 11;

 
        System.out.println("Today is: ");
        System.out.println( actualYyear );
        System.out.println("-");
        System.out.println( actualMonth );
        System.out.println("-");
        System.out.print( actualDay );
	
//     One error with print statement rewritten to "println"
//  P1 Print "ln" line is used to go to next line after executing statement; 
//        in case above we have many lines.
//	   Third line on SOUT added separator ";"
//  P2 Types are double float String char boolean ...   
        
	
        System.out.print("Today is: " + actualDay + "-" + actualMonth + "-" + actualYyear);
	}

}


