package alex.example;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SubscriberController {
	
@Autowired
SubscriberRepo subsRepo;
	
@GetMapping("/subscribers")
public List<Subscriber> allSubscribers (){
	List<Subscriber> subs = subsRepo.findAll();
	return subs;
	
}
}
