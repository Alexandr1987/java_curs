package alex.example;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
@Repository
public class SubscriberRepo {
	@Autowired
	 JdbcTemplate jdbcTemplate;
	    public List<Subscriber> findAll() {

	        String sql = "SELECT id, name, email FROM public.subscribers";

	        return jdbcTemplate.query(
	                sql,
	                (rs, rowNum) ->
	                        new Subscriber(
	                                rs.getInt("id"),
	                                rs.getString("name"),
	                                rs.getString("email")
	                        )
	        );
	    }
}
