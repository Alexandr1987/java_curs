package marine;

public interface FishInterface {
    public void setType(String type);
    public String getType();
    public void setWeight(Integer weight);
    public Integer getWeight();
    public void setEnvironment(String environment);
    public String getEnvironment();
}
