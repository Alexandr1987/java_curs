package main;
import geometry.Line;
public class MainApplication {

	public static void main(String[] args) {
		Line l1 = new Line(0,0,5,5);		
		l1.printCoords();
		l1.length();
		Line l2 = new Line(10,20,30,40);
		l2.printCoords();
		l2.length();
		
		Line short_horizontal_line = new Line( 100,0, 200,0 );
	    short_horizontal_line.printCoords();
	    System.out.println("hor line: "+ short_horizontal_line.length() );
	      
	    Line long_diagonal_line = new Line( 100,100, 500,500 );
	    long_diagonal_line.printCoords();
	    System.out.println( "diag line: "+ long_diagonal_line.length() );
		
	}	
}
