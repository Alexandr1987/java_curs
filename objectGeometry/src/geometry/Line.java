package geometry;
import java.lang.Math;
public class Line {
	  // properties
	  public static int startX;
	  public static int endX;
	  public static int startY;
	  public static int endY;


	  // constructor
	  public Line(int startX,int startY,int endX,int endY) {
		  if (endX - startX >= 0 && endY - startY >=0) {
			  	this.startY = startY;
			    this.startX = startX;
			    this.endX   = endX;
			    this.endY   = endY; 
		  }else {
			  System.err.println("Your coords are in one point!");
		  }		
      }
	 public static void printCoords() {
			 System.out.printf("[%d:%d]----->[%d:%d]\n",startX,startY,endX,endY);
		}  
	 
	 public static int length() {
		 int res = (short)Math.round(Math.sqrt(Math.pow((endX-startX), 2)+Math.pow((endY-startY), 2)));
		 System.out.println(res);
		 return res;
	 }
}