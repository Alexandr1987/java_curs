package provider;
import nasaStorage.NasaStorage;
import java.net.*;

import org.json.JSONObject;

import java.io.*;
@SuppressWarnings("all")
public class NasaDataProvider {
 private final static String ACCESS_KEY= "XXX";
 private final static String NEO_ENDPOINT = "https://api.nasa.gov/neo/rest/v1/feed";

 
 public void getNeoAsteroids(String start, String end ) throws Exception{
	 //1 connect to NASA API 
	 URL nasa = new URL(NEO_ENDPOINT+"?start_date="+start+"&end_date="+end+"&api_key="+ACCESS_KEY);
	 BufferedReader in = new BufferedReader(
	 new InputStreamReader(nasa.openStream()));
	 //2 read data
	 String stringData ="";
	 String inputLine;
	 while ((inputLine = in.readLine()) != null)
//	     System.out.println(inputLine.replaceAll("}", "}\n"));
		 stringData +=inputLine;
	 in.close();
	 
	 

	 //3 Parse JSON
	 JSONObject data = new JSONObject(stringData);
	 int count = data.getInt("element_count");
	 
	 //4 Test some data
	 System.out.println("Found "+count+" results");


	 String[] datesStart =start.split("-");
//	System.out.println(datesStart[2]);
	String[] datesEnd =end.split("-");
//	System.out.println(datesEnd[2]);
	String dateNow;
	String dataFin;
	 for (int i = Integer.parseInt(datesStart[2]);i<=Integer.parseInt(datesEnd[2]);i++) {
	    dateNow = datesStart[0]+"-"+datesStart[1]+"-"+String.format("%02d", i);

		
		int arr = data.getJSONObject("near_earth_objects").getJSONArray(dateNow).length();
//		System.out.println( "number of Asteroids in array for date: "+ dateNow+ "  elements: "+arr);
		
	
	for(int counter=0;counter<arr;counter++) {			
			Float d_min = data.getJSONObject("near_earth_objects")
	 				.getJSONArray(dateNow)
	 				.getJSONObject(counter)
	 				.getJSONObject("estimated_diameter")
	 				.getJSONObject("kilometers")
	 				.getFloat("estimated_diameter_min");
			
			Float d_max = data.getJSONObject("near_earth_objects")
					.getJSONArray(dateNow)
					.getJSONObject(counter)
					.getJSONObject("estimated_diameter")
					.getJSONObject("kilometers")
					.getFloat("estimated_diameter_max");
			
			Long distance_km = data.getJSONObject("near_earth_objects")
					.getJSONArray(dateNow)
					.getJSONObject(counter)
					.getJSONArray("close_approach_data")
					.getJSONObject(0)
					.getJSONObject("miss_distance")
					.getLong("kilometers");
			
			boolean isHazardous = data.getJSONObject("near_earth_objects")
					  .getJSONArray(dateNow)
					  .getJSONObject(counter)
					  .getBoolean("is_potentially_hazardous_asteroid");
			
			NasaStorage NS = new NasaStorage(dateNow,d_max, d_max, distance_km, isHazardous);
			

			System.out.println(NS);
//			System.out.println(dateNow+"  "+"min Diameter in Km : "+d_min+"   max Diameter in Km : "+d_max+" miss_distance:   "+distance_km+"  Dangerous?: "+isHazardous);	
		}
//	System.out.println("########################################################################");
	 }





 }
}
