package main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map.Entry;

public class ListOfPurchase {

	public static void main(String[] args) {
        ArrayList<String> listOfWishes = new ArrayList<>( Arrays.asList("Spagetti,2", "Pepper,5","Sugar,1"));
        ArrayList<String> listOfOffers = new ArrayList<>( Arrays.asList("Spagetti,10.00", "Sugar,5.00","Water,2.50"));
        ArrayList<String> listOfPurchases = new ArrayList<>();
        
        HashMap<String,Integer> clientsList = new HashMap<>();
        HashMap<String,Double> sellerList = new HashMap<>();
        
        String [] wishList;
        int i = 0;
        for(String wishlist: listOfWishes) {
        	wishList=wishlist.split(",");
        	clientsList.put(wishList[i], Integer.valueOf(wishList[i+1]));
        }        
        for(String wishlist: listOfOffers) {
        	wishList=wishlist.split(",");
        	sellerList.put(wishList[i], Double.valueOf(wishList[i+1]));
        }
        
        for(Entry<String, Integer> intro : clientsList.entrySet()) {
        	for(Entry<String, Double> extro : sellerList.entrySet()) {
        		if(intro.getKey().equals(extro.getKey())) {
        			listOfPurchases.add(intro.getKey() + 
        					"- price : "+extro.getValue()+
        					"; quantity :"+intro.getValue()+
        					"; total price :"+(intro.getValue()*extro.getValue())+"\n");}
        	}
        }
        System.out.println(listOfPurchases);
	}

}
