import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class LogInPage implements ActionListener{
	JFrame frame= new JFrame();
	JButton loginButton = new JButton("Log In");
	JButton resetButton = new JButton("Reset");
	JTextField usrIDField = new JTextField();
	JLabel usrIDLabel = new JLabel("User ID");
	JPasswordField usrPasswordField  = new JPasswordField();
	JLabel usrPassLabel = new JLabel("User Password");
	JLabel message = new JLabel("");
	HashMap<String, String> loginInfo = new HashMap<String, String>();
	
	LogInPage(HashMap<String, String> loginInfoOriginal){
		loginInfo = loginInfoOriginal;
		
		usrIDLabel.setBounds(50, 100, 75, 25);
		usrPassLabel.setBounds(50, 150, 75, 25);
		
		message.setBounds(125,250,250,35);
		message.setFont(new Font(null,Font.ITALIC,25));
		
		usrIDField.setBounds(150,100,200,25);
		usrPasswordField.setBounds(150,150,200,25);
		
		loginButton.setBounds(125,200,100,25);
		loginButton.setFocusable(false);
		loginButton.addActionListener(this);
		
		resetButton.setBounds(225,200,100,25);
		resetButton.setFocusable(false);
		resetButton.addActionListener(this);
		
		frame.add(usrIDLabel);
		frame.add(usrPassLabel);
		frame.add(message);
		frame.add(usrIDField);
		frame.add(usrPasswordField);
		frame.add(loginButton);
		frame.add(resetButton);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(420,420);
		frame.setLayout(null);
		frame.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==resetButton) {
			usrIDField.setText("");
			usrPasswordField.setText("");
		}
		
		
		if(e.getSource()==loginButton) {
			String usrId = usrIDField.getText();
			String password = String.valueOf(usrPasswordField.getPassword());
			
			if(loginInfo.containsKey(usrId)) {
				if(loginInfo.get(usrId).equals(password)) {
					message.setForeground(Color.GREEN);
					message.setText("Login successful");
					frame.dispose();
					WelcomePage welcomePage = new WelcomePage(usrId);
				}else {
					message.setForeground(Color.RED);
					message.setText("Try Again");
				}
			}else {
				message.setForeground(Color.RED);
				message.setText("username not found");
			}
			
		}
	}
}
