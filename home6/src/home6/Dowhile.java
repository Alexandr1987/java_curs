package home6;
import java.util.Scanner;


public class Dowhile {

	public static void main(String[] args) {
		final short CORRECT_PIN = 1234;
        short pin=0000;
       byte tries = 0;
		do {
			Scanner enterPin =  new Scanner(System.in);
            System.out.print("Enter pin: ");
            pin = enterPin.nextShort();
    		if (tries <=3 && pin != CORRECT_PIN) {
    			tries++;
    			System.out.println("Try again!");
    			
    			if (tries == 3) {
    				
    				System.out.println("TOO MANY TRIES!");
    				break;
    				
    			}
    		}else {
    			System.out.println("Access Granted!");
    		} 
		}while(pin != CORRECT_PIN);

	}

}
