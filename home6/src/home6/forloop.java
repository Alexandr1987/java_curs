package home6;

import java.util.Scanner;

public class forloop {

	public static void main(String[] args) {
		 char direction;
	        byte length;
	        System.out.println("Enter direction (char V or H)! ");
        	Scanner direction1 =  new Scanner(System.in);
        	direction = direction1.next().charAt(0);
        	System.out.println("Enter length: ");
        	Scanner length1 =  new Scanner(System.in);
        	length = length1.nextByte();
        	
	        for(int i=1;i <= length;i++) {
	           if (direction == 'V') {
	        	   System.out.print("-");
	           } else if(direction == 'H')
	        	   System.out.println("|");
	        	
	        }

	}

}
