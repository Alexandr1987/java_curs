package listiter;

import java.util.LinkedList;
import java.util.ListIterator;

public class Listiter {

	public static void main(String[] args) {
		LinkedList<String> list = new LinkedList<>();
		list.add("max");
		list.add("jack");
		list.add("soul");
		list.add("july");
		System.out.println(list);
		final ListIterator<String> iterator = list.listIterator();
		while (iterator.hasNext()) {
		final String next = iterator.next();
		if (next.equals("jack")) {
		iterator.remove();
		} else if (next.equals("july")) {
		iterator.add("mary");
		} else if (next.equals("soul")) {
		iterator.set("soul2");
		}
		}
		System.out.println(list);

	}

}
