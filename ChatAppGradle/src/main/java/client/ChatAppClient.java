package client;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

import lib.Message;

public class ChatAppClient {

	public static void main(String[] args) throws UnknownHostException, IOException, ClassNotFoundException {
		// client ----> message ----> server
		// client <---- message <---- server
		
		
		Message mes = new Message("Hello Server");
		Socket clientSocket = new Socket("localhost",7777);
		
		try {
			ObjectOutputStream oos = new ObjectOutputStream(clientSocket.getOutputStream());
			oos.writeObject(mes);
			oos.flush();
			
			ObjectInputStream ois = new ObjectInputStream(clientSocket.getInputStream());
			Object o = ois.readObject();
			System.out.println("Client sent: "+ o);
			clientSocket.close();

		}catch (Exception e) {
			System.out.println(e.toString());
		}

		System.out.println("Client: Ending!");
	}

}
