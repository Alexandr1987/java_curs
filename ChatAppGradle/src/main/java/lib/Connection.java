package lib;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

public class Connection {
	private Socket socket;

	public Socket getSocket() {
		return socket;
	}

	public void setSocket(Socket socket) {
		this.socket = socket;
	}
	
	
	public Connection(Socket socket) {
		super();
		this.socket = socket;
	}
	
	public Connection(String host,Integer port) throws UnknownHostException, IOException {
			this (new Socket(host,port));
	}

	public void send(Object object)throws IOException {
		try {
			ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream()); 
			oos.writeObject(object);
			oos.flush();
		} catch (Exception e) {
			System.out.println("OoStream"+e.toString());
		}
	}
	public Object fetch()throws IOException, ClassNotFoundException {
		ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
		Object o = ois.readObject();
		return o;
	}
	
	
}
