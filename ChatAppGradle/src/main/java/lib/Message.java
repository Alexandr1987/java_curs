package lib;

import java.io.Serializable;

public class Message implements Serializable{
private String body;

public String getBody() {
	return body;
}

public void setBody(String body) {
	this.body = body;
}

public Message(String body) {
	super();
	this.body = body;
}

@Override
public String toString() {
	return "Message [body=" + body + "]";
	}	

}
