package lib;

import java.io.Serializable;

public class Action implements Serializable{
	private ActionEnum name;

	public ActionEnum getName() {
		return name;
	}

	public void setName(ActionEnum name) {
		this.name = name;
	}

	public Action(ActionEnum name) {
		super();
		this.name = name;
	}

	@Override
	public String toString() {
		return "Action [name=" + name + "]";
	}
	
	
	
}
