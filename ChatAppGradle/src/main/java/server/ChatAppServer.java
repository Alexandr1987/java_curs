package server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import lib.Message;
public class ChatAppServer {
	static Socket socket;
	public static void main(String[] args) throws IOException, ClassNotFoundException {
		Message mmm = new Message("Hello client");
		System.out.println("Server: waiting for client:");
				
		
		

				
		try {
			ServerSocket serverSocket = new ServerSocket(7777);
			System.out.println("Server: client connected!!!");
			socket = serverSocket.accept();
			
			ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
			Object o = ois.readObject();
			System.out.println("Client sent: "+ o);
			
			ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
			oos.writeObject(mmm);
			oos.flush();
			
			
		}catch (Exception e) {
			System.out.println(e.toString());
		}
		
		
		System.out.println("Server: closing");
		socket.close();
	}

}
