package tool;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Before;

public aspect LoggingAspect {
	@Before("execution(public static void server.ChatAppServer.main(String[]))")
	public void logBeforeServer(JoinPoint joinPoint) {
		System.out.println("Server: started!");
		System.out.println("Server: waiting for ops");
	}
//	@After("execution(public static void server.ChatAppServer.main(String[]))")
//	public void logAfterServer(JoinPoint joinPoint) {
//		System.out.println("Server: closing!");
//	}
	@Before("execution(public static void client.ChatAppClient.main(String[]))")
	public void logBeforeClient(JoinPoint joinPoint) {
		System.out.println("Client: started!");
		System.out.println("Client: waiting for ops");
	}
//	@After("execution(public static void client.ChatAppClient.main(String[]))")
//	public void logAfterClient(JoinPoint joinPoint) {
//		System.out.println("Client: closing!");
//	}
	
	
	
	
}
