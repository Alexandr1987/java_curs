package threadsP3;

public class BreadConsumer extends Thread{
	private String name;
	private Integer total_breads;
	private Bread bread_reserve;
	private BreadProvider provider;
	public BreadConsumer(String name, BreadProvider provider) {
		super();
		this.name = name;
		this.total_breads = 0;
		this.provider = provider;
	}
	
	public void consumeOneBread() {
		if(bread_reserve!=null) {
			total_breads++;
			System.out.println(this+"CONSUMING <"+bread_reserve);
			bread_reserve=null;
		}
	}
	
	public void buyOneBread() {
		Bread fresh_bread = provider.sellOneBread();
		if(fresh_bread !=null) {
			System.out.println(this + "BUING <" +fresh_bread);
			bread_reserve=fresh_bread;
		}
	}
	
	@Override
	public void run() {
		while(true) {
			if(bread_reserve==null) {
				buyOneBread();
			}
			consumeOneBread();
			try {
				sleep(500);
			}catch(InterruptedException ie) {
				ie.printStackTrace();
			}
		}
	}
	@Override
	public String toString() {
		return"			BreadConsumer(total:"+total_breads+") [name="+name+"]";
	}
}
