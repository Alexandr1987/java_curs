package threadsP3;

import java.util.Random;

public class Bread {
	private String name;
	private Integer weight;
	private Integer id;
	public Bread(String name, Integer id) {
		super();
		this.name = name;
		this.weight = new Random().nextInt(50)+400;
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	@Override
	public String toString() {
		return "Bread ("+id+")[name=" + name + ", weight=" + weight + "g]";
	}

	
	
}
