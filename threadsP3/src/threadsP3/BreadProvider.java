package threadsP3;

import java.util.ArrayList;

public class BreadProvider extends Thread {
	private ArrayList<Bread> breads;
	private Integer breads_sold;
	private Integer bread_id;
	public BreadProvider() {
		super();
		breads = new ArrayList<>();
		this.breads_sold = 0;
		this.bread_id = 0;
	}
	
	@Override
	public void run() {
		while(true) {
			if(breads.size()<10) {
				produceOneBread();
			}
			try {
				sleep(100);
				}
			catch(InterruptedException ie) {
				ie.printStackTrace();
			}
		}
	}
	private void produceOneBread() {
		Bread fresh_bread = new Bread("White",++bread_id);
		System.out.println(this + "PRODUCING >"+fresh_bread);
		breads.add(fresh_bread);
	}
	
	public Bread sellOneBread() {
		if(breads.size()>0) {
			breads_sold++;
			return breads.remove(0);
		}else {
			return null;
		}
	}
	@Override
	public String toString() {
		return "BreadProvider (sold:"+breads_sold+"left: "+breads.size()+")";
	}
}
