package meinClass;

import annProvider.PrivateFields;

@PrivateFields
public class Car implements CarInterface {
	
    	private String model;
     	private int year;
	
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public Car() {
		
	}
	@Override
	public String toString() {
		return "Car [model=" + model + ", year=" + year + "]";
	}

	
	
	
	
}
