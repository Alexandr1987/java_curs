package meinClass;

public interface CarInterface {
	public String getModel();
	public void setModel(String model);
	public int getYear();
	public void setYear(int year);
}
