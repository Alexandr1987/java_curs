package tools;

import java.io.IOException;

import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtConstructor;
import javassist.CtNewConstructor;
import javassist.NotFoundException;

public class tools {
	public static Class<?> transform() throws NotFoundException, CannotCompileException, IOException {
	//  get the class manipulation tool
		ClassPool pool = ClassPool.getDefault();
		pool.appendClassPath("D:\\mavenplay\\java_ann_refl_assist_ex_consumer\\bin");
	// .appendClassPath - in case of errors
	// load desired class
		CtClass cc = pool.get("meinClass.Car");
		CtConstructor ccon = cc.getDeclaredConstructor(null);
	// remove it
		cc.removeConstructor(ccon);
	// create a new default constructor
		CtConstructor cstructor =  CtNewConstructor.make("public"+" "+"Car"+"(String model, int year)"+"{"+"this.model=model;this.year=year;"+"}",cc);
	// add constructor to class
		cc.addConstructor(cstructor);
	// add the modified class
		return cc.toClass();
	}
}
