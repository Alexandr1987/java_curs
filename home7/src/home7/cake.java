package home7;
import java.util.Scanner;
public class cake {
	public static void main(String[] args) {		
		final int SMOKE  = 1; 
	       final int FIRE   = 2; 
	       final int CANDLE = 3; 
	       final int CREAM  = 4; 
	       final int BASE   = 5; 
	       Scanner cakeWidth = new Scanner(System.in);
	       System.out.print("Cake width pls: ");
	       int width = cakeWidth.nextInt(); 
	       
	       for(int level=1; level<=5 ;level++) {
	           switch(level) {
	               case SMOKE:
	            	   for(int lines=1; lines<=1; lines++) {
	                        for(int elements=1; elements<=width; elements++) {
	                            System.out.print(" ` ");
	                        }
	                        System.out.println();
	                    }
	            	   break;
	               case FIRE: 
		            	   for(int lines=1; lines<=1; lines++) {
		                        for(int elements=1; elements<=width; elements++) {
		                            System.out.print(" ^ ");
		                        }
		                        System.out.println();
		                    }
		            	   break;
	               case CANDLE: 
		            	   for(int lines=1; lines<=1; lines++) {
		                        for(int elements=1; elements<=width; elements++) {
		                            System.out.print(" | ");
		                        }
		                        System.out.println();
		                    }
		            	   break;
	               case CREAM:
		            	   for(int lines=1; lines<=1; lines++) {
		                        for(int elements=1; elements<=width*3; elements++) {
		                            System.out.print("~");
		                        }
		                        System.out.println();
		                    }
		            	   break;
	               case BASE:
	                    for(int lines=1; lines<=3; lines++) {
	                        for(int elements=1; elements<=width*3; elements++) {
	                            System.out.print("#");
	                        }
	                        System.out.println();
	                    }
	                    break;
	           }
	       }
		
	}

}
