package home7;

public class Explicit {

	public static void main(String[] args) {
			int a=1,b=1,c=1;
//			boolean x;
		
//			b = a++; // b remains 1 
//			System.out.println(b);
//			x = c<++b; // b will update to 2 therefore is true
//			System.out.println(x);
//			a=c+=2; //a will be 3
//			System.out.println(a);
			for( b=a++; c<++b; a=c+=2 ) {   // why ? if i=0; i<10; i++
			System.out.printf("%d %d %d\n",--a,--b, c);
		}

	}

}
// a=1 b=1 c=1 --->  b = 2 ;c < b; a=(c=1+2) --> a=3 b=1 c=1  

// 1. Instantiation int -> type and variables a,b,c = 1
// 2. Entering for loop b= a+1; compare-ing (c=1) c <(true) ++b (b=2 update then write); assigning to a = 3 from a = c=1+2;
// printing values 2 0 1