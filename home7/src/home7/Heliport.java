package home7;

public class Heliport {

	public static void main(String[] args) {
		final int SCALE  = 10; 

        int hX = 5;
        int hY = 4;
        int d = 0;
 	    int e = 0;
  for(int y=1; y<=SCALE; y++) {
   System.out.printf("%2d. ",y);
   for(int x=1; x<=SCALE; x++) {
       if( x == 1 || y == 1 || x == SCALE || y == SCALE ) {
           System.out.print("# ");
       } else if ( x == hX && y == hY ) {
           System.out.print("H ");  
       }else if (d == hX-1 || e == hY-1) {
    	   for (int i = 0; i<=d;i++) {
    		   for (int j = 0; j<=e;j++) {
    		   System.out.print("D ");}
    	   }
//    	   System.out.print("D ");
       }else {
    	   System.out.print("  ");
       }
   }
   System.out.println();
  }

	}

}
