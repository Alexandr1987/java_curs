package restaurant;
import world.Person;
public class Table {
	  // properties
	  int number;
	 private Person seatA;
	 private Person seatB;
	
		// set get 
	 
	  	public int getNumber() {
		    return number;
		}

		public void setNumber(int number) {
			if(number==1 ||number==2||number==3) {
				this.number = number;
			}else {
				System.err.println("no seats here!");
			}
		}
		public Person getSeat(char seat) {
			if (seat == 'A') {
				return seatA;
			}else if (seat == 'B') {
				return seatB;
			}else {
				System.err.println("No such type");
				return null;
			}
			
		}
		public void setSeat(Person person, char seat ) {
			if (seat == 'A') {
				this.seatA=person;
			}else if (seat == 'B') {
				this.seatB=person;
			}else {
				System.err.println("err");
			}
		}


	//	 constructor
	  public Table(int number) {
	    this.number = number;
	    
	  }

	  
	 boolean isFree(){
		 return this.seatA == null && this.seatB==null;
	 }
	 public void  printInfo() {
		 System.out.print("A)");
		 if (seatA == null) {
			 System.out.println("Free seat");
		 }else seatA.printInfo(); 
		 System.out.print("B)");
		 if (seatB == null) {
			 System.out.println("Free seat");
		 }else seatB.printInfo(); 
		}
	  
}
