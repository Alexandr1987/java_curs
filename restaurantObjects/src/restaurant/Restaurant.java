package restaurant;

import world.Person;

public class Restaurant {
	  // properties
	  private String name;
	 private Table tableNearWindow;
	 private Table tableNearDoors;
	 private Table tableNearBathroom;
	 private Person waiter;
	    
	  
	 
	    public Person getWaiter() {
	        return waiter;
	    }

	    public void setWaiter(Person waiter) {
	        this.waiter = waiter;
	    }
		public String getName() {
		return name;
	}
		public void setName(String name) {
		this.name = name;
	}
		public Table getTable( int tables) {
			if ( tables == 1) {
				return tableNearWindow;
			}else if (tables == 2) {
				return tableNearDoors;
			}else if (tables == 3) {
				return tableNearBathroom;
			}else {
				System.err.println("No such type");
				return null;
			}
			
		}
		public void setTable(Table table, int tables ) {
			if ( tables == 1) {
				this.tableNearWindow=table;
			}else if ( tables == 2) {
				this.tableNearDoors=table;
			}else if ( tables == 3) {
				this.tableNearBathroom=table;
			}else {
				System.err.println("err");
			}
		}

	  // constructor
	  public Restaurant(String name) {
	    this.name = name;
	  } 
	    public boolean isFree() {
	        if (tableNearWindow == null && tableNearDoors == null && tableNearBathroom == null) {
	            return true;
	        } else {
	            return false;
	        }
	    }
	  
	    public void printInfo() {
	        System.out.println("RESTAURANT: Good Morning Sunshine!");
	        waiter.printInfo();
	        System.out.println("##################################");
	        System.out.println("----------------------------------");
	        System.out.println("Table 1 :");
	        tableNearWindow.getSeat('A').printInfo();
	        tableNearWindow.getSeat('B').printInfo();
	        System.out.println("Table 2 :");
	        tableNearDoors.getSeat('A').printInfo();
	        tableNearDoors.getSeat('B').printInfo();
	    }
}
