package main;
import world.Person;
import restaurant.Restaurant;
import restaurant.Table;
public class ApplicationRestaurant {

	public static void main(String[] args) {
		Person p1 = new Person("John Doe",'m',31);		
		p1.setJobTitle("Designer");
		Person p2 = new Person("Jane Doe",'f',30);
		p2.setJobTitle("Medic");
		p2.printInfo();
		Person p3 = new Person("Uncle Sam",'f',30);
		p3.setJobTitle("Soldier");
		p3.printInfo();
		Person p4 = new Person("Life",'f',30);
		p4.setJobTitle("Teacher");
		p4.printInfo();
		
		
        Person waiter = new Person("Gregory", 'm', 36);
        waiter.setJobTitle("Waiter");
		
		
		
        Table table = new Table(1);
        table.setSeat(p1, 'A');
        table.setSeat(p2, 'B');
        Table table2 = new Table(2);
        table2.setSeat(p3, 'A');
        table2.setSeat(p4, 'B');

        Restaurant restaurant = new Restaurant("Good Morning Sunshine!");
        restaurant.setWaiter(waiter);
        restaurant.setTable(table, 1);
        restaurant.setTable(table2, 2);
        restaurant.printInfo();
	}
}
