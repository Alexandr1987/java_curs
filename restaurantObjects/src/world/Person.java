package world;

public class Person {
	  // properties
	 public String fullName;
	 private char gender;
	 private int age;
	 private String jobTitle;
	  
	  
	  public void setJobTitle(String x){
	      jobTitle = x;
	  }

	  public String  getJobTitle(){
	    return  jobTitle ;
	  }
	 
	  public int getAge() {
		return age;
	  }



	  public void setAge(int age) {
		if(age>=13 && age< 250) {
			this.age = age;
		}else {
			System.err.println("Check your age!");
		}
	  }



      public char getGender() {
		return gender;
      }



      public void setGender(char gender) {
		if(gender == 'm' || gender == 'f') {
			this.gender = gender;
		}else {
			System.err.println("Other is not implemented!");
		}
      }



	// constructor
	  public Person(String fullName,char gender,int age) {
	    this.fullName = fullName;
//	    this.gender = gender;
//	    this.age   = age;
		 setGender(gender);
		 setAge(age);
	  }  
	  public void  printInfo() {
		  System.out.println(fullName+": "+gender+", "+age+" years");
	  }
}
