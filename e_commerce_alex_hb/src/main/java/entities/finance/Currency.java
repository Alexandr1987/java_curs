package entities.finance;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
@Entity
public class Currency {
	@Id
	@GeneratedValue
	private int id;
	@Column(nullable = false,length = 40)
	private String name;
	@Column(nullable = false,length = 3)
	private char code;
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "sourceCurrency" )
	private List<Rate> exchangeRates = new ArrayList<>();
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public char getCode() {
		return code;
	}
	public void setCode(char code) {
		this.code = code;
	}
	public List<Rate> getExchangeRates() {
		return exchangeRates;
	}
	public void setExchangeRates(List<Rate> exchangeRates) {
		this.exchangeRates = exchangeRates;
	}
	public Currency(String name, char code) {
		super();
		this.name = name;
		this.code = code;
	}
	
	public Currency() {}
	@Override
	public String toString() {
		return "Currency [id=" + id + ", name=" + name + ", code=" + code + "]";
	}
	
	
}
