package entities.finance;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
@Entity
public class Money {
	@Id
	@GeneratedValue
	private int id;
	private Double value;
	@ManyToOne(fetch = FetchType.EAGER)
	private Currency currency;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Double getValue() {
		return value;
	}
	public void setValue(Double value) {
		this.value = value;
	}
	public Currency getCurrency() {
		return currency;
	}
	public void setCurrency(Currency currency) {
		this.currency = currency;
	}
	public Money(Double value, Currency currency) {
		this.value = value;
		this.currency = currency;
	}
	public Money() {

	}
	@Override
	public String toString() {
		return "Money [id=" + id + ", value=" + value + ", currency=" + currency.getCode()+ "]";
	} 
	
	
}
