package entities.finance;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.time.LocalDate;
@Entity
public class Rate {
	@Id
	@GeneratedValue
	private int id;
	
	private LocalDate date;

	private Double value;
	
	@ManyToOne(fetch = FetchType.EAGER)
	private Currency sourceCurrency;

	@ManyToOne(fetch = FetchType.EAGER)
	private Currency targetCurrency;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}

	public Currency getSourceCurrency() {
		return sourceCurrency;
	}

	public void setSourceCurrency(Currency sourceCurrency) {
		this.sourceCurrency = sourceCurrency;
	}

	public Currency getTargetCurrency() {
		return targetCurrency;
	}

	public void setTargetCurrency(Currency targetCurrency) {
		this.targetCurrency = targetCurrency;
	}

	public Rate(LocalDate date, Double value, Currency sourceCurrency, Currency targetCurrency) {
		this.date = date;
		this.value = value;
		this.sourceCurrency = sourceCurrency;
		this.targetCurrency = targetCurrency;
	}
	
	public Rate() {}

	@Override
	public String toString() {
		return "Rate [id=" + id + ", date=" + date + ", value=" + value + ", sourceCurrency=" + sourceCurrency
				+ ", targetCurrency=" + targetCurrency + "]";
	}
	
	
}
