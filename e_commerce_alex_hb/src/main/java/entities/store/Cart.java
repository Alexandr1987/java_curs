package entities.store;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
//import javax.persistence.Embeddable;

import entities.finance.Money;


@Entity
@Table(name="carts")
public class Cart {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="client_id")
	private Client client;
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "cart")
	private Set<CartItem> items = new HashSet<>();
	@Embedded
	@Column(columnDefinition = "smallint", nullable = false)
	private CartState status;
	@OneToOne(fetch = FetchType.EAGER)
	private Money totalCost;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Client getClient() {
		return client;
	}
	public void setClient(Client client) {
		this.client = client;
	}
	public Set<CartItem> getItems() {
		return items;
	}
	public void setItems(Set<CartItem> items) {
		this.items = items;
	}
	public CartState getStatus() {
		return status;
	}
	public void setStatus(CartState status) {
		this.status = status;
	}
	public Money getTotalCost() {
		return totalCost;
	}
	public void setTotalCost(Money totalCost) {
		this.totalCost = totalCost;
	}
	public Cart(Client client, Set<CartItem> items, CartState status, Money totalCost) {
		this.client = client;
		this.items = items;
		this.status = status;
		this.totalCost = totalCost;
	}
	public Cart() {
	}
	@Override
	public String toString() {
		return "Cart [id=" + id + ", client=" + client + ", items=" + items + ", status=" + status + ", totalCost="
				+ totalCost + "]";
	}
	
	
	
}
