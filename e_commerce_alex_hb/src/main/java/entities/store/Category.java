package entities.store;

import java.util.List;
import java.util.Set;
import java.util.ArrayList;
import java.util.HashSet;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name = "categories")
public class Category {
		 @Id
		 @GeneratedValue(strategy=GenerationType.AUTO)
		private Integer id;
	 
		 @Column(name = "title", length = 40)
		private String name;
		 
		 @OneToMany(fetch = FetchType.LAZY, mappedBy = "category")
		 private List<Product> products = new ArrayList<>();
	 
	 	@ManyToOne(fetch = FetchType.EAGER)
		private Category parent;

		@OneToMany(fetch = FetchType.EAGER, mappedBy = "parent")
		private Set<Category> subCategories = new HashSet<>();

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public List<Product> getProducts() {
			return products;
		}

		public void setProducts(List<Product> products) {
			this.products = products;
		}

		public Category getParent() {
			return parent;
		}

		public void setParent(Category parent) {
			this.parent = parent;
		}

		public Set<Category> getSubCategories() {
			return subCategories;
		}

		public void setSubCategories(Set<Category> subCategories) {
			this.subCategories = subCategories;
		}
		
		public Category() {}

		public Category(String name, Category parent) {
			this.name = name;
			this.parent = parent;
		}
		
		public Category(String name, List<Product> products, Category parent, Set<Category> subCategories) {
			this.name = name;
			this.products = products;
			this.parent = parent;
			this.subCategories = subCategories;
		}

		@Override
		public String toString() {
			return "Category [id=" + id + ", name=" + name + "]";
		}
	 
		
	 
}
