package entities.store;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
@Entity
@Table(name="addresses")
public class Address {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	 
	@Column(nullable = false, length = 50)
	private String city;
	
	@Column(length = 25)
	private String phone;
	
	@Column(nullable = false, length = 100)
	private String email;
	
	@OneToOne(fetch = FetchType.EAGER)	
	@JoinColumn(name="client_id",nullable = false)
	private Client client;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Client getClient() {
		return client;
	}
	public void setClient(Client client) {
		this.client = client;
	}
	public Address(String city, String phone, String email, Client client) {
		this.city = city;
		this.phone = phone;
		this.email = email;
		this.client = client;
	}
	public Address() {
	}
	@Override
	public String toString() {
		return "Address [id=" + id + ", city=" + city + ", phone=" + phone + ", email=" + email + ", client=" + client
				+ "]";
	}
	
	
	
	
}
