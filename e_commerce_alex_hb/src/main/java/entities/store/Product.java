package entities.store;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import entities.finance.Money;

@Entity
@Table(name = "products")
public class Product {
	 @Id
	 @GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	 @Column(nullable = false, length = 100)
	private String name;
	 @OneToOne(fetch = FetchType.EAGER)
	private Money price;
	 @ManyToOne(fetch = FetchType.LAZY)
	private Category category;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Money getPrice() {
		return price;
	}
	public void setPrice(Money price) {
		this.price = price;
	}
	public Category getCategory() {
		return category;
	}
	public void setCategory(Category category) {
		this.category = category;
	}
	public Product(String name, Money price, Category category) {
		this.name = name;
		this.price = price;
		this.category = category;
	}
	public Product(String name, Money price) {
		this.name = name;
		this.price = price;
	}
	public Product() {
	}
	@Override
	public String toString() {
		return "Product [id=" + id + ", name=" + name + ", price=" + price + ", category=" + category+"]";
	}
	
	

	
	
	
}
