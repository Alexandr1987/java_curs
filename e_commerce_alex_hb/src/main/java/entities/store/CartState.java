package entities.store;
import javax.persistence.Embeddable;
@Embeddable
public class CartState {
	public enum CartStateEnum {

		FRESH,ACTIVE,CANCELED,COMPLETED
	}

	private CartStateEnum state;


	public CartState() {

		this.state = CartStateEnum.FRESH;
	}

	public CartState(CartStateEnum state) {

		this.state = state;
	}

	public CartState(int state) {

		this.state = CartStateEnum.values()[state];
	}


	public CartStateEnum getState() {

		return state;
	}

	public void setState(int state) {

		this.state = CartStateEnum.values()[state];
	}

	public void setState(CartStateEnum state) {

		this.state = state;
	}

	@Override
	public String toString() {

		return  state.toString();
	}
}
