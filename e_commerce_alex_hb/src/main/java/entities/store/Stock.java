package entities.store;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Stock {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	 
	@Column(nullable = false)
	private int quantity;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public Stock(int quantity) {
		this.quantity = quantity;
	}
	
	public Stock() {}
	@Override
	public String toString() {
		return "Stock [id=" + id + ", quantity=" + quantity + "]";
	}
	
	
	
}
