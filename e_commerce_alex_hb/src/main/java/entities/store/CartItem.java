package entities.store;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import entities.finance.Money;

@Entity
@Table(name="cart_items")
public class CartItem {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	@OneToOne(fetch = FetchType.EAGER)
	private Product product;
	private Integer quantity;
	@ManyToOne(fetch = FetchType.EAGER)
	private Cart cart;
	@OneToOne(fetch = FetchType.EAGER)
	private Money totalCost;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public Cart getCart() {
		return cart;
	}
	public void setCart(Cart cart) {
		this.cart = cart;
	}
	public Money getTotalCost() {
		return totalCost;
	}
	public void setTotalCost(Money totalCost) {
		this.totalCost = totalCost;
	}
	public CartItem(Product product, Integer quantity, Cart cart, Money totalCost) {
		this.product = product;
		this.quantity = quantity;
		this.cart = cart;
		this.totalCost = totalCost;
	}
	public CartItem() {	}
 
	
}
