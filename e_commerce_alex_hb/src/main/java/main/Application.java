package main;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import entities.finance.Money;
import entities.store.Category;
import entities.store.Product;
import repositories.CategoryRepository;
import repositories.ProductRepository;


@SuppressWarnings("unused")
public class Application {

	public static void main(String[] args) {		
		
		EntityManagerFactory emf = Persistence.createEntityManagerFactory( "local-pg" );
		EntityManager em = emf.createEntityManager();
		
		ProductRepository pr = new ProductRepository();
		CategoryRepository cr = new CategoryRepository();
		System.out.println(cr.findById(4));

	}
	
	public static void fillLinear(int n ) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory( "local-pg" );
		EntityManager em = emf.createEntityManager();
		for (int i=1; i<=n; i++) {
		Category c = new Category();
		c.setName("Category"+i);
		Product p = new Product();
		p.setName("Product"+i);
		p.setCategory(c);

		em.getTransaction().begin();
		em.persist(c);
		em.persist(p);
		em.getTransaction().commit();
		}
	}

}
