package currencyConverter;

import entities.finance.Currency;
import entities.finance.Money;
import entities.finance.Rate;

public class MoneyConverter {

	public MoneyConverter() {}
	
	public Money convertCurrency(Money money, Rate rate, Currency toCurrency) {
		return new Money(money.getValue()*rate.getValue(),toCurrency);
	}
	// Singleton on demand
	public static class ConverterOnSingleton {
		public static MoneyConverter INSTANCE = new MoneyConverter();
	}
	
	public static MoneyConverter getInstance() {
		return ConverterOnSingleton.INSTANCE;
	}
}
