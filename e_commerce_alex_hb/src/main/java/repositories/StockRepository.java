package repositories;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import entities.store.Stock;

public class StockRepository {
	EntityManagerFactory emf = Persistence.createEntityManagerFactory( "local-pg" );
	EntityManager em = emf.createEntityManager();
	
	@SuppressWarnings("unchecked")
	public List<Stock> findAll(){
		List <Stock> stock = null;
		Query stocks = em.createNativeQuery("SELECT * FROM Stock");
		stock = stocks.getResultList();
		return stock;
	}
	
	public List<Stock> findByQuantity(Integer quantity) {
		List<Stock> quantityNr = null;
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Stock> myStock = cb.createQuery(Stock.class);
		Root <Stock> source = myStock.from(Stock.class);
		Predicate findByQuantity = cb.equal(source.get("quantity"), quantity);
		myStock
					  .select(source)
					  .where(findByQuantity);
		TypedQuery<Stock> stocks = em.createQuery(myStock);
		quantityNr = stocks.getResultList();
		return quantityNr;
	}
	
	public Stock findById (Integer id) {
		Stock stockId = null;
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Stock> myStock = cb.createQuery(Stock.class);
		Root <Stock> source = myStock.from(Stock.class);
		Predicate findId = cb.equal(source.get("id"),id);
		myStock
					.select(source)
					.where(findId);
		TypedQuery<Stock> stocks = em.createQuery(myStock);
		stockId =  stocks.getSingleResult();
		return stockId;
	}
	
	public Boolean remove (Stock stock) {
		em.getTransaction().begin();
		em.remove(em.contains(stock)? stock: em.merge(stock));
		em.getTransaction().commit();
		em.close();
		emf.close();
		return false;
	}
	
	public Boolean removeStockById(Integer id) {
		Stock s = em.find(Stock.class, id);
		em.getTransaction().begin();
		em.remove(s);
		em.getTransaction().commit();
		em.close();
		emf.close();
		return true;
	}
	
	public Boolean update(Stock stock) {
		em.getTransaction().begin();
		em.merge(stock);
		em.getTransaction().commit();
		em.close();
		emf.close();
		return false;
	}
	
	public Boolean create(Stock stock) {
		em.getTransaction().begin();
		em.persist(stock);
		em.getTransaction().commit();
		em.close();
		emf.close();
		return true;
	}
}
