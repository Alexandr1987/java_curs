package repositories;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import entities.store.Category;

public class CategoryRepository {
	EntityManagerFactory emf = Persistence.createEntityManagerFactory( "local-pg" );
	EntityManager em = emf.createEntityManager();
			
	@SuppressWarnings("unchecked")
	public List<Category> findAll(){
		List<Category> category = null;
		Query categories = em.createNativeQuery("Select * FROM categories",Category.class);
		category = categories.getResultList();
		return category;
		}
	
	public Category findByCategoryName(String name) {
		Category categoryName = null;
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Category> myCategory = cb.createQuery(Category.class);
		Root <Category> source = myCategory.from(Category.class);
		Predicate  findName = cb.equal(source.get("title"),name);
		myCategory
					.select(source)
					.where(findName);
		TypedQuery<Category> categories = em.createQuery(myCategory);
		categoryName = categories.getSingleResult();
		return categoryName;
	}

	public Category findById (Integer id) {
		Category categoryId = null;
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Category> myCategory = cb.createQuery(Category.class);
		Root <Category> source = myCategory.from(Category.class);
		Predicate findId = cb.equal(source.get("id"),id);
		myCategory
					.select(source)
					.where(findId);
		TypedQuery<Category> categories = em.createQuery(myCategory);
		categoryId =  categories.getSingleResult();
		return categoryId;
	}

	public Boolean remove (Category category) {
		em.getTransaction().begin();
		em.remove(em.contains(category)? category: em.merge(category));
		em.getTransaction().commit();
		em.close();
		emf.close();
		return false;
	}
	
	public Boolean removeCategoryById(Integer id) {
		Category c = em.find(Category.class, id);
		em.getTransaction().begin();
		em.remove(c);
		em.getTransaction().commit();
		em.close();
		emf.close();
		return true;
	}
	
	public Boolean update(Category category) {
		em.getTransaction().begin();
		em.merge(category);
		em.getTransaction().commit();
		em.close();
		emf.close();
		return false;
	}
	
	public Boolean create(Category category) {
		em.getTransaction().begin();
		em.persist(category);
		em.getTransaction().commit();
		em.close();
		emf.close();
		return true;
	}
	
	public List<Category> findSubcategories(Category category){
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Category> g = cb.createQuery(Category.class);
		Root <Category> source = g.from(Category.class);
		g.select(source);
		List<Category> cats = em.createQuery(g).getResultList();
		return cats;
	}
}
