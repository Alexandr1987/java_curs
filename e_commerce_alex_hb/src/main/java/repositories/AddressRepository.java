package repositories;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import entities.store.Address;

public class AddressRepository {
	EntityManagerFactory emf = Persistence.createEntityManagerFactory( "local-pg" );
	EntityManager em = emf.createEntityManager();
	
	@SuppressWarnings("unchecked")
	public List<Address> findAll(){
		List<Address> address = null;
		Query addresses = em.createNativeQuery("Select * FROM addresses",Address.class);
		address= addresses.getResultList();
		return address;
		}
	
	public Address findById (Integer id) {
		Address addressyId = null;
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Address> myAddress = cb.createQuery(Address.class);
		Root <Address> source = myAddress.from(Address.class);
		Predicate findId = cb.equal(source.get("id"),id);
		myAddress
					.select(source)
					.where(findId);
		TypedQuery<Address> addresses = em.createQuery(myAddress);
		addressyId =  addresses.getSingleResult();
		return addressyId;
	}
	
	public Address findByCity(String city) {
		Address myCity = null;
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Address> myAddress = cb.createQuery(Address.class);
		Root <Address> source = myAddress.from(Address.class);
		Predicate  findCity = cb.equal(source.get("city"),city);
		myAddress
					.select(source)
					.where(findCity);
		TypedQuery<Address> cities = em.createQuery(myAddress);
		myCity = cities.getSingleResult();
		return myCity;
	}
	
	public Address findByPhoneNumber(String phoneNumber) {
		Address phoneNr = null;
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Address> myAddress = cb.createQuery(Address.class);
		Root <Address> source = myAddress.from(Address.class);
		Predicate  findPhone = cb.equal(source.get("phone"),phoneNumber);
		myAddress
					.select(source)
					.where(findPhone);
		TypedQuery<Address> phones = em.createQuery(myAddress);
		phoneNr = phones.getSingleResult();
		return phoneNr;
	}
	
	public Address findByEmail(String email) {
		Address emailAddress = null;
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Address> myAddress = cb.createQuery(Address.class);
		Root <Address> source = myAddress.from(Address.class);
		Predicate  findEmail = cb.equal(source.get("email"),email);
		myAddress
					.select(source)
					.where(findEmail);
		TypedQuery<Address> emails = em.createQuery(myAddress);
		emailAddress = emails.getSingleResult();
		return emailAddress;
	}
	
	
	
}
