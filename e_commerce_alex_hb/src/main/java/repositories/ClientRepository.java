package repositories;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import entities.store.Client;

public class ClientRepository {
	EntityManagerFactory emf = Persistence.createEntityManagerFactory( "local-pg" );
	EntityManager em = emf.createEntityManager();
	
	@SuppressWarnings("unchecked")
	public List <Client> findAllClients (){
		List <Client> client = null;
		Query clients = em.createNativeQuery("SELECT * FROM clients",Client.class);
		client = clients.getResultList();
		return client;
	}
	
	public Client findByClientsName(String name) {
		Client clientsName= null;
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery <Client> myClient = cb.createQuery(Client.class);
		Root<Client> source = myClient.from(Client.class);
		Predicate findName = cb.equal(source.get("name"),name);
		myClient
				.select(source)
				.where(findName);
		TypedQuery<Client> clients = em.createQuery(myClient);
		clientsName = clients.getSingleResult();
		return clientsName;
		}
	
	public Client findById(Integer id) {
		Client clientId = null;
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery <Client>  myClient = cb.createQuery(Client.class);
		Root<Client> source = myClient.from(Client.class);
		Predicate findId = cb.equal(source.get("id"),id);
		myClient 
				.select(source)
				.where(findId);
		TypedQuery<Client> clients = em.createQuery(myClient);
		clientId = clients.getSingleResult();
		return clientId;
	}
	
	public Boolean remove (Client client) {
		em.getTransaction().begin();
		em.remove(em.contains(client)? client: em.merge(client));
		em.getTransaction().commit();
		em.close();
		emf.close();
		return false;
	}
	
	public Boolean removeClientById(Integer id) {
		Client c = em.find(Client.class, id);
		em.getTransaction().begin();
		em.remove(c);
		em.getTransaction().commit();
		em.close();
		emf.close();
		return true;
	}
	
	public Boolean update(Client client) {
		em.getTransaction().begin();
		em.merge(client);
		em.getTransaction().commit();
		em.close();
		emf.close();
		return false;
	}
	
	public Boolean create(Client client) {
		em.getTransaction().begin();
		em.persist(client);
		em.getTransaction().commit();
		em.close();
		emf.close();
		return true;
	}	
}
