package repositories;


import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import entities.store.Category;
import entities.store.Product;

public class ProductRepository {
	EntityManagerFactory emf = Persistence.createEntityManagerFactory( "local-pg" );
	EntityManager em = emf.createEntityManager();
				
		@SuppressWarnings("unchecked")
		public List <Product> findAll() {
			List <Product> product = null;
			Query products =  em.createNativeQuery("SELECT * FROM products",Product.class);
			product = products.getResultList();
			return product;				
		}
		
		public Product findByProductName(String namen) {
			Product productName = null;
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery <Product> myProduct = cb.createQuery(Product.class);
			Root <Product> source = myProduct.from(Product.class);
			Predicate findName = cb.equal(source.get("name"),namen);
			
			myProduct
					.select(source)
					.where(findName);
			TypedQuery<Product> products = em.createQuery(myProduct);
			productName=products.getSingleResult();
			return productName;
 		}
		
		public Product findById(Integer id) {
			Product productId = null;
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery <Product> myProduct = cb.createQuery(Product.class);
			Root <Product> source = myProduct.from(Product.class);
			Predicate findId = cb.equal(source.get("id"),id);
			
			myProduct
					.select(source)
					.where(findId);
			TypedQuery<Product> products = em.createQuery(myProduct);
			productId=products.getSingleResult();
			return productId;	
 		}	
		
		public Boolean remove(Product product) {
	         em.getTransaction().begin();
	 		 em.remove(em.contains(product) ? product: em.merge(product));
	 		 em.getTransaction().commit();
			 em.close();
			 emf.close();
			return false;
		}	
		
		public Boolean removeById(Integer id) {				
			Product p = em.find(Product.class, id);
			em.getTransaction().begin();
			em.remove(p);
			em.getTransaction().commit();
			em.close();
			emf.close();
			return true;
		}
		
		public Boolean update(Product product) {
			em.getTransaction().begin();
			em.merge(product);
			em.getTransaction().commit();
			em.close();
			emf.close();
			return true;
		}
		
		public Boolean create(Product product) {
			em.getTransaction().begin();
			em.persist(product);
			em.getTransaction().commit();
			em.close();
			emf.close();
			return true;
		}	
		
		public List<Product> findByCategory (Category category, Integer limit) {
			List<Product> products = null;
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery <Product> myProduct = cb.createQuery(Product.class);
			Root <Product> source = myProduct.from(Product.class);
			source.join("category");
			myProduct
					.select(source)
					.where(cb.equal(source.get("category"),category.getId()));
			TypedQuery<Product> myProduct1 = em.createQuery(myProduct);
			products = myProduct1
								.setMaxResults(limit)
								.getResultList();
			return products;
		}
		
		public List<Product> findSimilarProducts(Integer productId, Integer limit){
			List<Product> products = null;
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery <Product> g = cb.createQuery(Product.class);
			Root <Product> source = g.from(Product.class);
			g.select(source)
			 .where(cb.and(cb.notEqual(source.get("id"),productId)));
			TypedQuery<Product> q = em.createQuery(g);
			products = q.setMaxResults(limit)
						.getResultList();
			return products;
			}
}
