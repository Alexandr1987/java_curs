package main;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import entities.Category;

public class CLIApplication {

	public static void main(String[] args) {
		
		EntityManagerFactory sessionFactory = Persistence.createEntityManagerFactory( "dev.auto-moto" );
		EntityManager entityManager = sessionFactory.createEntityManager();
		entityManager.getTransaction().begin();
		
//		Category newCat = new Category(1,"Vasea Cat");
//		entityManager.persist(newCat);
		
//		Category newCat2 = new Category(2,"Vasea Cat2");
//		entityManager.persist(newCat2);
		
		Category newCat2 = entityManager.find(Category.class, 2);
		
		System.out.println(newCat2);
		
//		entityManager.detach(newCat2); // nefunctional
		
		entityManager.getTransaction().commit();
		entityManager.close();
		sessionFactory.close();

	}

}
