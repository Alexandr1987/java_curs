package tool;

public aspect LoggingAspect {
		pointcut whenMultiplyCalled():call(public static int multiply(int,int));
		
		before():whenMultiplyCalled(){
			System.out.println("MULTIPLICATION STARTED!!!");
		}
		
		after():whenMultiplyCalled(){
			System.out.println("MULTIPLICATION FINISHED!!!");
		}
}
