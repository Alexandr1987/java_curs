package collections;

public class SimplePositiveQueue {
	  // properties
	  private int length;

	  private int first;
	  private int middle;
	  private int last;
	public SimplePositiveQueue(int length, int first, int middle, int last) {
		this.length =  0;
		this.first = 1;
		this.middle = 1;
		this.last = 1;
	}
	
	public boolean push(int value) {
		 if (value >= 0) {
	            if (first >= 0) {
	                first = value;
	                return true;
	            } else if (middle >= 0) {
	                middle = value;
	                return true;
	            } else if (last >= 0) {
	                last = value;
	                return true;
	            } else {
	                return false;
	            }
	        } else {
	            System.err.println("Error! ");
	            return false;
	        }
		
		}
	public int poll() {
	       int temp = 0;
	        if(first != 0){
	            temp = first;
	            first = middle;
	            middle = last;
	            last = 0;
	        }
	        return temp > 0 ? temp : -1;
	}
	  
}
