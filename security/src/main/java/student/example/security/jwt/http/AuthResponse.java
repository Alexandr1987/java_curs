package student.example.security.jwt.http;

public class AuthResponse implements AuthResponseInterface{
	private String jwt;

	public String getJwt() {
		return jwt;
	}

	public void setJwt(String jwt) {
		this.jwt = jwt;
	}

	public AuthResponse(String jwt) {
		this.jwt = jwt;
	}

	public AuthResponse() {
	}

	@Override
	public String toString() {
		return "AuthResponse [jwt=" + jwt + "]";
	}
 
}
