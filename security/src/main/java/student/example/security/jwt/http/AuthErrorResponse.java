package student.example.security.jwt.http;

public class AuthErrorResponse implements AuthResponseInterface{
	private String error;

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public AuthErrorResponse(String error) {
		this.error = error;
	}

	public AuthErrorResponse() {
	}

	@Override
	public String toString() {
		return "AuthErrorResponse [error=" + error + "]";
	}
	
	
}
