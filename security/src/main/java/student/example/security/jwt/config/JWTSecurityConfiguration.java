package student.example.security.jwt.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import student.example.security.jwt.filters.JWTSecurityFilter;


@Configuration
@EnableWebSecurity
public class JWTSecurityConfiguration extends WebSecurityConfigurerAdapter{
	
	private final JWTSecurityFilter jwtFilter; 
	@Autowired
	public JWTSecurityConfiguration(JWTSecurityFilter jwtFilter) {
		this.jwtFilter=jwtFilter;
	}
	@Override
	protected void configure (AuthenticationManagerBuilder auth)throws Exception{
		PasswordEncoder encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
		
		auth
			.inMemoryAuthentication()
			.withUser("admin")
			.password(encoder.encode("admin"))
			.roles("HACKER");
		
	}
	
	@Override
	protected void configure(HttpSecurity http)throws Exception{
		http
			.csrf()
			.disable();
		
		http = http
				   .sessionManagement()
				   .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
				   .and();
		
		http.authorizeRequests()
		//accessible for all
				    .antMatchers("/auth")
				    .permitAll()
	    // accessible for authenticated users
				    .antMatchers("/api/*")
				    .authenticated()
				    	;
		
		http.addFilterBefore(jwtFilter, UsernamePasswordAuthenticationFilter.class);
		

	}
	@Override
	@Bean
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}
	
}
