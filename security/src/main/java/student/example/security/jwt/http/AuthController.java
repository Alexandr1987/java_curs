package student.example.security.jwt.http;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import student.example.security.jwt.JWTProvider;
import student.example.security.jwt.JwtUserDetailService;



@RestController
public class AuthController {
	@Autowired
	private JwtUserDetailService jwtUserService;
	
	@PostMapping("/auth")
	@ResponseBody
	public AuthResponseInterface authenticate(@RequestBody AuthRequest request) {
		
		try {
			UserDetails user = jwtUserService.loadUserByUsername(request.getUsername());
			
			
			if(request.getPassword().equals(user.getPassword()) ) {
				System.err.println(request);
				String jwtToken = JWTProvider.createSimpleToken(request.getUsername());
				return new AuthResponse(jwtToken);
			}
		}catch(UsernameNotFoundException e) {
			return new AuthErrorResponse("Bad Credentials");
		}
		return new AuthErrorResponse("Empty response");
	}
}
