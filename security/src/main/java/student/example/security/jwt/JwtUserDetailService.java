package student.example.security.jwt;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;



@Service
public class JwtUserDetailService implements UserDetailsService{
	
	private List<UserDetails> users ;
	public JwtUserDetailService() {
		users = new ArrayList<>();
		users.add(new User("admin","admin", new ArrayList<>()));
		users.add(new User("user","user", new ArrayList<>()));
	}
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		try {
		UserDetails userFound = users
										.stream()
										.filter(user -> user.getUsername().equals(username))
										.limit(1)
										.collect(Collectors.toList())
										.get(0);
									
		
		
		return userFound;
	}catch(Exception e) {
		throw new UsernameNotFoundException("User not found wit username"+username);
	}
	

	
	
}
}