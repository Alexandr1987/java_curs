package student.example.security.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;


@Component
public class HttpSecurityFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		
		String path = req.getRequestURI(); // get path
		
		
		
		
		System.err.println("HttpSecurityFilter BEFORE");
		if(path.equals("/profile")) {
//			req = new HttpServletRequestWrapper((HttpServletRequest) request) {
//				@Override
//				public String getRequestURI() {
//					return "/login";
//				}
//			};
//			res.setStatus(403);
			res.setStatus(403);
			res.sendRedirect("/login");
			
		}
		chain.doFilter(req,res);
		System.err.println("HttpSecurityFilter AFTER");
		
	}

}
