package myGui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyGUI implements ActionListener {
	
	private JFrame frame;
	private JButton button;
	private JButton button1;
	private JLabel label;
	private JLabel label1;
	private JPanel panel;
	private int count = 0;
	
	public MyGUI() {
		frame = new JFrame();
		panel = new JPanel();
		button = new JButton("Click");
		button1 = new JButton("Click1");
		button.addActionListener(this);
		button1.addActionListener(this);
		button.setPreferredSize(new Dimension(20, 50));
		label = new JLabel("Nr of clicks: 0");
		label1 = new JLabel("Nr of clicks: 0");
		panel.setBorder(BorderFactory.createEmptyBorder(50,50,50,50));
		panel.setLayout(new GridLayout(0,1));
		panel.add(button);
		panel.add(button1);
		panel.add(label);
		panel.add(label1);
		frame.add(panel, BorderLayout.CENTER);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("meine Kleine GUI");
		frame.pack();
		frame.setVisible(true);
		
	}
	public static void main(String[] args) {
		
		new MyGUI();
		
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		
		if(e.getSource() == button1) {
			count++;
			label1.setText("Nr of clicks: "+count);
		}else if(e.getSource() == button) {
			count++;
			label.setText("Nr of clicks: "+count);
			
		}

		
	}


}
