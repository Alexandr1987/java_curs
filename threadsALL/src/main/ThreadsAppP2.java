package main;

public class ThreadsAppP2 {

	public static void main(String[] args) throws InterruptedException {
		Plate commonPlate = new Plate(1000);
		Cat bars = new Cat("Bars", commonPlate);
		Dog mars = new Dog("Mars", commonPlate);
		System.out.println("Start"+ commonPlate);
		bars.start();
		mars.start();
		bars.join();
		mars.join();
		System.out.println("End: " + commonPlate);
	}

}
class Cat extends Thread{
	private String name;
	private Plate plate;
	public Cat(String name, Plate plate) {
		super();
		this.name = name;
		this.plate = plate;
	}
	@Override
	public String toString() {
		return "cat [name=" + name + "]";
	}
	
	public void eat() {
		while (plate.getFood()>0) {
			System.out.println("Cat is eating: "+plate.getFood());
			plate.setFood(plate.getFood()-1);
		}
	}
	@Override
	public void run() {
		this.eat();
		super.run();
	}
}
class Dog extends Thread{
	private String name;
	private Plate plate;
	public Dog(String name, Plate plate) {
		super();
		this.name = name;
		this.plate = plate;
	}
	@Override
	public String toString() {
		return "Dog [name=" + name + "]";
	}
	public void eat() {
		while (plate.getFood()>0) {
			System.out.println("Dog is eating: "+plate.getFood());
			plate.setFood(plate.getFood()-2);
		}
	}
	@Override
	public void run() {
		this.eat();
		super.run();
	}
}

class Plate {
	private int food;
	public Plate (int food) {
		super();
		this.food=food;
	}
	@Override
	public String toString() {
		return "Plate [food=" + food + "]";
	}
	public int getFood() {
		return food;
	}
	public void setFood(int food) {
		this.food = food;
	}
	
}