package util;

import java.lang.reflect.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ObjectReflection {
	public static String getObjectClass(Object object) {
//		System.out.println(object.getClass());  //object >class
//		System.out.println(object.getClass().getName()); //object > string
		
		return object.getClass().getName().substring(7);
	}
	
	public static  Map<String,String> getObjectPrivateFields(Object object)throws IllegalArgumentException, IllegalAccessException{
		Map<String,String> result = new HashMap<>();
		Field [] declaredFields = object.getClass().getDeclaredFields();
//		System.out.println(declaredFields.length);
		for (Field field: declaredFields) {
			if(!field.isAccessible()) {
				result.put(field.getName(), field.getType().getTypeName());
			}
		}
		return result;
	}
	
	
	public static  List<String> getObjectSetters(Object object)throws IllegalArgumentException, IllegalAccessException{
		List<String> result = new ArrayList<>();
		Method [] declaredMethods = object.getClass().getDeclaredMethods();
//		System.out.println(declaredMethods.length+"wow");
		for (Method method: declaredMethods) {
			if(!method.isAccessible() && method.getName().startsWith("set")) {
				result.add(method.getName());
			}
		}
		return result;
	}
	
	
} // end Class
