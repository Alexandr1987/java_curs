package util;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ClassReflection {
	public static String getMyClass(Object object) {
		
		return object.getClass().getName().toLowerCase().substring(7);
	}
	
	
	public static  Map<String,String> getClassPrivateFields(String className)throws IllegalArgumentException, IllegalAccessException, SecurityException, ClassNotFoundException{
		Map<String,String> result = new HashMap<>();
		Field [] declaredFields = Class.forName(className).getDeclaredFields();
//		System.out.println(declaredFields.length);
		for (Field field: declaredFields) {
			if(!field.isAccessible()) {
				result.put(field.getName(), field.getType().getTypeName());
			}
		}
		return result;
	}
	
	
	public static  List<String> getObjectSetters(Object object)throws IllegalArgumentException, IllegalAccessException{
		List<String> result = new ArrayList<>();
		Method [] declaredMethods = object.getClass().getDeclaredMethods();
		for (Method method: declaredMethods) {
			if(!method.isAccessible() && method.getName().startsWith("set")) {
				result.add(method.getName());
			}
		}
		return result;
	}
}
