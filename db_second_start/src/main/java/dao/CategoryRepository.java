package dao;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import connectivity.ConnectionTest;
import entity.Categories;
import readFile.ReadSQLFile;
import util.ClassReflection;
import util.ObjectReflection;
public class CategoryRepository {
	
	ConnectionTest ct = new ConnectionTest();
	ReadSQLFile SRF = new  ReadSQLFile();
	ClassReflection crfl= new ClassReflection();
	private List<Categories> list = new ArrayList<>();
		public Categories read(long Id,Object object) throws SQLException, IllegalArgumentException, IllegalAccessException, InvocationTargetException {	
		PreparedStatement st = ct.getConection().prepareStatement("SELECT * FROM "+ClassReflection.getMyClass(object)+" WHERE id=?");
		st.setLong(1, Id);
		ResultSet RS = st.executeQuery();	
		List<String> tab = ClassReflection.getObjectSetters(object);
//		System.out.println(tab+"!!!!!!!!!");
	
//		if(RS.next()) {
//			newCAt=new Categories();
//			newCAt.setId(RS.getLong("id"));
//			newCAt.setName(RS.getString("name"));			
//			newCAt.setParentCategoryId(RS.getLong("parent_category_id"));
//		}
	
		Categories category = null;
		Method[] methods = object.getClass().getMethods();
//		// list of set Methods	
//		Stream.of(methods)
//				.filter(method->method.getName().startsWith("set"))
//				.forEach(method->System.out.println(method.getName()));
//		
//		Stream.of(methods)
//		.filter(method->method.getName().startsWith("set"))
//		.forEach(method->System.out.println(method.getParameterTypes()[0].getName()));
////		//list of columns 
//		Stream.of(methods)
//		.filter(method->method.getName().startsWith("set"))
//		.forEach(method->System.out.println(method.getName().substring(3).toLowerCase().charAt(0)+method.getName().substring(4)));
//		System.out.println(methods[5].getName().startsWith("set"));
//		System.out.println(methods[5].getParameterTypes()[0].getName());
//		System.out.println(methods[5].getName().substring(3).toLowerCase().charAt(0)+methods[5].getName().substring(4));
		
		// TO DO USE HASHMAP
	 Map<String, String> resultPair = new HashMap<String, String>();

		for(Method method:methods) {
//			System.out.println(method);
			if(method.getName().startsWith("set")) {
				resultPair.put(method.getParameterTypes()[0].getName(), method.getName().substring(3).toLowerCase().charAt(0)+method.getName().substring(4));
				
			}

		};
//		System.out.println(resultPair);
		if(RS.next()) {
			category =new Categories();
				for (String i : resultPair.keySet()) {
					  if(i.equals("java.lang.String")) {
						category.setName(  RS.getString(resultPair.get(i)));
					  }else if(i.equals("int")) {
							category.setId(  RS.getInt(resultPair.get(i))); 
					  }
					  else if(i.equals("long")) {
							category.setParentCategoryId(  RS.getLong(resultPair.get(i)));
					  }
//					 System.out.println("key: " + i + " value: " + resultPair.get(i));
					}
	    }
//		System.out.println(RS.getString(method.getName().substring(3).toLowerCase().charAt(0)+method.getName().substring(4));

		st.close();
		return category;
		
	}

}
