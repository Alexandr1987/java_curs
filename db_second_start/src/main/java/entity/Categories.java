package entity;


public class Categories {
private int id;
private String name;
private long parentCategoryId;
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public long getParentCategoryId() {
	return parentCategoryId;
}
public void setParentCategoryId(long parentCategoryId) {
	this.parentCategoryId = parentCategoryId;
}
public Categories(int id, String name, long parentCategoryId) {
	this.id = id;
	this.name = name;
	this.parentCategoryId = parentCategoryId;
}
public Categories() {}
@Override
public String toString() {
	return "Categories [id=" + id + ", name=" + name + ", parentCategoryId=" + parentCategoryId + "]";
}

}
