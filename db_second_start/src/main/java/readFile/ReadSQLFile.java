package readFile;

import java.io.InputStream;
import java.util.Scanner;





@SuppressWarnings("all")

public class ReadSQLFile {

	public String readSQLstatement(String name) {
		 try {
		 ClassLoader classLoader = ReadSQLFile.class.getClassLoader();
		 InputStream inputStream = classLoader.getSystemResourceAsStream("sql/"+name+".sql");
	      Scanner myReader = new Scanner(inputStream);
	      while (myReader.hasNextLine()) {
	        String data = myReader.nextLine();
//	        System.out.println(data);
	        return data;
	        }
	      myReader.close();
		 }
		 catch(Exception e) {e.printStackTrace();}
		return name;
		
	 }
}
