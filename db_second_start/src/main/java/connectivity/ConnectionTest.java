package connectivity;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import confProvider.XMLConfProvider;

public class ConnectionTest {

	public Connection getConection() throws SQLException {
		String url = "jdbc:postgresql://localhost/auto-moto";
		Properties props = new Properties();
		props.setProperty("user",new XMLConfProvider().getKey("user"));
		props.setProperty("password",new XMLConfProvider().getKey("pass"));
		props.setProperty("ssl",new XMLConfProvider().getKey("SC"));
		Connection conn = DriverManager.getConnection(url, props);
		return conn;
	}
}
