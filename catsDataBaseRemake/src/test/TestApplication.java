package test;

import data.Cat;
import data.FelineInterface;
import persistance.FelineRepository;
import persistance.FelineRepositoryInterface;

public class TestApplication {

	public static void main(String[] args) throws CloneNotSupportedException{
//		FelineInterface cat = new Cat(1,"Murzik","British",2000);
//		FelineInterface cloned = cat.clone();
//		System.out.println("------------------");
//		System.out.println(cat);
//		System.out.println("------------------");
//		System.out.println(cloned);
//		System.out.println("------------------");
		FelineRepositoryInterface catRepo = new FelineRepository();
		((FelineRepository) catRepo).generate();
//		System.out.println(catRepo.findById(5));
//		System.out.println(catRepo.findByName("Мурзик"));
//		System.out.println(catRepo.findByRace("Персидская"));
		
		
		FelineRepository fr = new FelineRepository();
				fr.generate();
		FelineInterface original =  fr.findById(5);
									original.setYear(1900);
									original.setId(1000);
									fr.update(original);
									fr.save(original);
									fr.delete(fr.findById(5));
									fr.delete(fr.findById(6));
									System.out.println(fr.findAll());
									
	}

}
