package persistance;
import data.FelineInterface;

import java.util.List;
public interface FelineRepositoryInterface {
    boolean save(FelineInterface feline);

    boolean delete(FelineInterface feline);

    boolean update(FelineInterface feline); 

    List<FelineInterface> findAll() throws CloneNotSupportedException;

    FelineInterface findById(Integer id)throws CloneNotSupportedException;

    List<FelineInterface> findByName(String name)throws CloneNotSupportedException;

    List<FelineInterface> findByRace(String race)throws CloneNotSupportedException;
}
