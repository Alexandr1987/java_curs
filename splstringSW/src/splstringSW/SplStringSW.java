package splstringSW;
import java.util.ArrayList;
import java.util.Collections;
public class SplStringSW {
	public static void main(String[] args) {
		  ArrayList<String> devs = new ArrayList<String>(); 
		  // 1.      ^^^^ ??? 
		  // adding data to the list
		  devs.add("Johny");
		  devs.add("Valerya");
		  devs.add("Tatyana");
		  devs.add("Peter");
		  devs.add("Marry");		  
		  // showing the initials, use "charAt(index)" from "String" class
		  for( String myDevs : devs ) {          
		      System.out.println(myDevs.charAt(0));
		  }
		  System.out.println("-------------------------------------");
		  // the longest name, use "length()" from "String" class
		  int maxLength = 0;
		  ArrayList vals = new ArrayList<Integer>();
		  for( String myDevs : devs ) {          
		      System.out.println(myDevs.length());
		     
		      vals.add(myDevs.length());		      
		  }
		  Integer max = (Integer) Collections.max(vals);
		  System.out.println("-------------------------------------");
    	  System.out.println("Longest name consists of: "+max+" letters");//
    	  System.out.println("-------------------------------------");
		  String temp;
		  int last;
		  last = devs.size()-1;  // int
		  temp = devs.get(0); //string
		  System.out.println(devs);
		  Collections.swap(devs, 0, last);
		  System.out.println(last+""+temp);
		  System.out.println("-------------------------------------");
		  System.out.println(devs);



	}

}
