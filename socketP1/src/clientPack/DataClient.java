package clientPack;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.List;

public class DataClient {
private List <Integer> data;

public List<Integer> getData() {
	return data;
}

public void setData(List<Integer> data) {
	this.data = data;
}

public DataClient(List<Integer> data) {
	super();
	this.data = data;
}

@Override
public String toString() {
	return "DataClient [data=" + data + "]";
}

	public String dataProcessor() {
		StringBuilder sb = new StringBuilder();
		for(Integer i:getData()) {
			sb.append(i+",");
		}
//		System.out.println(sb.length()-1);
		return sb.substring(0, sb.length()-1).toString();	
	}

public void dataTransmiter () {
		try {
			Socket socket = new Socket("localhost",8080);
			System.out.println(">>> Client request");
			DataOutputStream dout = new DataOutputStream(socket.getOutputStream());
			String x = dataProcessor().toString();	
//			System.out.println(x);
			dout.writeUTF(x);
			dout.flush();
			socket.close();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


} // Last line
