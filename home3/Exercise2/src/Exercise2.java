
public class Exercise2 {

	public static void main(String[] args) {
		int distanceBetweenCitiesMiles = 200;
		final float kilometerToMile = 1.609344f;
		int speedKmPh = 160;
		
		float distanceInKm = distanceBetweenCitiesMiles * kilometerToMile;
		float timeSpendedInRoad = distanceInKm / speedKmPh;
		
		int hour = (int)timeSpendedInRoad;
		
		float min = (float)timeSpendedInRoad - hour;		
		int finalMinutes = (int)min;
		
		float seconds = min - (float)finalMinutes*60;
		int finalSeconds = (int)seconds;
		
		
		System.out.println("Time spended in road: "+hour+ "h "+ finalMinutes+"m "+finalSeconds+"s");
		
//		1.31 hours can be broken down to 1 hour plus 0.31 hours - 1 hour
//		0.31 hours * 60 minutes/hour = 18.6 minutes - 18 minutes
//		0.6 minutes * 60 seconds/minute = 36 seconds - 36 seconds

	}

}
