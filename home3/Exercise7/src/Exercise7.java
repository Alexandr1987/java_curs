
public class Exercise7 {

	public static void main(String[] args) {
	    // # DATA - CONSTANTS    
	    final int YEAR_DAYS = 365;
	    final int MONTH_DAYS = 30;
	    final int WEEK_DAYS = 7;    
	    

	    // # DATA - variables
	    int periodDays = 1234;
	    int toYears;
	    int toMonth;
	    int toMonthResult;
	    int toWeek;
	    int toWeekResult;
	    int toDays;

	    // # CALCULATIONS
	  
	    toYears = periodDays / YEAR_DAYS; 
	    toMonth = periodDays - toYears * YEAR_DAYS;
	    toMonthResult = (periodDays - toYears * YEAR_DAYS)/MONTH_DAYS;
	    toWeek  = toMonth - (toMonthResult * MONTH_DAYS);
	    toWeekResult = toWeek / WEEK_DAYS;
	    toDays = toWeek - (toWeekResult*WEEK_DAYS);
	    // # PRINT RESULTS
	    System.out.println("Result of division: "+toYears+" years "+toMonthResult+" months "+toWeekResult+" weeks "+toDays+" days");

	}

}
