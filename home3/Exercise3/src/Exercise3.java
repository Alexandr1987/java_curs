
public class Exercise3 {

	public static void main(String[] args) {
		int theoryHours = 100;
		int successfulExcercises = 75;
		int studentExamMark = 8;
		
		final int FULL_THEORY_WAGE = 200;
		final int MINIMUM_THEORY_WAGE = 100;
		final int FULL_EXERCISE_SUBMISSION = 120;
		final float MINIPAL_PERCENT_OF_ACCREDITATION = 70;
		final int MINIMAL_MARK_OF_ACCREDITATION = 7;
		
		boolean AcreditatinTheory = MINIMUM_THEORY_WAGE <= theoryHours && MINIMUM_THEORY_WAGE <= FULL_THEORY_WAGE ;
		double exercisePercentAcomplishment = successfulExcercises * 100/FULL_EXERCISE_SUBMISSION;
		boolean exerciseAcomplishment = MINIPAL_PERCENT_OF_ACCREDITATION <= exercisePercentAcomplishment;
		
		boolean ExamMark = studentExamMark >= MINIMAL_MARK_OF_ACCREDITATION;
		
		boolean finalAcredit = ExamMark && exerciseAcomplishment && AcreditatinTheory;
		System.out.println("Did the student pass acreditation? :" + finalAcredit);
	}

}
