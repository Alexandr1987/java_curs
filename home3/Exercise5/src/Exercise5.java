
public class Exercise5 {

	public static void main(String[] args) {
		 int result1 = (int)1.999999f;  // type casting doesn't do any rounding before truncating value after the decimal point.
		   System.out.println(result1);
		 int result2 = (int)1.99999999f;  // Ca si in exemplul precedent Ex4 cu 8 valori dupa virgula variabila se rotungeste automat
		   System.out.println(result2);

	}

}
