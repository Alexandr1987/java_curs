package alex.example.SubscriberRepo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import alex.example.domains.Subscriber;
import alex.example.domains.SubscriberRowMapper;
@Repository
public class SubscriberRepo {
	@Autowired
	 JdbcTemplate jdbcTemplate;
	
	
	    public List<Subscriber> findAll() {
	        String sql = "SELECT * FROM public.subscribers";
	        List<Subscriber> subscr = jdbcTemplate.query(
	                sql,
	                new SubscriberRowMapper());
			return subscr;
	    }
	    
	    public void save(Subscriber subscriber) {
	    	String sql1 =" INSERT INTO public.subscribers (name, email) VALUES ('"+subscriber.getName()+"', '"+ subscriber.getEmail()+"');";
	    	jdbcTemplate.update(sql1);
	    }
	    
	    public void updateName(String email,String newName) {
	    	System.out.println(email+" "+newName);
	    	String sql2 ="UPDATE subscribers SET name='"+newName+"' WHERE email='"+email+"';";
	    	System.out.println(sql2);
	    	jdbcTemplate.update(sql2);
	    }
	    
	    public void removeById(int id) {
	    	String sql3 ="DELETE FROM public.subscribers WHERE id="+id+";";
	    	jdbcTemplate.update(sql3);
	    }

		public void adminSubscribersSendMessage(String message, String email) {			
			String sql4 = "UPDATE subscribers SET message='"+message+"' WHERE email='"+email+"';";
//			jdbcTemplate.update(sql4);
		}

		public void updateEmail(int id, String email) {
	    	String sql5 ="UPDATE subscribers SET email='"+email+"' WHERE id='"+id+"';";
	    	jdbcTemplate.update(sql5);			
		}
		public List<String> getSubscribersEmailsByIds(List<Integer> ids) {
			String id_values="";
			for(Integer id :ids) {
				id_values += id+",";
			};
			id_values=id_values.substring(0,id_values.length()-1);
			
			String sql6 = "SELECT email FROM public.subscribers WHERE id IN ("+id_values+"); ";
			List<String> data = jdbcTemplate.queryForList(sql6,String.class);
				return data;

			
			
		}
		
		public Subscriber getSubscriberById(int id) {
			String sql2 ="SELECT * FROM public.subscribers WHERE id="+id+";";		
			Subscriber subscriber = jdbcTemplate.query(sql2,new SubscriberRowMapper()).get(0);
			return subscriber;                
		}
}
