package alex.example.SubscriberRepo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;

import alex.example.domains.Message;
import alex.example.domains.MessageRowMapper;
import alex.example.domains.Subscriber;
@Repository
public class MessageRepo {
    @Autowired
  	JdbcTemplate jdbcTemplate;
    @Autowired
    SubscriberRepo subsRepo;
	public void saveMessage(Message message) {
    	String sql1 =" INSERT INTO public.messages (id, content) VALUES ('"+message.getId()+"', '"+ message.getContents()+"');";
		jdbcTemplate.update(sql1);
	}
	
	public Message getMessageById(int id) {
		String sql2 ="SELECT * FROM public.messages WHERE id="+id+";";		
		Message mesg = jdbcTemplate.query(sql2,new MessageRowMapper()).get(0);
		return mesg;                
	}
	
	
	public List<Message> getMessages() {	        
		String sql2 = "SELECT * FROM public.messages";
		List<Message> msg = jdbcTemplate.query(sql2, new MessageRowMapper());
		return msg;
	}
	
	public Map<Subscriber,Message> getNextUnsentMessage()  {
		
		Map<Subscriber,Message> tuple = new HashMap<>();
		String sql2 ="SELECT subscriber_id, message_id FROM public.message_subscriber WHERE sent = false LIMIT 1 OFFSET 0;";	
		
		try {
			SqlRowSet result = jdbcTemplate.queryForRowSet(sql2);
			result.first();
			System.out.println(">> "+result.getInt("subscriber_id")+"<<>>"+result.getInt("message_id")+"<<");
			Message message = (Message) getMessageById(result.getInt("message_id"));
			Subscriber subscribs = (Subscriber) subsRepo.getSubscriberById(result.getInt("subscriber_id"));
			tuple.put(subscribs, message);
			System.out.println(tuple);
		} catch (ArrayIndexOutOfBoundsException AIOOBE) {
//			System.out.println(AIOOBE.toString());
			System.out.println("NO DATA PASSED FROM SQL STATEMENT!!!!");
		}
		return tuple;
	}
	
	public void updateMailSubscriberStatus(int id1, int id2) {
    	String sql ="UPDATE public.message_subscriber SET sent=true WHERE subscriber_id="+id1+"AND message_id="+id2+";";
    	jdbcTemplate.update(sql);			
	}
	
	
	
	
}
