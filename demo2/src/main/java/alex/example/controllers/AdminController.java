package alex.example.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import alex.example.SubscriberRepo.SubscriberRepo;

@Controller
public class AdminController {
	
@Autowired
SubscriberRepo subsRepo;
	
@PostMapping("/admin/subscribers/remove")
public String adminSubscriberRemove(@RequestParam int id){	
	subsRepo.removeById(id);	
	return "subscriber "+id+" removed !";
}

@PostMapping("/admin/subscribers/send")
public void adminSubscribersSendMessage(@RequestParam String message, @RequestParam String email){
 subsRepo.adminSubscribersSendMessage(message, email);
System.out.println(message+" "+email);	
}

@PostMapping("/admin/subscribers/setemail")
public String adminSubscriberSetEmail(@RequestParam int id, @RequestParam String email){
	subsRepo.updateEmail(id, email);	
	return "subscriber with"+id +"updated to new "+email+" !";
}


@GetMapping("/admin/subscribers")
public String adminSubscriberIndex(Model model){
	model.addAttribute("subscribers",subsRepo.findAll());
 return "admin/admin-subscribers";
}
@PostMapping("/admin/subscribers/compose")
public String adminSubscriberCompose(@RequestParam (required=true) List<Integer> subscribers){
	System.out.println(subscribers);
	List<String> emails = subsRepo.getSubscribersEmailsByIds(subscribers);
	System.out.println(emails);
 return "admin/compose";
}


}
