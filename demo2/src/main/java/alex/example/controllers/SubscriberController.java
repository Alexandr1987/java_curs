package alex.example.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import alex.example.SubscriberRepo.SubscriberRepo;
import alex.example.domains.Subscriber;

@RestController
public class SubscriberController {
	
@Autowired
SubscriberRepo subsRepo;
	
@GetMapping("/subscribers")
public List<Subscriber> allSubscribers (){
	List<Subscriber> subs = subsRepo.findAll();
	return subs;	
}
@PostMapping("/subscribers/add")
public String subscriberAdd(@RequestParam(required = true) String email,@RequestParam(required = true) String name){
	subsRepo.save(new Subscriber(name,email));
	return "subscriber added!" + email + " " + name;	
}

@GetMapping("/subscribers/setname/{email}/{newName}")
public String subscriberSetName(@PathVariable String email, @PathVariable String newName){
//	System.out.println(email+" "+newName);
	subsRepo.updateName(email,newName);
	return "subscriber updated!";	
}

}
