package alex.example.tasks;

import java.io.IOException;
import java.util.Map;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import alex.example.SubscriberRepo.MessageRepo;
import alex.example.domains.Message;
import alex.example.domains.Subscriber;

import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;	
@Component
public class EmailScheduler {
    @Autowired
    private JavaMailSender javaMailSender;
	@Autowired
	MessageRepo msgRepo;
	@Scheduled(fixedRate = 6*1000, initialDelay= 1*1000)
	public void sendEmail() throws MessagingException, IOException {
		
		System.err.println("prep to send email");
		Map<Subscriber,Message> tuple2 = msgRepo.getNextUnsentMessage();
		System.err.println(tuple2);
		SimpleMailMessage msg = new SimpleMailMessage();  
		msg.setFrom("norepl@goAway.com");
		try {
			msg.setTo(((Subscriber)tuple2.keySet().toArray()[0]).getEmail());
	        msg.setSubject("Testing from Spring Boot");
	        msg.setText( ((Message)tuple2.values().toArray()[0]).getContents());
	        System.out.println(((Subscriber)tuple2.keySet().toArray()[0]).getEmail());
	        
	        try{
//	            this.javaMailSender.send(msg);
	        	System.out.println(msg);
	            msgRepo.updateMailSubscriberStatus(((Subscriber)tuple2.keySet().toArray()[0]).getId(),((Message)tuple2.values().toArray()[0]).getId());
	        }
	        catch(MailException ex) {
	            // simply log it and go on...
	           ex.printStackTrace();           
	        }
	        
		}catch(ArrayIndexOutOfBoundsException ex) {
			System.err.println("ERROR NO DATA IN TUPLE");
		}



        System.out.println("Done");
	}
}
