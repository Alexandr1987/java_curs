package alex.example.domains;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class SubscriberRowMapper implements RowMapper<Subscriber>{
	   @Override
	    public Subscriber mapRow(ResultSet rs, int rowNum) throws SQLException {

		   Subscriber subscr = new Subscriber();
		   subscr.setId(rs.getInt("id"));
		   subscr.setName(rs.getString("name"));
		   subscr.setEmail(rs.getString("email"));
		   

	        return subscr;

	    }
}
