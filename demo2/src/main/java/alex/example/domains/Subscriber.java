package alex.example.domains;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;
@Table
public class Subscriber {
@Id
private int id;
@Column (value="name")
private String name;
@Column (value="email")
private String email;


public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public Subscriber(String name, String email) {
	this.name = name;
	this.email = email;
}
public Subscriber() {}
@Override
public String toString() {
	return "Subscriber [id=" + id + ", name=" + name + ", email=" + email + "]";
}
}
