package alex.example.domains;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;

public class Message {
	@Id
	private int id;
	@Column (value="contents")
	private String contents;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getContents() {
		return contents;
	}
	public void setContents(String contents) {
		this.contents = contents;
	}
	public Message(int id, String contents) {
		super();
		this.id = id;
		this.contents = contents;
	}
	public Message() {}
	@Override
	public String toString() {
		return "Message [id=" + id + ", contents=" + contents + "]";
	}
	



}
