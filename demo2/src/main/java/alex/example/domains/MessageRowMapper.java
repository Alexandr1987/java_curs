package alex.example.domains;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class MessageRowMapper implements RowMapper<Message>{
	   @Override
	    public Message mapRow(ResultSet rs, int rowNum) throws SQLException {

		   Message mesage = new Message();
		   mesage.setId(rs.getInt("id"));
		   mesage.setContents(rs.getString("content"));
		 
		   

	        return mesage;

	    }
}
