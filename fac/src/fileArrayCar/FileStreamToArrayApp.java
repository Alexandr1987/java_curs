package fileArrayCar;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class FileStreamToArrayApp {
	public static Car[] cars;
	public static void main(String[] args) {
		loadFileToArray("fac.txt");
		printArrayData();
	}
	
	public static void loadFileToArray( String filename ) {
		File file = new File("fac.txt");
		 if(!file.exists()) {
			 System.err.println("file does not exist!");
		 }else {
			 try {
				 Scanner fin = new Scanner(file);
				 int i=0;
				 cars = new Car[3]; 
					while(fin.hasNext()){						
						Car car = new Car();
						car.model = fin.next();
						car.year=fin.nextInt();
						car.km=fin.nextInt();
						car.price=fin.nextInt();
						cars[i++]=car;						
					}
				 fin.close();
			 } catch(IOException e) {
				 System.err.println(e);
			 }
		 }
	}	
    public static void printArrayData() {
    	for (Car car :cars) {
    		System.out.println(car);}
    }	
}
class Car{
	String model;
    Integer year;
    Integer km;
    Integer price;
    // добавить конструктор и toString()
    // БОНУС - валидировать данные (вдруг отрицательный год или прайс)
    
	public String getModel() {return model;}
	public void setModel(String model) {this.model = model;}
	public Integer getYear() {return year;}
	public void setYear(Integer year) {
			if (year>=0) {
				this.year = year;
			}else {
				System.err.println("negative year! not match ");
			}
		}
	public Integer getKm() {return km;}
	public void setKm(Integer km) {this.km = km;}
	public Integer getPrice() {return price;}
	public void setPrice(Integer price) {
		if(price > 0) {this.price = price;}else {System.err.println("price must be pozitive value");}
		
		}
    
	public Car(String model, Integer year, Integer km, Integer price) {
		super();
		this.model = model;
		this.year = year;
		this.km = km;
		this.price = price;
	}
	
    public Car() {	}
	@Override
	public String toString() {
		return "Car [model=" + model + ", year=" + year + ", km=" + km + ", price=" + price + "]";
	}
}