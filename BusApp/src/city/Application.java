package city;
import garage.Bus;
public class Application {

	public static void main(String[] args) {
		
//		testCase1();
//		testCase2();
		TestWheelsBasic();
	}

	
	public static void testCase1() {
		Bus.setSeats(40);
		Bus.enterPassenger(5);
		Bus.enterPassenger(2);
		Bus.enterPassenger(2);
		Bus.enterPassenger(2);
		Bus.exitPassenger(50);
		Bus.exitPassenger(1);
		System.out.println(Bus.getSeats());
	}
	
	public static void testCase2() {
		Bus.setSeats(2);
		for(int i=0; i<=20; i++) {
			Bus.enterPassenger(2);
		}
		for (int i=0; i<=20; i++) {
			Bus.exitPassenger(1);
		}
		System.out.println(Bus.getSeats());
	}
	
	public static void TestWheelsBasic(){
		  // 4 working wheels within parameters
		  Bus.Wheels.setWheelPSI(115.0, Bus.Wheels.FRONT_LEFT);  //FRONT_LEFT
		  Bus.Wheels.setWheelPSI(115.0, Bus.Wheels.FRONT_RIGHT);
		  Bus.Wheels.setWheelPSI(100.0, Bus.Wheels.BACK_LEFT_PRIMARY);
		  Bus.Wheels.setWheelPSI(100.0, Bus.Wheels.BACK_RIGHT_PRIMARY);
		  Bus.Wheels.setWheelPSI(0.0, Bus.Wheels.BACK_LEFT_SECONDARY);
		  Bus.Wheels.setWheelPSI(0.0, Bus.Wheels.BACK_RIGHT_SECONDARY);

		  // Too many passengers
		  Bus.setSeats(40);
		  System.out.println( Bus.isGoodToGo() );
		  // false

		  // less passengers
		  Bus.setSeats(29);
		  System.out.println( Bus.isGoodToGo() );
		  // true

		  // pressurizing back wheels
		  Bus.Wheels.setWheelPSI(80.0, Bus.Wheels.BACK_LEFT_SECONDARY);
		  Bus.Wheels.setWheelPSI(80.0, Bus.Wheels.BACK_RIGHT_SECONDARY);


		  // full bus
		  Bus.setSeats(45);
		  System.out.println( Bus.isGoodToGo() );
		  // true    


		}
	
}
