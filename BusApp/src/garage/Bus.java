package garage;

public class Bus {
	final static int MAX_SEATS = 45;
	final static int MIN_SEATS = 0;
	private static int routeNumber = 77;
	private static int seats = 0;
	
	public static class LEDPanel{
		public static void showRoute() {
			System.out.printf("%s\n","##################");
			System.out.printf("##        %d     ##\n",routeNumber);
			System.out.printf("%s\n","##################");
		}	
	}
	
	public static class Wheels{
		public final static int FRONT_LEFT = 1;
		public final static int FRONT_RIGHT = 2;
		public final static int BACK_LEFT_PRIMARY = 3;
		public final static int BACK_RIGHT_PRIMARY = 4;
		public final static int BACK_LEFT_SECONDARY = 5;
		public final static int BACK_RIGHT_SECONDARY = 6;
        
        // wheel pressure values
        private static double frontLeftPSI =0.0;
        private static double frontRightPSI = 0.0;
        private static double backLeftPrimaryPSI = 0.0;
        private static double backRightPrimaryPSI = 0.0;
        private static double backLeftSecondaryPSI = 0.0;
        private static double backRightSecondaryPSI = 0.0;
        
        
     // getter & setter
        public double getWheelPSI(int whichWheel, double pressure) {
     	  if(whichWheel == FRONT_LEFT) {
     		  return frontLeftPSI = pressure;
     	  }else if (whichWheel == FRONT_RIGHT) {
     		  return frontRightPSI = pressure;
     	  }else if (whichWheel == BACK_LEFT_PRIMARY) {
     		  return backLeftPrimaryPSI = pressure;
     	  }else if (whichWheel == BACK_RIGHT_PRIMARY) {
     		  return backRightPrimaryPSI = pressure;
     	  }else if (whichWheel == BACK_LEFT_SECONDARY) {
     		  return backLeftSecondaryPSI = pressure;
     	  }else if (whichWheel == BACK_RIGHT_SECONDARY) {
     		  return backRightSecondaryPSI = pressure;
     	  }else {
     		  System.err.println("something wrong!!!");
     	  }
     	  	  return pressure;
        }
        
        
        public static void setWheelPSI(double pressure, int whichWheel) {        	
        	  if(whichWheel == FRONT_LEFT) {
        		  if(frontLeftPSI>=0.0 && pressure <= 120.0 && FRONT_LEFT == whichWheel) {
        			  System.out.println("FRONT_LEFT wheel pressure is: " + pressure);
                      frontLeftPSI =  pressure;
        		  }else {
      			      System.err.println("Check pressure on front FRONT_LEFT ");
      		   	   }
         	  }else if (whichWheel == FRONT_RIGHT) {
	       		   if(frontRightPSI>=0.0 && pressure <= 120.0 && FRONT_RIGHT  == whichWheel) {
		   			   System.out.println("FRONT_RIGHT wheel pressure is: " + pressure);
		   			   frontRightPSI =  pressure;
	       		   }else {
	   			   System.err.println("Check pressure on FRONT_RIGHT wheel ");
	       		   }
         	  }else if (whichWheel == BACK_LEFT_PRIMARY) {
         		   if(backLeftPrimaryPSI>=0.0 && pressure <= 100.0 && BACK_LEFT_PRIMARY  == whichWheel) {
		      		   System.out.println("BACK_LEFT_PRIMARY wheel pressure is: " + pressure);
		      		   backLeftPrimaryPSI =  pressure;
	      		   }else {
	      			   System.err.println("Check pressure on BACK_LEFT_PRIMARY wheel ");
	      		   }
         	  }else if (whichWheel == BACK_RIGHT_PRIMARY) {
	       		   if(backRightPrimaryPSI>=0.0 && pressure <= 100.0 && BACK_RIGHT_PRIMARY  == whichWheel) {
		   			   System.out.println("BACK_RIGHT_PRIMARY wheel pressure is: " + pressure);
		   			   backRightPrimaryPSI =  pressure;
	       		   }else {
	       			   System.err.println("Check pressure on BACK_RIGHT_PRIMARY wheel ");
	       		   }
         	  }else if (whichWheel == BACK_LEFT_SECONDARY) {
	       		   if(backLeftSecondaryPSI>=0.0 && pressure <= 80.0 && BACK_LEFT_SECONDARY  == whichWheel) {
		   			   System.out.println("BACK_LEFT_SECONDARY wheel pressure is: " + pressure);
		   			backLeftSecondaryPSI =  pressure;
	       		   }else {
	       			   System.err.println("Check pressure on BACK_LEFT_SECONDARY wheel ");
	       		   }
         	  }else if (whichWheel == BACK_RIGHT_SECONDARY) {
	       		   if(backRightSecondaryPSI>=0.0 && pressure <= 80.0 && BACK_RIGHT_SECONDARY  == whichWheel) {
		   			   System.out.println("BACK_RIGHT_SECONDARY wheel pressure is: " + pressure);
		   			   backRightSecondaryPSI =  pressure;
	       		   }else {
	       			   System.err.println("Check pressure on BACK_RIGHT_SECONDARY wheel ");
	         	   }
         	  }else{
         		  System.err.println("something wrong!!!");
         	  }
       }
      
	}
	// getter & setter
	
	
	public static int getRouteNumber() {
		return routeNumber;
	}

	public static void setRouteNumber(int routeNumber) {
		if(routeNumber == 77 || routeNumber == 88 || routeNumber == 99) {
		Bus.routeNumber = routeNumber;
		}else {
			System.out.println("Route not available");
		}
	}

	public static int getSeats() {
		return seats;
	}

	public static void setSeats(int seats) {
		if(seats<=MAX_SEATS){Bus.seats = seats;}else if(seats>=MIN_SEATS){Bus.seats = seats;}else {System.out.println("not enough or too many passengers");}
	}

	public static void enterPassenger(int passenger) {
		if((passenger <= 1 || passenger <= 2  )&& seats + passenger <= MAX_SEATS ) {
			seats+=passenger;
		}else {
			System.err.println("To many! try 1 or 2 people");
		}
	}
	public static void exitPassenger(int passenger) {
		if((passenger <= 1 || passenger <= 2  )&& seats - passenger >= MIN_SEATS ) {
			seats-=passenger;
		}else {
			System.err.println("Not enough people");
		}
	}
	
	public static boolean isGoodToGo() {
		if (seats > 30 && 
						   Wheels.frontLeftPSI * 100 / 120 >= 95.0 && 
						   Wheels.frontRightPSI * 100 / 120 >= 95.0 && 
						   Wheels.backLeftPrimaryPSI * 100 / 100 >= 95.0 && 
						   Wheels.backRightPrimaryPSI * 100 / 100 >= 95.0 && 
						   Wheels.backLeftSecondaryPSI * 100 / 80 >= 95.0 && 
						   Wheels.backRightSecondaryPSI * 100 / 80 >= 95.0) {
			return true;
		}else if (seats <= 30 && 
				   Wheels.frontLeftPSI * 100 / 120 >= 95.0 && 
				   Wheels.frontRightPSI* 100 / 120 >= 95.0 && 
				   Wheels.backLeftPrimaryPSI * 100 / 100 >= 95.0&& 
				   Wheels.backRightPrimaryPSI * 100 / 100 >= 95.0){
			return true;
		}else {
			return false;
		}
	}
	
    
}
