package main;
import builderPattern.BulderPattern;
public class Application {

	public static void main(String[] args) {
		BulderPattern BP = new BulderPattern.Builder()
				.withBrand("Nokia")
				.withYear(2049)
				.withModel("Phoenix")
				.withDiagonal(30)
				.withMemory(500)
				.withCores(5)
				.withBattery(9)
				.build();
		
		System.out.println(BP);
	}

}
