package builderPattern;

public class BulderPattern {
	private String brand;
	private int year;
	private String model;
	private double diagonal;
	private int memory;
	private int cores;
	private int battery;

	
	
	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public double getDiagonal() {
		return diagonal;
	}

	public void setDiagonal(double diagonal) {
		this.diagonal = diagonal;
	}

	public int getMemory() {
		return memory;
	}

	public void setMemory(int memory) {
		this.memory = memory;
	}

	public int getCores() {
		return cores;
	}

	public void setCores(int cores) {
		this.cores = cores;
	}

	public int getBattery() {
		return battery;
	}

	public void setBattery(int battery) {
		this.battery = battery;
	}

	
	
//	public BulderPattern(String brand, int year, String model, double diagonal) {
//		this.brand = brand;
//		this.year = year;
//		this.model = model;
//		this.diagonal = diagonal;
//	}
//	
//	public BulderPattern(String brand, int year, String model, double diagonal, int memory, int cores, int battery) {
//		this.brand = brand;
//		this.year = year;
//		this.model = model;
//		this.diagonal = diagonal;
//		this.memory = memory;
//		this.cores = cores;
//		this.battery = battery;
//	}

	
	@Override
	public String toString() {
		return "BulderPattern [brand=" + brand + ", year=" + year + ", model=" + model + ", diagonal=" + diagonal
				+ ", memory=" + memory + ", cores=" + cores + ", battery=" + battery + "]";
	}



	public static class Builder {
		private BulderPattern newBulderPattern;
		
        public Builder() {
        	newBulderPattern = new BulderPattern();
        }
 
        public Builder withBrand(String brand){
        	newBulderPattern.brand = brand;
            return this;
        }
 
        public Builder withYear(int year){
        	newBulderPattern.year = year;
            return this;
        }
 
        public Builder withModel(String model){
        	newBulderPattern.model = model;
            return this;
        }
 
        public Builder withDiagonal(double diagonal){
        	newBulderPattern.diagonal = diagonal;
            return this;
        }
 
        public Builder withMemory(int memory){
        	newBulderPattern.memory = memory;
            return this;
        }
 
        public Builder withCores(int cores){
        	newBulderPattern.cores = cores;
            return this;
        }
        public Builder withBattery(int battery){
        	newBulderPattern.battery = battery;
            return this;
        }
 
        public BulderPattern build(){
            return newBulderPattern;
        }
	}
}
