package entity;

import entityInterface.PartInterface;

public class Part implements PartInterface{
private long id;
private String name;
private long categoryId;
public long getId() {
	return id;
}
public void setId(long id) {
	this.id = id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public long getCategoryId() {
	return categoryId;
}
public void setCategoryId(long categoryId) {
	this.categoryId = categoryId;
}
public Part(long id, String name, long categoryId) {
	this.id = id;
	this.name = name;
	this.categoryId = categoryId;
}
public Part() {}
@Override
public String toString() {
	return "Parts [id=" + id + ", name=" + name + ", categoryId=" + categoryId + "]";
}

}
