package entity;

import entityInterface.CategoryInterface;

public class Category implements CategoryInterface {
private long id;
private String name;
private long parentCategoryId;
public long getId() {
	return id;
}
public void setId(long id) {
	this.id = id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public long getParentCategoryId() {
	return parentCategoryId;
}
public void setParentCategoryId(long parentCategoryId) {
	this.parentCategoryId = parentCategoryId;
}
public Category(long id, String name, long parent_category_id) {
	this.id = id;
	this.name = name;
	this.parentCategoryId = parent_category_id;
}
public Category() {}
@Override
public String toString() {
	return "Categories [id=" + id + ", name=" + name + ", parent_category_id=" + parentCategoryId + "]";
}

}
