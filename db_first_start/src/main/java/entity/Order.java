package entity;

import java.io.IOException;

import entityInterface.OrderInterface;

public class Order implements OrderInterface{
private long id;
private long quantity;
private long partId;

public long getId() {
	return id;
}
public void setId(long id) {
	this.id = id;
}
public long getQuantity() {
	return quantity;
}
public void setQuantity(long quantity) throws IOException {
	
	if(quantity<=0) {
			System.out.println("no more items in q_stock");
	}else {
			this.quantity = quantity;
	}
}
public long getPartId() {
	return partId;
}
public void setPartId(long partId) {
	this.partId = partId;
}
public Order(long id, long quantity, long partId) {
	this.id = id;
	this.quantity = quantity;
	this.partId = partId;
}
public Order() {}
@Override
public String toString() {
	return "Orders [id=" + id + ", quantity=" + quantity + ", partId=" + partId + "]";
}


}
