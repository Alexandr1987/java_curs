package entity;

import entityInterface.PartStockInterface;

public class PartStock implements PartStockInterface {
private long id;
private long quantity;
public long getId() {
	return id;
}
public void setId(long id) {
	this.id = id;
}
public long getQuantity() {
	return quantity;
}
public void setQuantity(long quantity) {
	this.quantity = quantity;
}
public PartStock(long id, long quantity) {
	this.id = id;
	this.quantity = quantity;
}
public PartStock() {}
@Override
public String toString() {
	return "PartsStock [id=" + id + ", quantity=" + quantity + "]";
}

}
