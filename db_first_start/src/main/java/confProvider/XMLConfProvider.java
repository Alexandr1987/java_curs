package confProvider;
import java.io.File;
import java.io.InputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
  
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
@SuppressWarnings("all")
public class XMLConfProvider {



		public  String getKey(String name) {
			 try {
			 ClassLoader classLoader = XMLConfProvider.class.getClassLoader();
//			 File file = new File(classLoader.getResource("xml_data/nasa.xml").getFile());
			 InputStream inputStream = classLoader.getSystemResourceAsStream("ConnParam/ConnParam.xml");
			 DocumentBuilderFactory factory =  DocumentBuilderFactory.newInstance();
			 DocumentBuilder builder = factory.newDocumentBuilder();
//			 Document doc = builder.parse(file); 
			 Document doc = builder.parse(inputStream); 
			 NodeList nList = doc.getElementsByTagName("connection");
			 		 Node nNode=nList.item(0);
					 Element eElement = (Element)nNode;				 
					 if(name == "user") {
						 name = eElement.getElementsByTagName("user").item(0).getTextContent();
						 return name;
					 }else if(name == "pass") {
						 name = eElement.getElementsByTagName("pass").item(0).getTextContent();
						 return name;
					 }
					 else if(name == "CS") {
						 name = eElement.getElementsByTagName("SC").item(0).getTextContent();
						 return name;
					 }
			 }
			 catch(Exception e) {e.printStackTrace();}
			return name;
			
		 }
		
	}

