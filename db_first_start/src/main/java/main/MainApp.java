package main;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import dao.CategoryRepository;
import entity.Category;
import entityInterface.CategoryInterface;
import readFile.ReadSQLFile;
@SuppressWarnings("all")
public class MainApp {

	public static void main(String[] args) throws SQLException {
		CategoryInterface testCat = new Category(1, "Test 1categowry", 1);
		CategoryRepository cr = new CategoryRepository();
	
//		CREATE
//		cr.create(testCat);		
		
//		UPDATE
//		CategoryInterface newCat = new Category(102, "grigore",0);
//		cr.update(newCat);
		
//		READ		
//		CategoryInterface Found =  cr.read(101);	
//		System.out.println(Found);
		
//		List<CategoryInterface> cats;
//        cats = cr.findAll();
//        for(CategoryInterface c: cats){ System.out.println(c); }
		
//		CategoryInterface cat = new Category(0,"", 1);
//		List<CategoryInterface> cats;
//		cats = cr.findAllByParent(cat);
//		 for(CategoryInterface c: cats){ System.out.println(c); }
		
//		DELETE		
//		cr.delete(200);
		
//		SAVE
		CategoryInterface category = new Category(106, "jaddvsfmafenddaalf",1);
		cr.save(category);
		
		
	}

}
