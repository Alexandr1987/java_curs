package dao;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import connectivity.ConnectionTest;
import entity.Category;
import entityInterface.CategoryInterface;
import readFile.ReadSQLFile;
public class CategoryRepository {
	
	ConnectionTest ct = new ConnectionTest();
	ReadSQLFile SRF = new  ReadSQLFile();
	private List<CategoryInterface> list = new ArrayList<>();
	
	
	public List<CategoryInterface> findAll() throws SQLException {

		String stmt = SRF.readSQLstatement("findAll");
		PreparedStatement st = ct.getConection().prepareStatement(stmt);
		ResultSet rs = st.executeQuery();
		
		CategoryInterface newCat = null;
		list.clear();
		while (rs.next()) {
			newCat = new Category();
			newCat.setId(rs.getLong("id"));
			newCat.setName(rs.getString("name"));
			newCat.setParentCategoryId(rs.getLong("parent_category_id"));
			list.add((Category) newCat);
		}
		st.close();
		return list;
	}
	public List<CategoryInterface> findAllByParent(CategoryInterface category) throws SQLException{

		String stmt = SRF.readSQLstatement("findAllByParent");
		PreparedStatement st = ct.getConection().prepareStatement(stmt);
		st.setLong(1, category.getParentCategoryId());
		ResultSet rs = st.executeQuery();
		category = null;
		list.clear();
		while (rs.next()) {
			category = new Category();
			category.setId(rs.getLong("id"));
			category.setName(rs.getString("name"));
			category.setParentCategoryId(rs.getLong("parent_category_id"));
			list.add(category);
		}
		st.close();
		return list;
	}	

	
	public CategoryInterface read(long Id) throws SQLException {		
		String stmt =SRF.readSQLstatement("readFromCategoryById");
		PreparedStatement st = ct.getConection().prepareStatement(stmt);
		st.setLong(1, Id);
		ResultSet RS = st.executeQuery();		
		CategoryInterface newCAt= null;
		if(RS.next()) {
			newCAt=new Category();
			newCAt.setId(RS.getLong("id"));
			newCAt.setName(RS.getString("name"));			
			newCAt.setParentCategoryId(RS.getLong("parent_category_id"));
		}
		st.close();
		return newCAt;
		
	}
	public boolean create(CategoryInterface category) throws SQLException {		
		String stmt =SRF.readSQLstatement("createToCategory");
		PreparedStatement st = ct.getConection().prepareStatement(stmt);
		st.setLong(1, category.getId());
		st.setString(2, category.getName());
		st.setLong(3, category.getParentCategoryId());
		return st.execute();																	
	}
	public boolean update(CategoryInterface category) throws SQLException {		
		String stmt = SRF.readSQLstatement("update");
		PreparedStatement st = ct.getConection().prepareStatement(stmt);
		st.setString(1, category.getName());
		st.setLong(2, category.getId());
		st.executeUpdate();			
		st.close();
		return true;
	}
	public boolean delete(long id) throws SQLException {
		String stmt =SRF.readSQLstatement("deleteFromCategoryById");
		PreparedStatement st = ct.getConection().prepareStatement(stmt);
		st.setLong(1, id);
		st.executeUpdate();
		st.close();
		return false;
	}	
	
	
	public void save(CategoryInterface category1) throws SQLException{
		CategoryInterface currentCat = read(category1.getId());
		if(currentCat==null) {
			create(category1);
		}else {
			update(category1);
		}
		
	}
}
