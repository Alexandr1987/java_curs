package entityInterface;

public interface PartStockInterface {
	long getId();
	void setId(long id);
	long getQuantity();
	void setQuantity(long quantity);
}
