package entityInterface;

import java.io.IOException;

public interface OrderInterface {
	long getId();
	void setId(long id);
	long getQuantity();
	void setQuantity(long quantity)throws IOException;
	long getPartId();
	void setPartId(long partId);
}
