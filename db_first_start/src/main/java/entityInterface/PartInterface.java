package entityInterface;

public interface PartInterface {
	long getId();
	void setId(long id);
	String getName();
	void setName(String name);
	long getCategoryId();
	void setCategoryId(long categoryId);
}
