package entityInterface;

public interface CategoryInterface {
	
	long getId();
	void setId(long id);
	String getName();
	void setName(String name);
	long getParentCategoryId();
	void setParentCategoryId(long parentCategoryId);
}
