package student.minisocial.minisocial.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import student.minisocial.minisocial.dao.UserRepository;
import student.minisocial.minisocial.domain.User;
@Service
public class UserService {
	@Autowired
	UserRepository userRepository;

	public List<User> findAllUsers() {return (List<User>) userRepository.findAll();};
	public User findUserById(long id) {return userRepository.getOne(id);};
	public void deleteUserById(long id) {userRepository.deleteById(id);;};
	public void saveUser(User user) {userRepository.saveAndFlush(user);};	
	public void update(long id, User user) {
		User oldUser = userRepository.getOne(id);
		User updateUser = new User();
		updateUser.setId(oldUser.getId());
		updateUser.setNickname(user.getNickname());
		updateUser.setEmail(user.getEmail());
		updateUser.setAvatar(user.getAvatar());
		userRepository.saveAndFlush(updateUser);
		
	};
	

}
