package student.minisocial.minisocial.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import student.minisocial.minisocial.domain.User;

public interface UserRepository extends JpaRepository<User, Long>{

}
