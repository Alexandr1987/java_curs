package student.minisocial.minisocial.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import student.minisocial.minisocial.domain.User;
import student.minisocial.minisocial.services.UserService;

@RestController		
public class UserController {
	@Autowired
	UserService userService;
	
	@GetMapping("/users")
	public List<User>index(){ 
		return (List<User>)userService.findAllUsers();
	}
	
	@RequestMapping(value="/users/{id}")
	public User show(@PathVariable("id") long id){
		return null;
	}
	
	@PostMapping("/users/new")
	public void create(@RequestParam String nickname, @RequestParam String email, @RequestParam String avatar){
		User user = new User();
		user.setNickname(nickname);
		user.setEmail(email);
		user.setAvatar(avatar);
		userService.saveUser(user);	
	}
	
	@RequestMapping(value="/users/delete/{id}")
	public void destroy(@PathVariable("id") long id) {userService.deleteUserById(id);}
	
	@RequestMapping(value="/users/update/{id}")
	public String update(@RequestBody User user, @PathVariable int id) {
		userService.update(id,user);
		return "User update in progres...";
	}
	
}
