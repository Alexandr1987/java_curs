package student.minisocial.minisocial.domain;

import javax.persistence.Entity;
import javax.persistence.Id;

import student.minisocial.minisocial.domainInterfaces.IsCommentable;

@Entity(name="UserAccount")
public class User implements IsCommentable{


@Id
private long id;
private String nickname;
private String email;
private String avatar;

public long getId() {
	return id;
}
public void setId(long id) {
	this.id = id;
}
public String getNickname() {
	return nickname;
}
public void setNickname(String nickname) {
	this.nickname = nickname;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public String getAvatar() {
	return avatar;
}
public void setAvatar(String avatar) {
	this.avatar = avatar;
}
public User(long id, String nickname, String email, String avatar) {
	super();
	this.id = id;
	this.nickname = nickname;
	this.email = email;
	this.avatar = avatar;
}
public User() {
	super();
	// TODO Auto-generated constructor stub
}
@Override
public String toString() {
	return "UserAccount [id=" + id + ", nickname=" + nickname + ", email=" + email + ", avatar=" + avatar + "]";
}



}
