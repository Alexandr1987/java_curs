package student.minisocial.minisocial.domain;

import javax.persistence.Entity;
import javax.persistence.Id;

import student.minisocial.minisocial.domainInterfaces.HasAuthor;
import student.minisocial.minisocial.domainInterfaces.HasSentiment;
import student.minisocial.minisocial.domainInterfaces.IsChronological;
import student.minisocial.minisocial.domainInterfaces.IsCommentable;

@Entity(name="comment")
public class Comment implements IsCommentable, IsChronological, HasSentiment, HasAuthor{
@Id
private long commentId;
private String commentBody;
public long getCommentId() {
	return commentId;
}
public void setCommentId(long commentId) {
	this.commentId = commentId;
}
public String getCommentBody() {
	return commentBody;
}
public void setCommentBody(String commentBody) {
	this.commentBody = commentBody;
}
public Comment(long commentId, String commentBody) {
	super();
	this.commentId = commentId;
	this.commentBody = commentBody;
}
public Comment() {
	super();
	// TODO Auto-generated constructor stub
}
@Override
public String toString() {
	return "Comment [commentId=" + commentId + ", commentBody=" + commentBody + "]";
}

}
