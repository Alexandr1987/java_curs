package student.minisocial.minisocial.domain;

import javax.persistence.Entity;
import javax.persistence.Id;

import student.minisocial.minisocial.domainInterfaces.HasAuthor;
import student.minisocial.minisocial.domainInterfaces.HasSentiment;
import student.minisocial.minisocial.domainInterfaces.IsChronological;
import student.minisocial.minisocial.domainInterfaces.IsCommentable;

@Entity(name="post")
public class Post implements IsCommentable, IsChronological, HasSentiment, HasAuthor{
@Id
private long postId;
private String title;
private String body;
public long getPostId() {
	return postId;
}
public void setPostId(long postId) {
	this.postId = postId;
}
public String getTitle() {
	return title;
}
public void setTitle(String title) {
	this.title = title;
}
public String getBody() {
	return body;
}
public void setBody(String body) {
	this.body = body;
}
public Post(long postId, String title, String body) {
	super();
	this.postId = postId;
	this.title = title;
	this.body = body;
}
public Post() {
	super();
	// TODO Auto-generated constructor stub
}
@Override
public String toString() {
	return "Post [postId=" + postId + ", title=" + title + ", body=" + body + "]";
}


}
