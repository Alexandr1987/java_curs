package serverPack;

import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


public class ServerApp {
	JSONObject json = new JSONObject();
	static JSONParser parser = new JSONParser();
	static List<Integer> data = new ArrayList<>();
	static ArrayList<Integer> data2 = new ArrayList<>();

	@SuppressWarnings("unchecked")
	public static void main(String[] args) throws EOFException, ParseException, InterruptedException, ClassNotFoundException {
		ServerSocket ss;
		 
		try {
			ss = new ServerSocket(8080);
			System.out.println("Server: waiting for client:");
			Socket s = ss.accept();
			System.out.println("Server: client connected!!!");
			ObjectInputStream in = new ObjectInputStream(s.getInputStream());
			JSONObject line = (JSONObject) in.readObject();
			ObjectOutputStream out = new ObjectOutputStream(s.getOutputStream());
			System.out.println(line.get("option"));
			data = (List<Integer>) line.get("data");
			if(line.get("option").equals("positive")) {
				System.out.println(data);
				data.stream().filter(x->{return x>0;}).forEach(x->{
					 data2.add(x);				
				});
			}else {
				data.stream().filter(x->{return x<0;}).forEach(x->{
					 data2.add(x);
					});
			}  
			int counter=0;
			double sum = 0;
			double result = 0;
		    for(int i = 0; i < data2.size(); i++)
		    {
		    	counter++;
		        sum = sum + data2.get(i);
		        result = sum / counter;
		    }
			System.out.println(result);
			JSONObject json2 = new JSONObject();  
			json2.put("avg",result);	
			    out.writeObject(json2);
			    System.out.println("Server: data sended to client- "+json2.toString());
			    out.flush();
			    
			
			Thread.sleep(5000);
			System.out.println("App Closed");
			s.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	

		}
}
