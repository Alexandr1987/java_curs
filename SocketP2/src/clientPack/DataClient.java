package clientPack;

import java.util.List;


public class DataClient {

private List <Integer> data;

public List<Integer> getData() {
	return data;
}

public void setData(List<Integer> data) {
	this.data = data;
}

public DataClient(List<Integer> data) {
	super();
	this.data = data;
}

@Override
public String toString() {
	return "DataClient [data=" + data + "]";
}
} // Last line
