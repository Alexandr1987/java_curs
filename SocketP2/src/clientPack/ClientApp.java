package clientPack;


import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Arrays;
import java.util.List;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

public class ClientApp {

	@SuppressWarnings("all")
	public static void main(String[] args) throws ClassNotFoundException, IOException, InterruptedException, ParseException{
		List<Integer> data = Arrays.asList(-2,1,-4,3,-6,5,8,7);
		DataClient dt= new DataClient(data);
		Socket s = new Socket("localhost", 8080 );
		JSONObject json = new JSONObject();
		json.put("data",dt.getData());
		json.put("option", "positive");	
		
		ObjectOutputStream out = new ObjectOutputStream(s.getOutputStream());
		out.writeObject(json);
		out.flush();
				
		ObjectInputStream i = new ObjectInputStream(s.getInputStream());
		JSONObject line = (JSONObject) i.readObject();
		System.out.println("Client: Data recived: The average is: "+line.get("avg")+" units");
		s.close();
		
		
	}
}
