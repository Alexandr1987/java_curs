package main;

import java.util.stream.Stream;

import com.github.javafaker.Faker;

import domain.Page;

public class Application {
	static Faker faker = new Faker();
	public static void main(String[] args) {
		//SUPPLY
		Stream <Page> pages = Stream.generate(()->{			
			return new Page(faker.lorem().sentence(5),faker.lorem().paragraph(3));
			});
		//PROCESS
		pages = pages
				.filter(v->{
					return v.getTitle().startsWith("A"); 	
				})
				.map(v->{
					v.setBody(v.getBody().substring(0,10));
					return v;
				})
				.map(v->{
					v.setTitle(v.getTitle().substring(0,5).toUpperCase());
					return v;
				});
		
		
		//CONSUME
		pages.forEach((v)->{System.out.println(v);});
	}

}
