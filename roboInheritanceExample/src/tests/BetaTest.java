package tests;
import robo.BetaRobot;
public class BetaTest {
    public static int runCase2(BetaRobot robot){
        int percentage = 0;
        robot.setCharge((byte)-100);
        if (robot.getCharge() < 0 && robot.getCharge() > 100){
            System.err.println("setCharge() test failed\nREASON: Value is out of bound!");
            return percentage;
        }
        percentage += 35;

        robot.setCharge((byte)1000);
        if (robot.getCharge() < 0 && robot.getCharge() > 100){
            System.err.println("setCharge() test failed\nREASON: Value is out of bound!");
            return percentage;
        }
        percentage += 35;

        robot.setCharge((byte)50);
        boolean temp;
        do {
            temp = robot.moveRight();
        }while (temp);
        System.out.println("Charge level is "+robot.getCharge());
        return percentage += 30;
    }
}
