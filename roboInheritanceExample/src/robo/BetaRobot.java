package robo;

 public class BetaRobot extends AlphaRobot{
	
	private byte charge;
	protected int stepNum = 0;
	public byte getCharge() {
		return charge;
	}

	public void setCharge(byte charge) {
		if(charge>=0 && charge <=100) {this.charge = charge;}else {System.err.println("charge out of bounds");}
	}

	public BetaRobot(String name, String model) {
		super(name, model);
		this.charge = 0;
	}
	@Override
	public boolean moveUp() {
		if (this.y > 0 && getCharge()>=5) {
			this.y+=1;
			stepNum ++;
			if(stepNum % 2 == 0 ) {
				charge -=1;
			}
			return true;
		}else return false;
	}
	@Override
	public boolean moveDown() {
		if (this.y <100 && getCharge()>=5) {
			this.y-=1;
			stepNum ++;
			if(stepNum % 2 == 0 ) {
				charge -=1;
			}
			return true;
		}else return false;
	}
	@Override
	public boolean moveRight() {
		if (this.x <100 && getCharge()>= 5) {
			this.x+=1;
			stepNum ++;
			if(stepNum % 2 == 0 ) {
				charge -=1;
			}
			return true;
		}else return false;
	}
	@Override
	public boolean moveLeft() {
		if (this.x > 0 && getCharge()>= 5) {
			this.x-=1;
			stepNum ++;
			if(stepNum % 2 == 0 ) {
				charge -=1;
			}
			return true;
		}else return false;
	}
	

	
    }