package robo;

public class AlphaRobot extends AbstractRobot{
	private String name;
	private String model;
	protected int x;
	protected int y;
	
	
	public AlphaRobot(String name, String model) {
		super();
		this.name = name;
		this.model = model;
		this.x = 0;
		this.y = 0;
	};
	
	public AlphaRobot() {}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		if(name !=null) {
			this.name = name;
		}else {
			System.err.println("Name can not be null");
		}
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		if(model !=null) {
			this.model = model;
		}else {
			System.err.println("model can not be null");
		};
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		if (x>= 0 && x<= 100) {this.x = x;}else {System.err.println("X out of permitted bounds");}
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		if (y >= 0 && y <= 100) {this.y = y;}else {System.err.println("Y out of permitted bounds");}
	};

	public boolean moveUp() {
		if (this.y > 0) {
			this.y+=1;
			return true;
		}else return false;
	}

	public boolean moveDown() {
		if (this.y <100) {
			this.y-=1;
			return true;
		}else return false;
	}

	public boolean moveRight() {
		if (this.x <100) {
			this.x+=1;
			return true;
		}else return false;
	}

	public boolean moveLeft() {
		if (this.x > 0) {
			this.x-=1;
			return true;
		}else return false;
	}
}
