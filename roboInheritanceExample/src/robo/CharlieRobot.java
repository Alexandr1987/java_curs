package robo;

public class CharlieRobot extends BetaRobot {
    public CharlieRobot(String name, String model) {
        super(name, model);
    }

    public boolean moveDownRight(){
        return (moveDown()&& moveRight());
    }
    public boolean moveDownLeft(){
        return (moveDown() && moveLeft());
    }
    public boolean moveUpRight(){
        return (moveUp() && moveRight());
    }
    public boolean moveUpLeft(){
        return moveUp() && moveLeft();
    }
}
