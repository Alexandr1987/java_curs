package robo;

public class RobotFactory {
    public static AbstractRobot getRobot(String name, String model){
        switch (model){
            case "Alpha" : return new AlphaRobot(name,model);
            case "Beta" : return new BetaRobot(name,model);
            case "Charlie" : return new CharlieRobot(name,model);
            default: return null;
        }
    }
}
