package main;

import instruments.AddConstructor;

@AddConstructor
public class Box {
	private int id;
	private String name;
	
	
	@Override
	public String toString() {
		return "Box [id=" + id + ", name=" + name + "]";
	}
	
}
