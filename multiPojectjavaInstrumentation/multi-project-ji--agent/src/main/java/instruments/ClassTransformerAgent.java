package instruments;
import java.io.IOException;
import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.Instrumentation;
import java.lang.reflect.InvocationTargetException;
import java.security.ProtectionDomain;

import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtConstructor;
import javassist.CtField;
import javassist.CtNewConstructor;
import javassist.LoaderClassPath;
import javassist.Modifier;
import javassist.NotFoundException;

public class ClassTransformerAgent {
	public static void premain (String args, Instrumentation instrumentation) {
		
		System.err.println("AGENT RUNNING !!!");
		instrumentation.addTransformer(new ClassFileTransformer() {

			@Override
			public byte[] transform(ClassLoader loader,
									String className, 
									Class<?> typeIfLoaded,
									ProtectionDomain domain,
									byte[] buffer){
		if(className.split("/")[0].equals("main")) {
			System.err.println("Loaded class from main:"+className);
			ClassPool pool = ClassPool.getDefault();
			pool.appendClassPath(new LoaderClassPath(loader));
			try {
				CtClass ctClass = pool.get (className.replace("/","."));
				if(ctClass.hasAnnotation(AddConstructor.class)) {
					CtField[] fields = ctClass.getDeclaredFields();
					for(CtField ctField : fields) {
						if((ctField.getModifiers() & Modifier.PRIVATE) !=0)
						System.err.println(">>>" + ctField.getName());
					}
					System.err.println("THIS CLASS WILL BE TRANSFOEMED :" + className);
					CtConstructor ccon = ctClass.getDeclaredConstructor(null);
					// remove it
					ctClass.removeConstructor(ccon);
					// create a new default constructor
						CtConstructor cstructor = CtNewConstructor.make( "public"+"Box(int id, String name)" +""+"{"+"}"+";",ctClass	) ;
					// modify construct behavior
						cstructor.setBody("this.id=id;","this.name = name;",className);
					// add constructor to class
						ctClass.addConstructor(cstructor);
						return ctClass.toBytecode();
				}
			} catch (NotFoundException e) {
				
				e.printStackTrace();
			} catch (CannotCompileException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}

				return null;
			}

		});
		
		

		}
}
